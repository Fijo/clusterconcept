using System.Collections.Generic;
using ClusterConcept.Shared.Infrastructure.Handler;
using ClusterConcept.Shared.Infrastructure.Repositories;
using ClusterConcept.Shared.Infrastructure.Server;
using ClusterConcept.Shared.Infrastructure.Server.Com;
using ClusterConcept.Shared.Infrastructure.Service.Exec;
using ClusterConcept.Shared.Infrastructure.Service.NodeConfiguration;
using ClusterConcept.Shared.Infrastructure.StreamSerializer;
using ClusterConcept.Shared.Model;
using ClusterConcept.Shared.Model.ComEntity.Type;
using ClusterConcept.Shared.Model.Events;
using ClusterConcept.Shared.Model.Node;
using Fijo.Infrastructure.DesignPattern.Events;
using Fijo.Infrastructure.DesignPattern.Repository;
using Fijo.Net.Nodelt.Dto;
using Fijo.Net.Nodelt.Handler;
using Fijo.Net.Nodelt.Properties;
using FijoCore.Infrastructure.DependencyInjection.Extentions.IKernel;
using FijoCore.Infrastructure.DependencyInjection.InitKernel.Base;
using FijoCore.Infrastructure.LightContrib.Module.Serialization.Stream;
using FijoCore.Infrastructure.LightContrib.Module.Serialization.Stream.Impl.Common;
using FijoCore.Infrastructure.LightContrib.Module.Serialization.Stream.Impl.Specific;
using IKernel = FijoCore.Infrastructure.WrappedLibs.DependencyInjection.Interface.IKernel;

namespace ClusterConcept.Shared.Properties {
	public class EntityNodeSharedInjectionModule : ExtendedNinjectModule {
		public override void AddModule(IKernel kernel) {
			kernel.Load(new NetNodeltClientInjectionModule<NodeCommand>());
			kernel.Load(new NetNodeltServerInjectionModule<NodeCommand>());
		}
		
		public override void OnLoad(IKernel kernel) {
			kernel.Bind<IRepository<InitNodeConfiguration>>().To<InitNodeConfigurationProvider>().InSingletonScope();
			kernel.Bind<IRepository<NodeConfiguration>>().To<NodeConfigurationProvider>().InSingletonScope();
			kernel.Bind<IRepository<NodeServerConfig>>().To<NodeServerConfigRepository>().InSingletonScope();
			kernel.Bind<IRepository<SockPoolConfiguration>>().To<SockPoolConfigurationRepository>().InSingletonScope();
			kernel.Bind<IStreamSerializer<NodeCommand>>().To<NodeCommandSerializer>().InSingletonScope();
			kernel.Bind<IStreamSerializer<IEnumerable<Node>>>().To<BinaryStreamSerializer<IEnumerable<Node>>>().InSingletonScope();

			kernel.Bind<IBaseHandler<NodeCommand>>().To<HelloClusterHandler>().InSingletonScope();

			kernel.Bind<IRepository<Node>>().To<NodeRepository>().InSingletonScope();
			kernel.Bind<IExecService>().To<ExecService>().InSingletonScope();
			kernel.Bind<INodeConfigurationService>().To<NodeConfigurationService>().InSingletonScope();

			kernel.Bind<IStreamSerializer<PType>>().To<PTypeStreamSerializer>().InSingletonScope();
			AddDictionarySerializer<PType, IList<byte>>();
			AddDictionarySerializer<PType, long>();
			kernel.Bind<IStreamSerializer<ChangeEntityEvent>>().To<ChangeEntityEventStreamSerializer>().InSingletonScope();
			kernel.Bind<IStreamSerializer<ChangePrimitiveEvent>>().To<ChangePrimitiveEventStreamSerializer>().InSingletonScope();
			kernel.Bind<IStreamSerializer<IEvent>>().To<BinaryStreamSerializer<IEvent>>().InSingletonScope();
			kernel.Bind<IStreamSerializer<IList<IEvent>>>().To<BinaryStreamSerializer<IList<IEvent>>>().InSingletonScope();
			kernel.Bind<IStreamSerializer<PostQuery>>().To<BinaryStreamSerializer<PostQuery>>().InSingletonScope();
			kernel.Bind<IStreamSerializer<GetQuery>>().To<BinaryStreamSerializer<GetQuery>>().InSingletonScope();
		}

		private void AddDictionarySerializer<TKey, TValue>() {
			Kernel.Bind<IStreamSerializer<KeyValuePair<TKey, TValue>>>().To<KeyValuePairStreamSerializer<TKey, TValue>>().InSingletonScope();
			Kernel.Bind<IStreamSerializer<ICollection<KeyValuePair<TKey, TValue>>>>().To<GenericCollectionStreamSerializer<KeyValuePair<TKey, TValue>>>().InSingletonScope();
			Kernel.Bind<IStreamSerializer<IDictionary<TKey, TValue>>>().To<GenericDictionaryStreamSerializer<TKey, TValue>>().InSingletonScope();
		}

		public override void Loaded(IKernel kernel) {
		}
	}
}