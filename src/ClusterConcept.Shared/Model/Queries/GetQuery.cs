﻿using System;
using ClusterConcept.Shared.Model.ComEntity.Type;
using Fijo.Infrastructure.Documentation.Attributes.DesignPattern;

namespace ClusterConcept.Shared.Model {
	[Dto, Serializable]
	public class GetQuery {
		public PBasicEntityType Key;
		public ulong FromRevision;
		public ulong ToRevision;
	}
}