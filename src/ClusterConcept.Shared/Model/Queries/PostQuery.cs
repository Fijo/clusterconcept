using System;
using System.Collections.Generic;
using ClusterConcept.Shared.Model.ComEntity.Type;
using Fijo.Infrastructure.DesignPattern.Events;
using Fijo.Infrastructure.Documentation.Attributes.DesignPattern;

namespace ClusterConcept.Shared.Model {
	[Dto, Serializable]
	public class PostQuery {
		public PBasicEntityType Key;
		public IList<IEvent> Events;
	}
}