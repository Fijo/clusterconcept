using System;
using System.Collections.Generic;
using ClusterConcept.Shared.Model.ComEntity.Type;
using Fijo.Infrastructure.DesignPattern.Events;

namespace ClusterConcept.Shared.Model.Events {
	[Serializable]
	public class ChangePrimitiveEvent : Event {
		public IDictionary<PType, IList<byte>> Changeset;
	}
}