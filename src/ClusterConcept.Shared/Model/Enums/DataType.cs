using System;

namespace ClusterConcept.Shared.Model.Enums {
	[Serializable]
	public enum DataType : byte {
		DataContext,
		Entity,
		Primitive
	}
}