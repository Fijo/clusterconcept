//BS:The Offspring - The Future Is Now (Instrumental)

using System;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using FijoCore.Infrastructure.LightContrib.Extentions.Generic.Equality.GetHashCode;

namespace ClusterConcept.Shared.Model.ComEntity.Type {
	[Serializable]
	[AboutName("P", "Persist")]
	public class PType {
		public readonly string UId;

		public PType(string uId) {
			UId = uId;
		}

		protected bool Equals(PType obj) {
			return GetType() == obj.GetType() && UId == obj.UId;
		}

		public override bool Equals(object obj) {
			return obj is PType && Equals((PType) obj);
		}

		public override int GetHashCode() {
			return UId.NullableGetHashCode();
		}

		public override string ToString() {
			return string.Format("@{0}", UId);
		}
	}
}