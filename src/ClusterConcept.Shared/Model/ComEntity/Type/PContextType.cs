using System;
using Fijo.Infrastructure.Documentation.Attributes.Info;

namespace ClusterConcept.Shared.Model.ComEntity.Type {
	[Obsolete("use PType instead")]
	[AboutName("P", "Persist")]
	public class PContextType : PType {
		public PContextType(string uId) : base(uId) {}
	}
}