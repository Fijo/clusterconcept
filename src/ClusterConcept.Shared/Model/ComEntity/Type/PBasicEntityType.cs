using System;
using System.Diagnostics;
using ClusterConcept.Shared.Model.Enums;
using Fijo.Infrastructure.Documentation.Attributes.DesignPattern;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using FijoCore.Infrastructure.LightContrib.Extentions.Integer;

namespace ClusterConcept.Shared.Model.ComEntity.Type {
	[Dto, Serializable]
	public class PBasicEntityType {
		public PType Type;
		private DataType _dataType;
		public DataType DataType {
			get { return _dataType; }
			set {
				Debug.Assert(value != DataType.Primitive);
				_dataType = value;
			}
		}
		[Note("only used if == DataType.Entity")]
		public long Id;

		#region Equals
		protected bool Equals(PBasicEntityType other) {
			#region PreCondition
			EqualityPrecondition(this);
			EqualityPrecondition(other);
			#endregion
			var dataType = DataType;
			return Equals(Type, other.Type) &&
			       Equals(dataType, other.DataType) &&
			       (!HasId(dataType) || Equals(Id, other.Id));
		}

		public override bool Equals(object obj) {
			return obj is PBasicEntityType && Equals((PBasicEntityType) obj);
		}

		public override int GetHashCode() {
			#region PreCondition
			EqualityPrecondition(this);
			#endregion
			var dataType = DataType;
// ReSharper disable NonReadonlyFieldInGetHashCode
			var hashCode = Type.GetHashCode().GetHashCodeAlgorithm(dataType.GetHashCode());
// ReSharper restore NonReadonlyFieldInGetHashCode
			if(HasId(dataType)) hashCode = hashCode.GetHashCodeAlgorithm(Id.GetHashCode());
			return hashCode;
		}
		#endregion

		public override string ToString() {
			var dataType = DataType;
			return string.Format("{0} {1}{2}", dataType, Type,
			                     HasId(dataType)
				                     ? string.Format(" #{0}", Id.ToString("x"))
				                     : string.Empty);
		}

		#region Static Methodes
		[Conditional("DEBUG")]
		private static void EqualityPrecondition(PBasicEntityType pBasicEntityType) {
			#if DEBUG
			Debug.Assert(pBasicEntityType.Type != null);
			#endif
		}

		private static bool HasId(DataType dataType) {
			return dataType == DataType.Entity;
		}
		#endregion
	}
}