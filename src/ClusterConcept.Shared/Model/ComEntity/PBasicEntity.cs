using System.Collections.Generic;
using ClusterConcept.Shared.Model.ComEntity.Type;
using Fijo.Infrastructure.Documentation.Attributes.DesignPattern;
using Fijo.Infrastructure.Documentation.Attributes.Info;

namespace ClusterConcept.Shared.Model.ComEntity {
	[Dto]
	[AboutName("P", "Persist")]
	public class PBasicEntity {
		public IDictionary<PType, long> SubEntityIds;
		public IDictionary<PType, IList<byte>> SubPrimitives;
	}
}