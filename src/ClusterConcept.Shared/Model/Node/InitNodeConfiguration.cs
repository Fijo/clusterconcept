using System.Collections.Generic;
using System.Net;
using Fijo.Infrastructure.Documentation.Attributes.DesignPattern;

namespace ClusterConcept.Shared.Model.Node {
	[Dto]
	public class InitNodeConfiguration {
		public IList<EndPoint> NodeAddresses;
		public EndPoint OwnNodeAddress;
	}
}