using System;
using Fijo.Infrastructure.Documentation.Attributes.DesignPattern;

namespace ClusterConcept.Shared.Model.Node {
	[Dto, Serializable]
	public class ClientVersion {
		public long Number;
	}
}