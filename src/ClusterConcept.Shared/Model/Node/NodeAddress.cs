using System;
using System.Net;
using Fijo.Infrastructure.Documentation.Attributes.DesignPattern;
using JetBrains.Annotations;

namespace ClusterConcept.Shared.Model.Node {
	[Dto, Serializable]
	public class NodeAddress {
		[NotNull] public EndPoint Address;
		public DateTime LastAvalible;
		
		protected bool Equals(NodeAddress other) {
			return Address.Equals(other.Address);
		}

		public override bool Equals(object obj) {
			return obj is NodeAddress && Equals((NodeAddress) obj);
		}

		public override int GetHashCode() {
			return Address.GetHashCode();
		}
	}
}