using System;
using ClusterConcept.Shared.Model.Events;
using Fijo.Infrastructure.Documentation.Attributes.DesignPattern;
using JetBrains.Annotations;

namespace ClusterConcept.Shared.Model.Node {
	[Dto, Serializable]
	public class Node {
		[NotNull] public ClientVersion Version;
		[NotNull] public NodeAddress Address;

		protected bool Equals(Node other) {
			return Address.Equals(other.Address);
		}

		public override bool Equals(object obj) {
			return obj is Node && Equals((Node) obj);
		}

		public override int GetHashCode() {
			return Address.GetHashCode();
		}
	}
}