using System.Collections.Generic;
using ClusterConcept.Shared.Model.ComEntity.Type;

namespace ClusterConcept.Shared.Model.Node {
	public class ClientVersionData
	{
		public IList<PEntityType> EntityTypes;
		public IList<PPropertyType> PropertyTypes;
	}
}