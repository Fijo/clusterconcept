using System.Collections.Generic;
using Fijo.Infrastructure.Documentation.Attributes.DesignPattern;

namespace ClusterConcept.Shared.Model.Node {
	[Dto]
	public class NodeConfiguration {
		public ISet<Node> Nodes;
		public Node OwnNode;
	}
}