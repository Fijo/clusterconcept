using System;
using System.Collections.Generic;
using ClusterConcept.Shared.Infrastructure.Server.Com;
using ClusterConcept.Shared.Infrastructure.Service.NodeConfiguration;
using ClusterConcept.Shared.Model.Node;
using Fijo.Infrastructure.DesignPattern.Lazy.Interface;
using Fijo.Net.Nodelt.Handler;
using FijoCore.Infrastructure.LightContrib.Module.Lazy;
using JetBrains.Annotations;

namespace ClusterConcept.Shared.Infrastructure.Handler {
	[UsedImplicitly]
	public class HelloClusterHandler : SendonlyHandler<IEnumerable<Node>, NodeCommand> {
		private readonly ILazy<INodeConfigurationService> _nodeConfigurationService = new LazyInject<INodeConfigurationService>();

		#region Overrides of SendonlyHandler<IEnumerable<Node>,NodeCommand>
		public override void Handle(IEnumerable<Node> obj) {
			var nodeConfigurationService = _nodeConfigurationService.Get();
			//Parallel.ForEach(obj, );
			foreach (var node in obj) {
				Console.WriteLine("Got HelloCommand from " + node.Address.Address);
				nodeConfigurationService.AddNode(node);
			}
		}

		public override NodeCommand Command { get { return NodeCommand.HelloCluster; } }
		#endregion
	}
}