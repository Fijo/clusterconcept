namespace ClusterConcept.Shared.Infrastructure.Server.Com {
	public enum NodeCommand {
		ListNodes,
		Test,
		PerfTest,
		HelloCluster,
		Get,
		Post
	}
}