using System;
using ClusterConcept.Shared.Model.Node;
using Fijo.Infrastructure.DesignPattern.Repository;
using Fijo.Net.Nodelt.Client.Provider;
using Fijo.Net.Nodelt.Dto;
using FijoCore.Infrastructure.DependencyInjection.InitKernel;
using FijoCore.Infrastructure.LightContrib.Module.Configuration;

namespace ClusterConcept.Shared.Infrastructure.Server.Com {
	public class SockPoolConfigurationRepository : RepositoryBase<SockPoolConfiguration> {
		#region Overrides of RepositoryBase<SockPoolConfiguration>
		public override SockPoolConfiguration Get() {
			var config = Kernel.Resolve<IRepository<InitNodeConfiguration>>().Get();
			var configurationService = Kernel.Resolve<IConfigurationService>();
			Console.WriteLine(configurationService.Get<int, SockPool>("DirectConnectMaxRetryCount"));
			Console.WriteLine(configurationService.Get<int, SockPool>("RetryDirectConnectWaitTime"));
			return new SockPoolConfiguration
			{
				Content = config.NodeAddresses,
				ShouldActives = configurationService.Get<int, SockPool>("ActiveSocketCount"),
				MaintenanceIntervall = configurationService.Get<int, SockPool>("RecycleMaintenanceIntervall"),
				SocketUsagesLimitToRecycle = configurationService.Get<int, SockPool>("RecycleAfterSocketUsageCount"),
				AllowRetryConnectionFaildAfterMilliseconds = configurationService.Get<int, SockPool>("AllowRetryConnectionFaildAfterMilliseconds"),
				DirectConnectMaxRetryCount = configurationService.Get<int, SockPool>("DirectConnectMaxRetryCount"),
				RetryDirectConnectWaitTime = configurationService.Get<int, SockPool>("RetryDirectConnectWaitTime"),
			};
		}
		#endregion
	}
}