using System.Linq;
using System.Net;
using ClusterConcept.Shared.Model.Node;
using Fijo.Infrastructure.DesignPattern.Repository;
using FijoCore.Infrastructure.DependencyInjection.InitKernel;
using FijoCore.Infrastructure.LightContrib.Module.Configuration;
using FijoCore.Infrastructure.LightContrib.Module.Converter.Simple.Interface;

namespace ClusterConcept.Shared.Infrastructure.Server {
	public class InitNodeConfigurationProvider : SingletonRepositoryBase<InitNodeConfiguration> {
		protected override InitNodeConfiguration Create() {
			var configurationService = Kernel.Resolve<IConfigurationService>();
			var s = configurationService.Get<string>("EntityNode.NodeAddresses");
			var nodeAddresses = string.IsNullOrEmpty(s) ? new string[] {} : s.Split(';');
			var nodeAddress = configurationService.Get<string>("EntityNode.OwnNodeAddress");
			var endPointParser = Kernel.Resolve<IConverter<string, EndPoint>>();

			return new InitNodeConfiguration
			{
				NodeAddresses = nodeAddresses.Select(endPointParser.Convert).ToList(),
				OwnNodeAddress = endPointParser.Convert(nodeAddress)
			};
		}
	}
}