using ClusterConcept.Shared.Model.Node;
using Fijo.Infrastructure.DesignPattern.Repository;
using Fijo.Net.Nodelt.Dto;
using FijoCore.Infrastructure.DependencyInjection.InitKernel;

namespace ClusterConcept.Shared.Infrastructure.Server {
	public class NodeServerConfigRepository : RepositoryBase<NodeServerConfig> {
		#region Overrides of RepositoryBase<NodeServerConfig>
		public override NodeServerConfig Get() {
			var initNodeConfiguration = Kernel.Resolve<IRepository<InitNodeConfiguration>>().Get();
			return new NodeServerConfig
			{
				NodeAddress = initNodeConfiguration.OwnNodeAddress
			};
		}
		#endregion
	}
}