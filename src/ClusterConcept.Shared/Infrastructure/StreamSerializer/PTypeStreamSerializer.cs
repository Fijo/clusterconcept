using System.IO;
using ClusterConcept.Shared.Model.ComEntity.Type;
using FijoCore.Infrastructure.DependencyInjection.InitKernel;
using FijoCore.Infrastructure.LightContrib.Default.Service.Out.Interface;
using FijoCore.Infrastructure.LightContrib.Module.Serialization.Stream;
using JetBrains.Annotations;

namespace ClusterConcept.Shared.Infrastructure.StreamSerializer {
	[UsedImplicitly]
	public class PTypeStreamSerializer : IStreamSerializer<PType> {
		private readonly IStreamSerializer<string> _stringStreamSerializer = Kernel.Resolve<IStreamSerializer<string>>();
		private readonly IOut _out = Kernel.Resolve<IOut>();
		#region Implementation of IStreamSerializer<PType>
		public void Serialize(StreamWriter streamWriter, PType obj) {
			_stringStreamSerializer.Serialize(streamWriter, obj.UId);
		}

		public bool TryDeserialize(StreamReader streamReader, out PType result) {
			string str;
			return _stringStreamSerializer.TryDeserialize(streamReader, out str)
				       ? _out.True(out result, new PType(str))
				       : _out.False(out result);
		}
		#endregion
	}
}