using System.Collections.Generic;
using System.IO;
using ClusterConcept.Shared.Model.ComEntity.Type;
using ClusterConcept.Shared.Model.Events;
using FijoCore.Infrastructure.DependencyInjection.InitKernel;
using FijoCore.Infrastructure.LightContrib.Module.Serialization.Stream;
using JetBrains.Annotations;

namespace ClusterConcept.Shared.Infrastructure.StreamSerializer {
	// todo may extract a base class for ChangeEntityEventStreamSerializer and ChangePrimitiveEventStreamSerializer
	[UsedImplicitly]
	public class ChangePrimitiveEventStreamSerializer : EventBaseStreamSerializer<ChangePrimitiveEvent> {
		private readonly IStreamSerializer<IDictionary<PType, IList<byte>>> _changesetStreamSerializer = Kernel.Resolve<IStreamSerializer<IDictionary<PType, IList<byte>>>>();

		public override void Serialize(StreamWriter streamWriter, ChangePrimitiveEvent obj) {
			base.Serialize(streamWriter, obj);
			_changesetStreamSerializer.Serialize(streamWriter, obj.Changeset);
		}

		public override bool TryDeserialize(StreamReader streamReader, out ChangePrimitiveEvent result) {
			IDictionary<PType, IList<byte>> changeset;
			if(!(base.TryDeserialize(streamReader, out result) && _changesetStreamSerializer.TryDeserialize(streamReader, out changeset))) return Out.False(out result);
			result.Changeset = changeset;
			return true;
		}
	}
}