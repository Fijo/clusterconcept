using System.IO;
using Fijo.Infrastructure.DesignPattern.Events;
using FijoCore.Infrastructure.DependencyInjection.InitKernel;
using FijoCore.Infrastructure.LightContrib.Default.Service.Out.Interface;
using FijoCore.Infrastructure.LightContrib.Module.Serialization.Stream;

namespace ClusterConcept.Shared.Infrastructure.StreamSerializer {
	public abstract class EventBaseStreamSerializer<TEvent> : IStreamSerializer<TEvent> where TEvent : IEvent, new() {
		private readonly IStreamSerializer<ulong> _stringStreamSerializer = Kernel.Resolve<IStreamSerializer<ulong>>();
		protected readonly IOut Out = Kernel.Resolve<IOut>();
		#region Implementation of IStreamSerializer<TEvent>
		public virtual void Serialize(StreamWriter streamWriter, TEvent obj) {
			_stringStreamSerializer.Serialize(streamWriter, obj.Revision);
		}

		public virtual bool TryDeserialize(StreamReader streamReader, out TEvent result) {
			ulong revision;
			return _stringStreamSerializer.TryDeserialize(streamReader, out revision)
				       ? Out.True(out result, new TEvent {Revision = revision})
				       : Out.False(out result);
		}
		#endregion
	}
}