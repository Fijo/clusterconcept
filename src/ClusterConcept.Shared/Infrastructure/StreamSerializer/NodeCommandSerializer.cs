using ClusterConcept.Shared.Infrastructure.Server.Com;
using Fijo.Net.Nodelt.ImplBase;

namespace ClusterConcept.Shared.Infrastructure.StreamSerializer {
	public class NodeCommandSerializer : DefaultNodeCommandSerializer<NodeCommand> {
		#region Overrides of DefaultNodeCommandSerializer<NodeCommand>
		protected override NodeCommand Convert(int value) {
			return (NodeCommand) value;
		}

		protected override int Convert(NodeCommand value) {
			return (int) value;
		}
		#endregion
	}
}