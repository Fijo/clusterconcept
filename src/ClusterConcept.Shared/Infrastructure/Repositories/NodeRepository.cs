using ClusterConcept.Shared.Model.Node;
using Fijo.Infrastructure.DesignPattern.Repository;
using FijoCore.Infrastructure.DependencyInjection.InitKernel;

namespace ClusterConcept.Shared.Infrastructure.Repositories {
	public class NodeRepository : SingletonRepositoryBase<Node> {
		#region Overrides of SingletonRepositoryBase<Node>
		protected override Node Create() {
			var initNodeConfiguration = Kernel.Resolve<IRepository<InitNodeConfiguration>>().Get();
			return new Node
			{
				Address = new NodeAddress
				{
					Address = initNodeConfiguration.OwnNodeAddress
				},
				Version = new ClientVersion
				{
					Number = -0x7fffffffffffffff
				}
			};
		}
		#endregion
	}
}