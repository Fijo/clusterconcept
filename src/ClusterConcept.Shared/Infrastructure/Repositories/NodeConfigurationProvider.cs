using System.Collections.Generic;
using ClusterConcept.Shared.Model.Node;
using Fijo.Infrastructure.DesignPattern.Repository;
using FijoCore.Infrastructure.DependencyInjection.InitKernel;

namespace ClusterConcept.Shared.Infrastructure.Repositories {
	public class NodeConfigurationProvider : SingletonRepositoryBase<NodeConfiguration> {
		#region Overrides of SingletonRepositoryBase<NodeConfiguration>
		protected override NodeConfiguration Create() {
			var node = Kernel.Resolve<IRepository<Node>>().Get();
			return new NodeConfiguration
			{
				Nodes = new HashSet<Node>(),
				OwnNode = node
			};
		}
		#endregion
	}
}