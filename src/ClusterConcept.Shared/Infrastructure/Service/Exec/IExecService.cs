using System.Collections.Generic;
using ClusterConcept.Shared.Model.Node;

namespace ClusterConcept.Shared.Infrastructure.Service.Exec {
	public interface IExecService {
		void HelloCluster(Node toNode = null, bool force = false);
		void HelloCluster(IEnumerable<Node> fromNode, Node toNode = null, bool force = false);
	}
}