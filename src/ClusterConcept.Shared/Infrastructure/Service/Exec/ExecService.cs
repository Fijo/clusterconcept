using System;
using System.Collections.Generic;
using System.Net;
using System.Threading;
using ClusterConcept.Shared.Infrastructure.Server.Com;
using ClusterConcept.Shared.Model.Node;
using Fijo.Infrastructure.DesignPattern.Repository;
using Fijo.Net.Nodelt.Client.Com;
using Fijo.Net.Nodelt.Handler;
using Fijo.Net.Nodelt.Provider;
using FijoCore.Infrastructure.DependencyInjection.InitKernel;
using FijoCore.Infrastructure.LightContrib.Extentions.Generic.IntoCollection;
using FijoCore.Infrastructure.LightContrib.Extentions.ICollection.Generic;
using FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic;

namespace ClusterConcept.Shared.Infrastructure.Service.Exec {
	public class ExecService : IExecService {
		private readonly Node _node = Kernel.Resolve<IRepository<Node>>().Get();
		private readonly InitNodeConfiguration _initNode = Kernel.Resolve<IRepository<InitNodeConfiguration>>().Get();
		private readonly ISendonlyHandler<IEnumerable<Node>, NodeCommand> _helloClusterHandler;
		private readonly INodeClientCom<NodeCommand> _nodeClientCom = Kernel.Resolve<INodeClientCom<NodeCommand>>();

		public ExecService() {
			var handlerProvider = Kernel.Resolve<IHandlerProvider<NodeCommand>>();
			_helloClusterHandler = (ISendonlyHandler<IEnumerable<Node>, NodeCommand>) handlerProvider.Get(NodeCommand.HelloCluster);
		}

		#region Implementation of IExecService
		public void HelloCluster(Node toNode = null, bool force = false) {
			if (_initNode.NodeAddresses.None()) return;
			HelloCluster(_node.IntoEnumerable(), toNode, force);
		}

		public void HelloCluster(IEnumerable<Node> fromNode, Node toNode = null, bool force = false) {
			fromNode = fromNode.Execute();
			LogHelloCommands(fromNode, toNode);
			InternalHelloCluster(fromNode, toNode != null ? toNode.Address.Address.IntoArray() : null, force ? 3 : -1);
		}

		private void InternalHelloCluster(IEnumerable<Node> fromNode, ICollection<EndPoint> toEndPoint, int maxTrys) {
			var from = fromNode.Execute();
			var currentTry = 0;
			while(!_nodeClientCom.Send(_helloClusterHandler, from, toEndPoint)) {
				if(++currentTry >= maxTrys) break;
				Thread.Sleep(3000);
			}
		}

		private void LogHelloCommands(IEnumerable<Node> fromNode, Node toNode) {
			var s = (toNode == null ? "random" : toNode.Address.Address.ToString());
			foreach (var node in fromNode) {
				Console.WriteLine("HelloCluster " + node.Address.Address + " => " + s);
			}
		}
		#endregion
	}
}