using System;
using ClusterConcept.Shared.Infrastructure.Service.Exec;
using ClusterConcept.Shared.Model.Node;
using Fijo.Infrastructure.DesignPattern.Repository;
using Fijo.Net.Nodelt.Client.Provider;
using FijoCore.Infrastructure.DependencyInjection.InitKernel;
using FijoCore.Infrastructure.LightContrib.Extentions.Generic.IntoCollection;
using FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic;

namespace ClusterConcept.Shared.Infrastructure.Service.NodeConfiguration {
	public class NodeConfigurationService : INodeConfigurationService {
		private readonly Model.Node.NodeConfiguration _nodeConfiguration = Kernel.Resolve<IRepository<Model.Node.NodeConfiguration>>().Get();
		private readonly IExecService _execService = Kernel.Resolve<IExecService>();
		private readonly ISockProvider _sockProvider = Kernel.Resolve<ISockProvider>();

		#region Implementation of INodeConfigurationService
		public void AddNode(Node node) {
			var nodes = _nodeConfiguration.Nodes;
			if(nodes.Add(node)) {
				Console.WriteLine("AddNode " + node.Address.Address);
				_sockProvider.Add(node.Address.Address.IntoEnumerable());
			}
			_execService.HelloCluster(nodes.Without(node).AddReturn(_nodeConfiguration.OwnNode), node);
		}
		#endregion
	}
}