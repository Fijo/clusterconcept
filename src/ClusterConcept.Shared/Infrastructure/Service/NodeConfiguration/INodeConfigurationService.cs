using ClusterConcept.Shared.Model.Node;

namespace ClusterConcept.Shared.Infrastructure.Service.NodeConfiguration {
	public interface INodeConfigurationService {
		void AddNode(Node node);
	}
}