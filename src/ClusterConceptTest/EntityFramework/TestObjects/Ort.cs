using ClusterConcept.EntityFramework.Model.Attributes;
using ClusterConcept.EntityFramework.Model.Entity;

namespace ClusterConceptTest.EntityFramework.TestObjects {
	[UId("00000023")]
	public class Ort : EntityBase
	{
		[UId("00000000")]
		public int Latlatitude { get; set; }
		[UId("00000001")]
		public int Longitude { get; set; }
	}
}