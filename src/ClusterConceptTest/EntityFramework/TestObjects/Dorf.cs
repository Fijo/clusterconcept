using ClusterConcept.EntityFramework.Model.Attributes;
using ClusterConcept.EntityFramework.Model.Entity;

namespace ClusterConceptTest.EntityFramework.TestObjects {
	[UId("00000021")]
	public class Dorf : EntityBase
	{
		[UId("00000000")]
		public string Name { get; set; }
		[UId("00000001")]
		public int Einwohnerzahl { get; set; }
		[UId("00000002")]
		public Ort Lage { get; set; }
		[UId("00000003")]
		public Ort NordGrenze { get; set; }
		[UId("00000004")]
		public Ort SuedGrenze { get; set; }
		[UId("00000005")]
		public Ort WestGrenze { get; set; }
		[UId("00000006")]
		public Ort OstGrenze { get; set; }
	}
}