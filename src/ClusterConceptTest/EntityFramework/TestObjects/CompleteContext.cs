using ClusterConcept.EntityFramework.Model.Attributes;
using ClusterConcept.EntityFramework.Model.DataContext;

namespace ClusterConceptTest.EntityFramework.TestObjects {
	[UId("00000020")]
	public class CompleteContext : IDataContext
	{
		[UId("00000000")]
		public LocalContext LocalContext { get; set; }
	}
}