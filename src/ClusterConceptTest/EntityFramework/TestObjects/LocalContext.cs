using ClusterConcept.EntityFramework.Model.Attributes;
using ClusterConcept.EntityFramework.Model.DataContext;

namespace ClusterConceptTest.EntityFramework.TestObjects {
	[UId("00000022")]
	public class LocalContext : IDataContext
	{
		[UId("00000000")]
		public Dorf Dorf { get; set; }
	}
}