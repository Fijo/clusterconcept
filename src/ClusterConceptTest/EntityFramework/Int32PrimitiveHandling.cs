using System;
using System.Collections.Generic;
using System.Linq;
using ClusterConcept.EntityFramework.Infrastructure.Interfaces;
using JetBrains.Annotations;

namespace ClusterConceptTest.EntityFramework {
	[UsedImplicitly]
	public class Int32PrimitiveHandling : IPrimitiveHandling {
		#region Implementation of IPrimitiveHandling
		public string UId { get { return Type.Name; } }
		public Type Type { get { return typeof (int); } }
		public IEnumerable<byte> Serialize(object obj) {
			return BitConverter.GetBytes((int) obj);
		}

		public object Deserialize(IEnumerable<byte> bytes) {
			return BitConverter.ToInt32(bytes.ToArray(), 0);
		}
		#endregion
	}
}