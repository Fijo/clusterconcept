﻿//BS:Böhse Onkelz - Dunkler Ort

using System.IO;
using System.Linq;
using ClusterConcept.EntityFramework.Infrastructure.Service;
using ClusterConcept.EntityFramework.Infrastructure.Service.ObjectStore;
using ClusterConcept.EntityFramework.Model.Internal.Type.Meta;
using ClusterConceptTest.EntityFramework.TestObjects;
using ClusterConceptTest.Properties;
using Fijo.Infrastructure.DesignPattern.Repository;
using FijoCore.Infrastructure.DependencyInjection.InitKernel;
using FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic;
using FijoCore.Infrastructure.LightContrib.Module.Configuration;
using FijoCore.Infrastructure.LightContrib.Repositories;
using NUnit.Framework;

namespace ClusterConceptTest.EntityFramework {
	[TestFixture]
	public class EntityFrameworkTest : TestBase {
		private IDataContextService _dataContextService;
		private TypeMetas _typeMetas;
		private string _dataPath;
		private IEntityService _entityService;
		private AllFileRepository _allFileRepository;

		[SetUp]
		public void SetUp() {
			var internalInitKernel = new InternalInitKernel();
			internalInitKernel.Init();
			_dataContextService = Kernel.Resolve<IDataContextService>();
			_entityService = Kernel.Resolve<IEntityService>();
			_typeMetas = Kernel.Resolve<IRepository<TypeMetas>>().Get();
			_dataPath = Kernel.Resolve<IConfigurationService>().Get<string>(typeof (FileBasedDataAccessService), "DataPath");
			_allFileRepository = Kernel.Resolve<AllFileRepository>();
		}

		[Test]
		public void Test() {
			var pContextTypeFor = GetContextMetaFor<LocalContext>();
			var context = (LocalContext) _dataContextService.Get(pContextTypeFor);
			context.Dorf.Name = "Öhringen";
			context.Dorf.Einwohnerzahl = 12345;
			context.Dorf.Lage.Latlatitude = 92;
			context.Dorf.Lage.Longitude = 23;
			context.Dorf.NordGrenze.Latlatitude = 91;
			context.Dorf.NordGrenze.Longitude = 19;
			context.Dorf.SuedGrenze.Latlatitude = 92;
			context.Dorf.SuedGrenze.Longitude = 29;
			context.Dorf.WestGrenze.Latlatitude = 93;
			context.Dorf.WestGrenze.Longitude = 39;
			context.Dorf.OstGrenze.Latlatitude = 94;
			context.Dorf.OstGrenze.Longitude = 49;
			_dataContextService.Save(context);
		}

		
		//[Test]
		//public void Test_Dorf() {
		//	var context = (Dorf) _entityService.Get(typeof(Dorf), -9223372036854775808);
		//	context.Name = "Öhringen";
		//	context.Einwohnerzahl = 12345;
		//	context.Lage.Latlatitude = 92;
		//	context.Lage.Longitude = 23;
		//	_entityService.Save(context);
		//}


		[Test]
		public void Data_SaveChangeLoad_WorksAsRestore() {
			Test();
			SetUp();
			var pContextTypeFor = GetContextMetaFor<LocalContext>();
			var context = (LocalContext) _dataContextService.Get(pContextTypeFor);
			Assert.AreEqual("Öhringen", context.Dorf.Name);
			Assert.AreEqual(12345, context.Dorf.Einwohnerzahl);
			Assert.AreEqual(92, context.Dorf.Lage.Latlatitude);
			Assert.AreEqual(23, context.Dorf.Lage.Longitude);
			Assert.AreEqual(91, context.Dorf.NordGrenze.Latlatitude);
			Assert.AreEqual(19, context.Dorf.NordGrenze.Longitude);
			Assert.AreEqual(92, context.Dorf.SuedGrenze.Latlatitude);
			Assert.AreEqual(29, context.Dorf.SuedGrenze.Longitude);
			Assert.AreEqual(93, context.Dorf.WestGrenze.Latlatitude);
			Assert.AreEqual(39, context.Dorf.WestGrenze.Longitude);
			Assert.AreEqual(94, context.Dorf.OstGrenze.Latlatitude);
			Assert.AreEqual(49, context.Dorf.OstGrenze.Longitude);

		}

		[Test]
		public void NoDataAvailible_EverythingIsEmptyOrAnUnchangedInstance() {
			// may security critical
			_allFileRepository.Get(_dataPath).ForEach(File.Delete);
			var pContextTypeFor = GetContextMetaFor<LocalContext>();
			var context = (LocalContext) _dataContextService.Get(pContextTypeFor);
			Assert.AreEqual(default(string), context.Dorf.Name);
			Assert.AreEqual(default(int), context.Dorf.Einwohnerzahl);
			Assert.AreEqual(default(int), context.Dorf.Lage.Latlatitude);
			Assert.AreEqual(default(int), context.Dorf.Lage.Longitude);
			Assert.AreEqual(default(int), context.Dorf.NordGrenze.Latlatitude);
			Assert.AreEqual(default(int), context.Dorf.NordGrenze.Longitude);
			Assert.AreEqual(default(int), context.Dorf.SuedGrenze.Latlatitude);
			Assert.AreEqual(default(int), context.Dorf.SuedGrenze.Longitude);
			Assert.AreEqual(default(int), context.Dorf.WestGrenze.Latlatitude);
			Assert.AreEqual(default(int), context.Dorf.WestGrenze.Longitude);
			Assert.AreEqual(default(int), context.Dorf.OstGrenze.Latlatitude);
			Assert.AreEqual(default(int), context.Dorf.OstGrenze.Longitude);
		}

		private ContextTypeMeta GetContextMetaFor<T>() {
			var type = typeof (T);
			return _typeMetas.Contextes.Single(x => x.Type == type);
		}
	}
}