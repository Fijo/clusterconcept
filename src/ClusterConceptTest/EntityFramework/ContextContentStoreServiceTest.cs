using System.Collections.Generic;
using System.Linq;
using ClusterConcept.EntityFramework.Infrastructure.Service;
using ClusterConcept.EntityFramework.Infrastructure.Service.ContentStore;
using ClusterConcept.EntityFramework.Infrastructure.Service.ObjectStore;
using ClusterConcept.EntityFramework.Model.DataContext;
using ClusterConcept.EntityFramework.Model.Internal.Type.Meta;
using ClusterConcept.Shared.Model.ComEntity.Type;
using ClusterConcept.Shared.Model.Enums;
using ClusterConceptTest.EntityFramework.Mocks;
using ClusterConceptTest.EntityFramework.TestObjects;
using Fijo.Infrastructure.DesignPattern.Repository;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using FijoCore.Infrastructure.DependencyInjection.InitKernel;
using NUnit.Framework;
using Ninject;

namespace ClusterConceptTest.EntityFramework {
	[TestFixture]//(typeof(ContextContentStoreService))
	public class ContextContentStoreServiceTest : TestBase {
		private IContentStoreService _contentStoreService;
		private TypeMetas _typeMetas;

		[SetUp]
		public override void SetUp() {
			base.SetUp();
		}

		protected override void ResolveFields() {
			_contentStoreService = Kernel.Inject.GetAll<IContentStoreService>().Single(x => x.DataType == DataType.DataContext);
			_typeMetas = Kernel.Resolve<IRepository<TypeMetas>>().Get();
			base.ResolveFields();
		}

		#region Save
		[Test]
		public void CompleteContextWithLocalContext_Save_MustReturnEmptyCollection() {
			Kernel.Inject.Rebind<IObjectStoreService<IDataContext>>().To<EntityObjectStoreServiceMock<IDataContext>>();
			Kernel.Inject.Rebind<IDataContextService>().To<DataContextServiceMock>();
			ResolveFields();

			var context = GetCompleteContextWithLocalContext();
			var dataBundle = GetDefaultDataBundle();

			var typeMeta = GetTypeMetaFor<CompleteContext>();
			var toStoreSets = CreateStoreSets();
			_contentStoreService.Save(context, null, dataBundle, typeMeta, toStoreSets);

			var pType = GetPTypeUId<CompleteContext>();
			var actual = GetEvents(toStoreSets, pType).Count();
			Assert.AreEqual(0, actual);
		}

		[Test, Note(@"Contextes are singletons so you don�t have to save any SubContextes of them.
					But you�ll have to save contained primitives or entities, in contextes and in SubContextes recursively.
					So its important, that SubContextes will although be saved cause they may contains primitives or entities.
		")]
		public void CompleteContextWithLocalContext_Save_MustCallSaveCorrectly() {
			var signatures = new List<IDataContext>();
			Kernel.Inject.Rebind<IObjectStoreService<IDataContext>>().ToConstant(new EntityObjectStoreServiceMock<IDataContext>
			{
				OnSave = cont => signatures.Add(cont)
			});
			Kernel.Inject.Rebind<IDataContextService>().To<DataContextServiceMock>();
			ResolveFields();

			var context = GetCompleteContextWithLocalContext();
			var dataBundle = GetDefaultDataBundle();
			var typeMeta = GetTypeMetaFor<CompleteContext>();
			var toStoreSets = CreateStoreSets();
			_contentStoreService.Save(context, null, dataBundle, typeMeta, toStoreSets);

			Assert.AreEqual(1, signatures.Count);
			Assert.AreEqual(context.LocalContext, signatures.First());
		}
		#endregion
		
		#region Load
		[Test]
		public void GetEmptyCompleteContext_Load_FillCompleteContextCorrect() {
			var localContext = new LocalContext
			{
				Dorf = new Dorf()
			};
			Kernel.Inject.Rebind<IObjectStoreService<IDataContext>>().To<EntityObjectStoreServiceMock<IDataContext>>();
			Kernel.Inject.Rebind<IDataContextService>().ToConstant(new DataContextServiceMock
			{
				OnGet = type => localContext
			});
			ResolveFields();

			var context = GetEmptyCompleteContext();
			var dataBundle = GetDefaultDataBundle();
			var typeMeta = GetTypeMetaFor<CompleteContext>();
			_contentStoreService.Load(context, dataBundle, typeMeta);

			Assert.AreSame(localContext, context.LocalContext);
		}
		
		[Test]
		public void GetEmptyCompleteContext_Load_MustCallGetCorrectly() {
			var signatures = new List<ContextTypeMeta>();
			Kernel.Inject.Rebind<IObjectStoreService<IDataContext>>().To<EntityObjectStoreServiceMock<IDataContext>>();
			Kernel.Inject.Rebind<IDataContextService>().ToConstant(new DataContextServiceMock
			{
				OnGet = type => {
					signatures.Add(type);
					return new LocalContext();
				}
			});
			ResolveFields();

			var context = GetEmptyCompleteContext();
			var dataBundle = GetDefaultDataBundle();
			var typeMeta = GetTypeMetaFor<CompleteContext>();
			_contentStoreService.Load(context, dataBundle, typeMeta);

			Assert.AreEqual(1, signatures.Count);
			Assert.AreEqual(GetTypeMetaFor<LocalContext>(), signatures.First());
		}
		#endregion

		private static CompleteContext GetCompleteContextWithLocalContext() {
			return new CompleteContext	{
				LocalContext = new LocalContext()
			};
		}
		
		private static CompleteContext GetEmptyCompleteContext() {
			return new CompleteContext();
		}


		private PContextType GetPContextTypeFor<T>() {
			return new PContextType(GetPTypeUId<T>());
		}

		private TypeMeta GetTypeMetaFor<T>() {
			var type = typeof (T);
			return _typeMetas.Contextes.Single(x => x.Type == type);
		}
	}
}