using System;
using System.Collections.Generic;
using System.Linq;
using ClusterConcept.EntityFramework.Infrastructure.Service.ObjectStore;
using ClusterConcept.EntityFramework.Model.Internal;
using ClusterConcept.Shared.Model.ComEntity;
using ClusterConcept.Shared.Model.ComEntity.Type;
using ClusterConceptTest.Properties;
using Fijo.Infrastructure.DesignPattern.Events;
using FijoCore.Infrastructure.DependencyInjection.InitKernel;
using FijoCore.Infrastructure.LightContrib.Module.Transaction;
using FijoCore.Infrastructure.TestTools.FAssert;

namespace ClusterConceptTest.EntityFramework {
	public abstract class TestBase {
		protected ITransactionFactory TransactionFactory;
		protected FAssert FAssert;
		private ITransactionService _transactionService;

		public virtual void SetUp() {
			new InternalInitKernel().Init();
			FAssert = new FAssert();
		}

		protected virtual void ResolveFields() {
			TransactionFactory = Kernel.Resolve<ITransactionFactory>();
			_transactionService = Kernel.Resolve<ITransactionService>();
		}

		protected IList<StoreSet> CreateStoreSets() {
			return new List<StoreSet>();
		}

		protected Transaction CreateTransaction() {
			return TransactionFactory.CreateTransaction();
		}

		protected DataBundle GetDefaultDataBundle()
		{
			return GetDataBundle(new IEvent[]{});
		}
		
		protected DataBundle GetDataBundle(IEnumerable<IEvent> events)
		{
			var transaction = CreateTransaction();
			foreach (var @event in events)
				_transactionService.AddEvent(transaction, @event);
			return new DataBundle	{
				Chnageset = transaction,
				Data = new PBasicEntity	{
					SubEntityIds = new Dictionary<PType, long>(),
					SubPrimitives = new Dictionary<PType, IList<byte>>()
				}
			};
		}

		protected string GetPTypeUId<T>() {
			return GetPTypeUId(typeof (T));
		}
		
		protected string GetPTypeUId(Type type) {
			return type.Name;
		}

		protected IEnumerable<IEvent> GetEvents(IEnumerable<StoreSet> toStoreSets, string pType) {
			return toStoreSets.Where(x => x.Key.Type.UId == pType).SelectMany(x => x.Transaction.Events);
		}
	}
}