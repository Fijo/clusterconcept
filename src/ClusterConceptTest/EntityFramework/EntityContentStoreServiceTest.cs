﻿//BS:Frei Wild - WahreWerte.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using ClusterConcept.EntityFramework.Infrastructure.Service;
using ClusterConcept.EntityFramework.Infrastructure.Service.ContentStore;
using ClusterConcept.EntityFramework.Infrastructure.Service.ObjectStore;
using ClusterConcept.EntityFramework.Model.Entity;
using ClusterConcept.EntityFramework.Model.Exceptions;
using ClusterConcept.EntityFramework.Model.Internal;
using ClusterConcept.EntityFramework.Model.Internal.Type.Meta;
using ClusterConcept.Shared.Model.ComEntity.Type;
using ClusterConcept.Shared.Model.Enums;
using ClusterConcept.Shared.Model.Events;
using ClusterConceptTest.EntityFramework.Mocks;
using ClusterConceptTest.EntityFramework.TestObjects;
using Fijo.Infrastructure.DesignPattern.Events;
using Fijo.Infrastructure.DesignPattern.Repository;
using FijoCore.Infrastructure.DependencyInjection.InitKernel;
using FijoCore.Infrastructure.LightContrib.Default.Service.CheckName;
using FijoCore.Infrastructure.LightContrib.Extentions.IList.Generic;
using FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic;
using NUnit.Framework;
using Ninject;

namespace ClusterConceptTest.EntityFramework {
	[TestFixture]//(typeof(EntityContentStoreService))
	public class EntityContentStoreServiceTest : TestBase {
		private IContentStoreService _contentStoreService;
		private TypeMetas _typeMetas;

		[SetUp]
		public override void SetUp() {
			base.SetUp();
		}

		protected override void ResolveFields() {
			_contentStoreService = Kernel.Inject.GetAll<IContentStoreService>().Single(x => x.DataType == DataType.Entity);
			_typeMetas = Kernel.Resolve<IRepository<TypeMetas>>().Get();
			base.ResolveFields();
		}

		#region Save
		[Test]
		public void DorfWithOrtThatHasId3_Save_MustReturnCorrectEvents() {
			Kernel.Inject.Rebind<IObjectStoreService<IEntity>>().To<EntityObjectStoreServiceMock<IEntity>>().InSingletonScope();
			Kernel.Inject.Rebind<IEntityService>().To<EntityServiceMock>().InSingletonScope();
			ResolveFields();

			var entity = GetTestDorfWithOrtThatHasId3();
			var dataBundle = GetDefaultDataBundle();
			var typeMeta = GetTypeMetaFor<Dorf>();
			var toStoreSets = CreateStoreSets();
			_contentStoreService.Save(entity, GetPBasicEntityType(entity), dataBundle, typeMeta, toStoreSets);


			var pType = GetPTypeUId<Dorf>();
			var events = GetEvents(toStoreSets, pType).Execute();
			Assert.AreEqual(1, events.Count);
			var @event = events.First();
			Assert.IsTrue(@event is ChangeEntityEvent);
			var changeEntityEvent = ((ChangeEntityEvent) @event);

			var changeset = changeEntityEvent.Changeset;
			Assert.AreEqual(1, changeset.Count);
			var changesetEntry = changeset.First();
			var memberPTypeUId = GetMemberPTypeUId<Dorf, Ort>(x => x.Lage);
			Assert.AreEqual(memberPTypeUId, changesetEntry.Key.UId);
			Assert.AreEqual(entity.Lage.Id, changesetEntry.Value);
		}

		[Test, Description(@"When save an entity subentities must be saved recursively so that their subentities are not left out.
							What is going to be called for this is the IObjectStoreService<TBasicEntity>.
							We use the an Entity as object that contains our subentities so TBasicEntity will be IEntity here.
		")]
		public void DorfWithOrtThatHasId3_Save_MustSaveOrtUsingEntityServiceMock() {
			var lastSavedEntities = new List<IEntity>();
			Kernel.Inject.Rebind<IObjectStoreService<IEntity>>().ToConstant(new EntityObjectStoreServiceMock<IEntity>
			{
				OnSave = entity => lastSavedEntities.Add(entity)
			}).InSingletonScope();
			Kernel.Inject.Rebind<IEntityService>().To<EntityServiceMock>().InSingletonScope();
			ResolveFields();

			var context = GetTestDorfWithOrtThatHasId3();
			var dataBundle = GetDefaultDataBundle();
			var typeMeta = GetTypeMetaFor<Dorf>();
			var toStoreSets = CreateStoreSets();
			_contentStoreService.Save(context, null, dataBundle, typeMeta, toStoreSets);

			Assert.AreEqual(1, lastSavedEntities.Count);
			Assert.AreSame(context.Lage, lastSavedEntities.First());
		}
		#endregion

		#region Load
		[Test]
		public void EmptyDorfAndDataBundleWithSubEntityIdOfOrtTypeThatIsId3_Load_MustFillDorfWithOrtWithId3() {
			Kernel.Inject.Rebind<IObjectStoreService<IEntity>>().To<EntityObjectStoreServiceMock<IEntity>>().InSingletonScope();
			Kernel.Inject.Rebind<IEntityService>().ToConstant(new EntityServiceMock	{
				OnInternalGet = (id, meta) => new Ort {Id = id}
			}).InSingletonScope();
			ResolveFields();
						
			var context = GetEmptyDorf();
			var dataBundle = GetDataBundleWithId3();
			dataBundle.Data.SubEntityIds.Add(GetPTypeFor<Ort>(), 3);
			var typeMeta = GetTypeMetaFor<Dorf>();
			_contentStoreService.Load(context, dataBundle, typeMeta);

			var lage = context.Lage;
			Assert.IsNotNull(lage);
			Assert.AreEqual(3, lage.Id);
		}

		private class LoadSignature {
			public long Id;
			public PType Key;
		}

		[Test]
		public void EmptyDorfAndDataBundleWithSubEntityIdOfOrtTypeThatIsId3_Load_CallLoadForCorrectSubEntity() {
			var signatures = new List<LoadSignature>();
			Kernel.Inject.Rebind<IObjectStoreService<IEntity>>().To<EntityObjectStoreServiceMock<IEntity>>().InSingletonScope();
			Kernel.Inject.Rebind<IEntityService>().ToConstant(new EntityServiceMock	{
				OnInternalGet = (id, meta) => {
					signatures.Add(new LoadSignature {Id = id, Key = meta.PKey});
					return new Ort {Id = 3};
				},
				OnInternalCreate = key => {
					throw new NotImplementedException();
				}
			}).InSingletonScope();
			ResolveFields();
						
			var context = GetEmptyDorf();
			var dataBundle = GetDataBundleWithId3();
			var typeMeta = GetTypeMetaFor<Dorf>();
			_contentStoreService.Load(context, dataBundle, typeMeta);

			Assert.AreEqual(1, signatures.Count);
			var signature = signatures.First();
			Assert.AreEqual(signature.Id, 3);
			var wanted = GetPTypeFor<Ort>();
			Assert.AreEqual(wanted, signature.Key);
		}

		private DataBundle GetDataBundleWithId3()
		{
			var dataBundle = GetDataBundle(new List<IEvent>
				                               {
					                               new ChangeEntityEvent
						                               {
							                               Revision = 1,
							                               Changeset = new Dictionary<PType, long>
								                                           {
									                                           {GetMemberPType<Dorf, Ort>(x => x.Lage), 3}
								                                           }
						                               }
				                               });
			return dataBundle;
		}

		[Test]
		public void EmptyDorf_Load_FillItWithNewOrtWithId() {
			Kernel.Inject.Rebind<IObjectStoreService<IEntity>>().To<EntityObjectStoreServiceMock<IEntity>>().InSingletonScope();
			Kernel.Inject.Rebind<IEntityService>().ToConstant(new EntityServiceMock	{
				OnInternalCreate = key => new Ort {Id = 4}
			}).InSingletonScope();
			ResolveFields();
						
			var context = GetEmptyDorf();
			var dataBundle = GetDefaultDataBundle();
			var typeMeta = GetTypeMetaFor<Dorf>();
			_contentStoreService.Load(context, dataBundle, typeMeta);

			var lage = context.Lage;
			Assert.IsNotNull(lage);
			FAssert.DoesNotThrow<NoIdSetException>(() => {
#pragma warning disable 168
				var test = lage.Id;
#pragma warning restore 168
			});
		}
		
		[Test]
		public void EmptyDorf_Load_CallCreateForCorrectSubEntity() {
			var signatures = new List<PType>();
			Kernel.Inject.Rebind<IObjectStoreService<IEntity>>().To<EntityObjectStoreServiceMock<IEntity>>().InSingletonScope();
			Kernel.Inject.Rebind<IEntityService>().ToConstant(new EntityServiceMock	{
				OnInternalCreate = key => {
					signatures.Add(key);
					return new Ort {Id = 4};
				}
			}).InSingletonScope();
			ResolveFields();
						
			var context = GetEmptyDorf();
			var dataBundle = GetDefaultDataBundle();
			var typeMeta = GetTypeMetaFor<Dorf>();
			_contentStoreService.Load(context, dataBundle, typeMeta);

			Assert.AreEqual(5, signatures.Count);
			var wanted = GetPTypeFor<Ort>();
			Assert.AreEqual(wanted, signatures.First());
		}
		#endregion

		private Dorf GetEmptyDorf()	{
			return new Dorf();
		}

		private Dorf GetTestDorfWithOrtThatHasId3() {
			return new Dorf	{
				Id = 1,
				Lage = new Ort{Id = 3}
			};
		}

		private TypeMeta GetTypeMetaFor<T>() {
			return GetTypeMetaFor(typeof (T));
		}

		private TypeMeta GetTypeMetaFor(Type type) {
			return _typeMetas.Entities.Single(x => x.Type == type);
		}

		private PType GetPTypeFor<T>() {
			return GetPTypeFor(typeof (T));
		}
		
		private PType GetPTypeFor(Type type) {
			return new PType(GetPTypeUId(type));
		}

		private PBasicEntityType GetPBasicEntityType(IEntity entity) {
			return new PBasicEntityType{Type = GetPTypeFor(entity.GetType()), DataType = DataType.Entity, Id = entity.Id};
		}

		private string GetMemberPTypeUId<TSource, TResult>(Expression<Func<TSource, TResult>> func) {
			return CN.Get(func);
		}
		
		private PType GetMemberPType<TSource, TResult>(Expression<Func<TSource, TResult>> func) {
			return new PType(GetMemberPTypeUId<TSource, TResult>(func));
		}
	}
}