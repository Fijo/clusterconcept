using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ClusterConcept.EntityFramework.Infrastructure.Interfaces;
using JetBrains.Annotations;

namespace ClusterConceptTest.EntityFramework {
	[UsedImplicitly]
	public class StringPrimitiveHandling : IPrimitiveHandling {
		#region Implementation of IPrimitiveHandling
		public string UId { get { return Type.Name; } }
		public Type Type { get { return typeof (string); } }
		public IEnumerable<byte> Serialize(object obj) {
			return Encoding.Default.GetBytes((string) obj);
		}

		public object Deserialize(IEnumerable<byte> bytes) {
			return Encoding.Default.GetString(bytes.ToArray());
		}
		#endregion
	}
}