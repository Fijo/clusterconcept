using ClusterConceptTest.Properties;
using FijoCore.Infrastructure.DependencyInjection.InitKernel;
using FijoCore.Infrastructure.DependencyInjection.InitKernel.Init;
using FijoCore.Infrastructure.ReferenceAssertion.Enums;
using FijoCore.Infrastructure.ReferenceAssertion.Services;
using NUnit.Framework;

namespace ClusterConceptTest.EntityFramework {
	[TestFixture]
	public class ReferenceTest : TestBase {
		private ReferenceAssertitionProvider _referenceAssertitionProvider;
		private ReferenceAssertitionVerifierService _referenceAssertitionVerifierService;

		public ReferenceTest() {
			new InternalInitKernel().Init();
			_referenceAssertitionProvider = Kernel.Resolve<ReferenceAssertitionProvider>();
			_referenceAssertitionVerifierService = Kernel.Resolve<ReferenceAssertitionVerifierService>();
		}

		[Test]
		public void FijoInfrastructureDependencyInjection_MustMatchRules() {
			_referenceAssertitionProvider.Assert(typeof(InitKernel), ReferenceAssertitionVerifier.DefinedContstants, "ReInject", true);
			_referenceAssertitionVerifierService.Verify(_referenceAssertitionProvider.GetAssertitions());
		}
	}
}