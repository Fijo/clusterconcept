using System;
using ClusterConcept.EntityFramework.Infrastructure.Service;
using ClusterConcept.EntityFramework.Model.Entity;
using ClusterConcept.EntityFramework.Model.Internal.Type.Meta;
using ClusterConcept.Shared.Model.ComEntity.Type;

namespace ClusterConceptTest.EntityFramework.Mocks {
	public class EntityServiceMock : IEntityService {
		public Func<long, EntityTypeMeta, IEntity> OnInternalGet { get; set; }
		public Func<PType, IEntity> OnInternalCreate { get; set; }

		#region Implementation of IEntityService
		public void Save(IEntity entity) {
			throw new NotImplementedException();
		}

		public void InternalSave(IEntity entity, TypeMeta typeMeta) {
			throw new NotImplementedException();
		}

		public IEntity Get(Type type, long id) {
			throw new NotImplementedException();
		}

		public IEntity InternalGet(long id, EntityTypeMeta entityTypeMeta) {
			return OnInternalGet(id, entityTypeMeta);
		}

		public IEntity InternalGet(long id, PType pType) {
			throw new NotImplementedException();
		}

		public IEntity Create(Type type) {
			throw new NotImplementedException();
		}

		public IEntity InternalCreate(PType pType) {
			throw new NotImplementedException();
		}

		public IEntity InternalCreate(EntityTypeMeta entityTypeMeta) {
			return OnInternalCreate(entityTypeMeta.PKey);
		}
		#endregion
	}
}