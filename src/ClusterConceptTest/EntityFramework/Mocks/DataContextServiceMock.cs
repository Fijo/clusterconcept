using System;
using ClusterConcept.EntityFramework.Infrastructure.Service;
using ClusterConcept.EntityFramework.Model.DataContext;
using ClusterConcept.EntityFramework.Model.Internal.Type.Meta;

namespace ClusterConceptTest.EntityFramework.Mocks {
	public class DataContextServiceMock : IDataContextService {
		public Func<ContextTypeMeta, IDataContext> OnGet { get; set; }
		
		#region Implementation of IDataContextService
		public void Save(IDataContext context) {
			throw new NotImplementedException();
		}

		public IDataContext Get(ContextTypeMeta typeMeta) {
			return OnGet(typeMeta);
		}
		#endregion
	}
}