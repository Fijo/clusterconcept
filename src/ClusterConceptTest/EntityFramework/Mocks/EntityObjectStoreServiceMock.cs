using System;
using System.Collections.Generic;
using ClusterConcept.EntityFramework.Infrastructure.Service.ObjectStore;
using ClusterConcept.EntityFramework.Model.Base;
using ClusterConcept.EntityFramework.Model.Internal.Type.Meta;
using FijoCore.Infrastructure.LightContrib.Module.Transaction;

namespace ClusterConceptTest.EntityFramework.Mocks {
	public class EntityObjectStoreServiceMock<T> : IObjectStoreService<T> where T : IBasicEntity {
		public Action<T> OnSave { get; set; }
		public Action<T> OnLoad { get; set; }

		#region Implementation of IObjectStoreService<in T>
		public void Save(T basicEntity, TypeMeta typeMeta, IList<StoreSet> toStoreSets) {
			if(OnSave != null) OnSave(basicEntity);
		}

		public void Load(T basicEntity, TypeMeta typeMeta) {
			if(OnLoad != null) OnLoad(basicEntity);
		}
		#endregion
	}
}