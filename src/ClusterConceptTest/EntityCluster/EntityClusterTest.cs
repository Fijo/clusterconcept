﻿using System.Linq;
using ClusterConcept.EntityFramework.Infrastructure.Service;
using ClusterConcept.EntityFramework.Infrastructure.Service.ObjectStore;
using ClusterConcept.EntityFramework.Model.Internal.Type.Meta;
using ClusterConcept.Shared.Infrastructure.Service.Exec;
using ClusterConceptTest.EntityFramework;
using ClusterConceptTest.EntityFramework.TestObjects;
using ClusterConceptTest.Properties;
using Fijo.Infrastructure.DesignPattern.Repository;
using Fijo.Net.Services.Server;
using FijoCore.Infrastructure.DependencyInjection.InitKernel;
using NUnit.Framework;

namespace ClusterConceptTest.EntityCluster {
	[TestFixture]
	public class EntityClusterTest : TestBase {
		private IDataContextService _dataContextService;
		private TypeMetas _typeMetas;

		[SetUp]
		public void SetUp() {
			var internalInitKernel = new ClusterClientInitKernel();
			internalInitKernel.Init();
			Kernel.Inject.Rebind<IDataAccessService>().To<ClusterBasedDataAccessService>().InSingletonScope();
			Kernel.Resolve<ISocketServer>().Start();
			Kernel.Resolve<IExecService>().HelloCluster(force: true);
			_dataContextService = Kernel.Resolve<IDataContextService>();
			_typeMetas = Kernel.Resolve<IRepository<TypeMetas>>().Get();
		}

		[Test]
		public void Test() {
			var pContextTypeFor = GetContextMetaFor<LocalContext>();
			var context = (LocalContext) _dataContextService.Get(pContextTypeFor);
			context.Dorf.Name = "Öhringen";
			context.Dorf.Einwohnerzahl = 12345;
			context.Dorf.Lage.Latlatitude = 92;
			context.Dorf.Lage.Longitude = 23;
			context.Dorf.NordGrenze.Latlatitude = 91;
			context.Dorf.NordGrenze.Longitude = 19;
			context.Dorf.SuedGrenze.Latlatitude = 92;
			context.Dorf.SuedGrenze.Longitude = 29;
			context.Dorf.WestGrenze.Latlatitude = 93;
			context.Dorf.WestGrenze.Longitude = 39;
			context.Dorf.OstGrenze.Latlatitude = 94;
			context.Dorf.OstGrenze.Longitude = 49;
			_dataContextService.Save(context);
		}

		[Test]
		public void Data_SaveChangeLoad_WorksAsRestore() {
			Test();
			SetUp();
			var pContextTypeFor = GetContextMetaFor<LocalContext>();
			var context = (LocalContext) _dataContextService.Get(pContextTypeFor);
			Assert.AreEqual("Öhringen", context.Dorf.Name);
			Assert.AreEqual(12345, context.Dorf.Einwohnerzahl);
			Assert.AreEqual(92, context.Dorf.Lage.Latlatitude);
			Assert.AreEqual(23, context.Dorf.Lage.Longitude);
			Assert.AreEqual(91, context.Dorf.NordGrenze.Latlatitude);
			Assert.AreEqual(19, context.Dorf.NordGrenze.Longitude);
			Assert.AreEqual(92, context.Dorf.SuedGrenze.Latlatitude);
			Assert.AreEqual(29, context.Dorf.SuedGrenze.Longitude);
			Assert.AreEqual(93, context.Dorf.WestGrenze.Latlatitude);
			Assert.AreEqual(39, context.Dorf.WestGrenze.Longitude);
			Assert.AreEqual(94, context.Dorf.OstGrenze.Latlatitude);
			Assert.AreEqual(49, context.Dorf.OstGrenze.Longitude);

		}

		private ContextTypeMeta GetContextMetaFor<T>() {
			var type = typeof (T);
			return _typeMetas.Contextes.Single(x => x.Type == type);
		}
	}
}