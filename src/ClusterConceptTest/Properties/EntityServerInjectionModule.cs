using ClusterConcept.EntityFramework.Infrastructure.Handlers;
using ClusterConcept.EntityServer.Infrastructure.Repositories;
using ClusterConcept.EntityServer.Infrastructure.Services;
using ClusterConcept.Shared.Infrastructure.Server.Com;
using ClusterConcept.Shared.Model.Node;
using ClusterConcept.Shared.Properties;
using Fijo.Infrastructure.DesignPattern.Repository;
using Fijo.Net.Nodelt.Handler;
using FijoCore.Infrastructure.DependencyInjection.Extentions.IKernel;
using FijoCore.Infrastructure.DependencyInjection.InitKernel.Base;
using FijoCore.Infrastructure.WrappedLibs.DependencyInjection.Interface;

namespace ClusterConceptTest.Properties {
	public class EntityServerInjectionModule : ExtendedNinjectModule {
		public override void AddModule(IKernel kernel) {
			kernel.Load(new EntityNodeSharedInjectionModule());
		}

		public override void OnLoad(IKernel kernel) {
			//kernel.Bind<IReadonlyHandler<IList<string>>>().To<Handler<IList<string>>>()

			//kernel.Bind<IRepository<InitNodeConfiguration>>().To<InitNodeConfigurationProvider>().InSingletonScope();
			//kernel.Bind<IRepository<NodeConfiguration>>().To<NodeConfigurationProvider>().InSingletonScope();

			//kernel.Bind<ISockProvider>().To<SockPool>().InSingletonScope();

			//kernel.Bind<INodeClientCom<NodeCommand>>().To<NodeClientCom<NodeCommand>>().InSingletonScope();

			//kernel.Bind<IHandlerProvider<NodeCommand>>().To<HandlerProvider<NodeCommand>>().InSingletonScope();

			//kernel.Bind<ISocketService>().To<SocketService>().InSingletonScope();
			//kernel.Bind<INodeServerCom>().To<NodeServerCom<NodeCommand>>().InSingletonScope();

			kernel.Bind<IRepository<NodeData>>().To<NodeDataRepository>().InSingletonScope();

			kernel.Bind<INodeDataService>().To<NodeDataService>().InSingletonScope();

    		kernel.Bind<IBaseHandler<NodeCommand>>().To<PostHandler>().InSingletonScope();
    		kernel.Bind<IBaseHandler<NodeCommand>>().To<GetHandler>().InSingletonScope();
		}
	}
}