using FijoCore.Infrastructure.DependencyInjection.InitKernel.Init;
using FijoCore.Infrastructure.LightContrib.Properties;
using FijoCore.Infrastructure.ReferenceAssertion.Properties;

namespace ClusterConceptTest.Properties {
	public class InternalInitKernel : ExtendedInitKernel {
		public override void PreInit() {
			LoadModules(new LightContribInjectionModule(), new EntityFrameworkClientInjectionModule(), new EntityServerInjectionModule(), new ReferenceAssertionInjectionModule());
		}
	}
}