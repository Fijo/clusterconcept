using FijoCore.Infrastructure.DependencyInjection.InitKernel.Init;
using FijoCore.Infrastructure.LightContrib.Properties;
using FijoCore.Infrastructure.ReferenceAssertion.Properties;

namespace ClusterConceptTest.Properties {
	public class ClusterClientInitKernel : ExtendedInitKernel {
		public override void PreInit() {
			LoadModules(new LightContribInjectionModule(), new EntityFrameworkClientInjectionModule(), new ReferenceAssertionInjectionModule());
		}
	}
}