﻿using System;
using System.Collections.Generic;
using System.Reflection;
using ClusterConcept.EntityFramework.Infrastructure.Factory;
using ClusterConcept.EntityFramework.Infrastructure.GenericLookups;
using ClusterConcept.EntityFramework.Infrastructure.Handlers;
using ClusterConcept.EntityFramework.Infrastructure.Interfaces;
using ClusterConcept.EntityFramework.Infrastructure.Providers;
using ClusterConcept.EntityFramework.Infrastructure.Providers.TypeMeta;
using ClusterConcept.EntityFramework.Infrastructure.Service;
using ClusterConcept.EntityFramework.Infrastructure.Service.ContentStore;
using ClusterConcept.EntityFramework.Infrastructure.Service.Creation;
using ClusterConcept.EntityFramework.Infrastructure.Service.Data.Access;
using ClusterConcept.EntityFramework.Infrastructure.Service.Data.Access.Spec;
using ClusterConcept.EntityFramework.Infrastructure.Service.Data.Bundle;
using ClusterConcept.EntityFramework.Infrastructure.Service.Event;
using ClusterConcept.EntityFramework.Infrastructure.Service.KeyResolver;
using ClusterConcept.EntityFramework.Infrastructure.Service.ObjectStore;
using ClusterConcept.EntityFramework.Infrastructure.Service.TypeMeta;
using ClusterConcept.EntityFramework.Model.DataContext;
using ClusterConcept.EntityFramework.Model.Entity;
using ClusterConcept.EntityFramework.Model.Internal.Type.Meta;
using ClusterConcept.EntityServer.Infrastructure.Interface;
using ClusterConcept.EntityServer.Infrastructure.Store;
using ClusterConcept.Shared.Infrastructure.Handler;
using ClusterConcept.Shared.Infrastructure.Server.Com;
using ClusterConcept.Shared.Model.Enums;
using ClusterConcept.Shared.Properties;
using ClusterConceptTest.EntityFramework;
using Fijo.Infrastructure.DesignPattern.Repository;
using Fijo.Infrastructure.DesignPattern.Repository.Collection;
using Fijo.Net.Nodelt.Handler;
using FijoCore.Infrastructure.DependencyInjection.Extentions.IKernel;
using FijoCore.Infrastructure.DependencyInjection.InitKernel.Base;
using FijoCore.Infrastructure.LightContrib.Module.Generics.Lookup;
using IKernel = FijoCore.Infrastructure.WrappedLibs.DependencyInjection.Interface.IKernel;

namespace ClusterConceptTest.Properties
{
    public class EntityFrameworkClientInjectionModule : ExtendedNinjectModule {
		public override void AddModule(IKernel kernel) {
			kernel.Load(new EntityNodeSharedInjectionModule());
		}

    	public override void OnLoad(IKernel kernel) {
    		base.OnLoad(kernel);
    		kernel.Bind<IGenericLookup<ITypeMetaServiceBase, DataType>>().To<TypeMetaServiceBaseLookup>().InSingletonScope();

    		kernel.Bind<ITypeMetaServiceBase>().To<ContextTypeMetaService>().InSingletonScope();
    		kernel.Bind<ITypeMetaServiceBase>().To<EntityTypeMetaService>().InSingletonScope();
    		kernel.Bind<ITypeMetaServiceBase>().To<PrimitiveTypeMetaService>().InSingletonScope();

    		kernel.Bind<IIdProvider>().To<SimpleIdProvider>().InSingletonScope();
    		kernel.Bind<IRepository<TypeMetas>>().To<TypeMetasRepository>().InSingletonScope();

    		kernel.Bind<IEntityFactory>().To<EntityFactory>().InSingletonScope();

    		kernel.Bind<IContentStoreService>().To<ContextContentStoreService>().InSingletonScope();
    		kernel.Bind<IContentStoreService>().To<EntityContentStoreService>().InSingletonScope();
    		kernel.Bind<IContentStoreService>().To<PrimitiveContentStoreService>().InSingletonScope();

    		kernel.Bind<IObjectStoreService<IEntity>>().To<EntityObjectStoreService>().InSingletonScope();
    		kernel.Bind<IObjectStoreService<IDataContext>>().To<ContextObjectStoreService>().InSingletonScope();

			kernel.Bind<IDataContextService>().To<DataContextService>().InSingletonScope();
    		kernel.Bind<IEntityService>().To<EntityService>().InSingletonScope();

    		kernel.Bind<IKeyResolver<Type>>().To<TypeKeyResolver>().InSingletonScope();
    		kernel.Bind<IKeyResolver<MemberInfo>>().To<MemberKeyResolver>().InSingletonScope();

    		kernel.Bind<IDataStore>().To<DataStore>().InSingletonScope();
    		kernel.Bind<IEntityStore>().To<EntityStore>().InSingletonScope();

    		kernel.Bind<IInternalDataAccessService>().To<InternalDataAccessService>().InSingletonScope();

    		kernel.Bind<IChangeEventService>().To<ChangeEventService>().InSingletonScope();

    		kernel.Bind<ICollectionRepository<KeyValuePair<Type, IPrimitiveHandling>>>().To<PrimitiveHandlingProvider>().InSingletonScope();

    		kernel.Bind<IPrimitiveHandling>().To<Int32PrimitiveHandling>().InSingletonScope();
    		kernel.Bind<IPrimitiveHandling>().To<StringPrimitiveHandling>().InSingletonScope();

    		kernel.Bind<IStoreSetService>().To<StoreSetService>().InSingletonScope();

    		kernel.Bind<ICreationService<IEntity>>().To<EntityCreationService>().InSingletonScope();

    		kernel.Bind<IObjectStateService>().To<ObjectStateService>().InSingletonScope();
    		kernel.Bind<IObjectsStateStock>().To<ObjectsStateStock>().InSingletonScope();

    		kernel.Bind<IDataAccessService>().To<FileBasedDataAccessService>().InSingletonScope();

    		kernel.Bind<IInternalDataAccessObjectsProvider>().To<InternalDataAccessObjectsProvider>().InSingletonScope();

    		kernel.Bind<IGenericLookup<IInternalDataAccessSpecObjectsProvider, DataType>>().To<InternalDataAccessSpecObjectsProviderLookup>().InSingletonScope();
    		kernel.Bind<IInternalDataAccessSpecObjectsProvider>().To<InternalDataAccessContextProvider>().InSingletonScope();
    		kernel.Bind<IInternalDataAccessSpecObjectsProvider>().To<InternalDataAccessEntityProvider>().InSingletonScope();

    		kernel.Bind<IDataBundleService>().To<DataBundleService>().InSingletonScope();
    		kernel.Bind<IDataBundleFactory>().To<DataBundleFactory>().InSingletonScope();

    		kernel.Bind<IBaseHandler<NodeCommand>>().To<FakePostHandler>().InSingletonScope();
    		kernel.Bind<IBaseHandler<NodeCommand>>().To<FakeGetHandler>().InSingletonScope();
    	}
    }
}