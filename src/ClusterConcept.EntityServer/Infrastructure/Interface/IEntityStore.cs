using ClusterConcept.Shared.Model;
using ClusterConcept.Shared.Model.ComEntity;
using ClusterConcept.Shared.Model.ComEntity.Type;
using JetBrains.Annotations;

namespace ClusterConcept.EntityServer.Infrastructure.Interface {
	public interface IEntityStore {
		[NotNull]
		PBasicEntity Get([NotNull] PEntityType type, long id);
		void Delete([NotNull] PEntityType type);
		void Delete([NotNull] PEntityType type, long id);
	}
}