using ClusterConcept.Shared.Model;
using ClusterConcept.Shared.Model.ComEntity;
using ClusterConcept.Shared.Model.ComEntity.Type;
using Fijo.Infrastructure.Documentation.Attributes.DesignPattern;
using JetBrains.Annotations;

namespace ClusterConcept.EntityServer.Infrastructure.Interface {
	[Store]
	public interface IDataStore {
		[NotNull]
		PBasicEntity Get([NotNull] PContextType key);
		void Delete([NotNull] PContextType key);
	}
}