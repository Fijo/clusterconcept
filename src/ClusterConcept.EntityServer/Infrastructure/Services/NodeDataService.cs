﻿using System.Collections.Generic;
using ClusterConcept.Shared.Model;
using ClusterConcept.Shared.Model.ComEntity.Type;
using ClusterConcept.Shared.Model.Node;
using Fijo.Infrastructure.DesignPattern.Events;
using Fijo.Infrastructure.DesignPattern.Repository;
using FijoCore.Infrastructure.DependencyInjection.InitKernel;
using FijoCore.Infrastructure.LightContrib.Extentions.IDictionary.Generic;
using FijoCore.Infrastructure.LightContrib.Extentions.IList.Generic;

namespace ClusterConcept.EntityServer.Infrastructure.Services {
	public class NodeDataService : INodeDataService {
		private readonly NodeData _nodeData = Kernel.Resolve<IRepository<NodeData>>().Get();

		#region Implementation of INodeDataService
		public void Post(PostQuery postQuery) {
			var key = postQuery.Key;
			GetEvents(key.Type, key.Id).AddRange(postQuery.Events);
		}

		public IList<IEvent> Get(GetQuery getQuery) {
			var key = getQuery.Key;
			return GetEvents(key.Type, key.Id);
		}
		#endregion

		private IList<IEvent> GetEvents(PType type, long id) {
			return _nodeData.Content.GetOrCreate(type, () => new Dictionary<long, IList<IEvent>>())
				.GetOrCreate(id, () => new List<IEvent>());
		}
	}
}