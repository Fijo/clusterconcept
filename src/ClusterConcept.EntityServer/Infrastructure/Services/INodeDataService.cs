using System.Collections.Generic;
using ClusterConcept.Shared.Model;
using Fijo.Infrastructure.DesignPattern.Events;

namespace ClusterConcept.EntityServer.Infrastructure.Services {
	public interface INodeDataService {
		void Post(PostQuery postQuery);
		IList<IEvent> Get(GetQuery getQuery);
	}
}