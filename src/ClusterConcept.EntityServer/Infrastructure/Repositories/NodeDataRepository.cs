﻿using System.Collections.Generic;
using ClusterConcept.Shared.Model.ComEntity.Type;
using ClusterConcept.Shared.Model.Node;
using Fijo.Infrastructure.DesignPattern.Events;
using Fijo.Infrastructure.DesignPattern.Repository;
using JetBrains.Annotations;

namespace ClusterConcept.EntityServer.Infrastructure.Repositories {
	[UsedImplicitly]
	public class NodeDataRepository : SingletonRepositoryBase<NodeData> {
		#region Overrides of SingletonRepositoryBase<NodeData>
		protected override NodeData Create() {
			return new NodeData
			{
				Content = new Dictionary<PType, IDictionary<long, IList<IEvent>>>()
			};
		}
		#endregion
	}
}