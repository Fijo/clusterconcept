using System.Collections.Generic;
using ClusterConcept.EntityServer.Infrastructure.Interface;
using ClusterConcept.Shared.Model;
using ClusterConcept.Shared.Model.ComEntity;
using ClusterConcept.Shared.Model.ComEntity.Type;
using FijoCore.Infrastructure.LightContrib.Extentions.IDictionary.Generic;

namespace ClusterConcept.EntityServer.Infrastructure.Store {
	public class DataStore : IDataStore {
		private readonly IDictionary<PContextType, PBasicEntity> _content = new Dictionary<PContextType, PBasicEntity>();

		public PBasicEntity Get(PContextType key) {
			return _content.GetOrCreate(key, () => new PBasicEntity());
		}
		
		public void Delete(PContextType type) {
			_content.TryGet(type, (me, key, value) => me.Remove(key));
		}
	}
}