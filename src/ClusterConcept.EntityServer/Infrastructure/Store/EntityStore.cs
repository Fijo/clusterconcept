using System.Collections.Generic;
using ClusterConcept.EntityServer.Infrastructure.Interface;
using ClusterConcept.Shared.Model;
using ClusterConcept.Shared.Model.ComEntity;
using ClusterConcept.Shared.Model.ComEntity.Type;
using FijoCore.Infrastructure.LightContrib.Extentions.IDictionary.Generic;
using JetBrains.Annotations;

namespace ClusterConcept.EntityServer.Infrastructure.Store {
	[UsedImplicitly]
	public class EntityStore : IEntityStore {
		private readonly IDictionary<PEntityType, IDictionary<long, PBasicEntity>> _content = new Dictionary<PEntityType, IDictionary<long, PBasicEntity>>();

		#region Implementation of IEntityStore
		public PBasicEntity Get(PEntityType type, long id) {
			return _content.GetOrCreate(type, () => new Dictionary<long, PBasicEntity>())
				.GetOrCreate(id, () => new PBasicEntity());
		}
		
		public void Delete(PEntityType type) {
			_content.TryGet(type, (me, key, value) => me.Remove(key));
		}

		public void Delete(PEntityType type, long id) {
			_content.TryGet(type, value => TryDelete(value, id));
		}

		private void TryDelete([NotNull] IDictionary<long, PBasicEntity> value, long id) {
			value.TryGet(id, (me, key, innerValue) => me.Remove(key));
		}
		#endregion
	}
}