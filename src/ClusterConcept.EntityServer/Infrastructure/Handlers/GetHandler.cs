using System.Collections.Generic;
using ClusterConcept.EntityServer.Infrastructure.Services;
using ClusterConcept.Shared.Infrastructure.Server.Com;
using ClusterConcept.Shared.Model;
using Fijo.Infrastructure.DesignPattern.Events;
using Fijo.Net.Nodelt.Handler;
using FijoCore.Infrastructure.DependencyInjection.InitKernel;
using JetBrains.Annotations;

namespace ClusterConcept.EntityFramework.Infrastructure.Handlers {
	[UsedImplicitly]
	public class GetHandler : Handler<GetQuery, IList<IEvent>, NodeCommand> {
		private readonly INodeDataService _nodeDataService = Kernel.Resolve<INodeDataService>();

		#region Overrides of Handler<GetQuery,IList<IEvent>,NodeCommand>
		public override IList<IEvent> Handle(GetQuery obj) {
			return _nodeDataService.Get(obj);
		}

		public override NodeCommand Command { get { return NodeCommand.Get; } }
		#endregion
	}
}