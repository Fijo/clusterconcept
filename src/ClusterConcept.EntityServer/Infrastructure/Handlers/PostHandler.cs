using ClusterConcept.EntityServer.Infrastructure.Services;
using ClusterConcept.Shared.Infrastructure.Server.Com;
using ClusterConcept.Shared.Model;
using Fijo.Net.Nodelt.Handler;
using FijoCore.Infrastructure.DependencyInjection.InitKernel;
using JetBrains.Annotations;

namespace ClusterConcept.EntityFramework.Infrastructure.Handlers {
	[UsedImplicitly]
	public class PostHandler : SendonlyHandler<PostQuery, NodeCommand> {
		private readonly INodeDataService _nodeDataService = Kernel.Resolve<INodeDataService>();

		#region Overrides of SendonlyHandler<PostQuery,NodeCommand>
		public override void Handle(PostQuery obj) {
			_nodeDataService.Post(obj);
		}

		public override NodeCommand Command { get { return NodeCommand.Post; } }
		#endregion
	}
}