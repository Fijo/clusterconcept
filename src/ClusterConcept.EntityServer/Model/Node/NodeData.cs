using System;
using System.Collections.Generic;
using ClusterConcept.Shared.Model.ComEntity.Type;
using Fijo.Infrastructure.DesignPattern.Events;
using Fijo.Infrastructure.Documentation.Attributes.DesignPattern;

namespace ClusterConcept.Shared.Model.Node {
	[Dto, Serializable]
	public class NodeData {
		public IDictionary<PType, IDictionary<long, IList<IEvent>>> Content;
	}
}