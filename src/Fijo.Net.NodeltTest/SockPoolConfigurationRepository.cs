using ClusterConcept.Shared.Model.Node;
using Fijo.Infrastructure.DesignPattern.Repository;
using Fijo.Net.Nodelt.Dto;
using FijoCore.Infrastructure.DependencyInjection.InitKernel;

namespace Fijo.Net.NodeltTest {
	public class SockPoolConfigurationRepository : RepositoryBase<SockPoolConfiguration> {
		#region Overrides of RepositoryBase<SockPoolConfiguration>
		public override SockPoolConfiguration Get() {
			var config = Kernel.Resolve<IRepository<InitNodeConfiguration>>().Get();
			return new SockPoolConfiguration
			{
				Content = config.NodeAddresses,
				ShouldActives = 1,
				MaintenanceIntervall = 60000,
				SocketUsagesLimitToRecycle = 12,
				AllowRetryConnectionFaildAfterMilliseconds = 15000,
				RetryDirectConnectWaitTime = 100,
				DirectConnectMaxRetryCount = 3
			};
		}
		#endregion
	}
}