using System.Collections.Generic;
using Fijo.Net.Nodelt.Handler;

namespace Fijo.Net.NodeltTest.TestClasses {
	public class PerfTestHandler : Handler<IEnumerable<int>, IEnumerable<int>, NodeCommand> {
		#region Overrides of Handler<IEnumerable<int>, IEnumerable<int>>
		public override IEnumerable<int> Handle(IEnumerable<int> obj) {
			return obj;
		}

		public override NodeCommand Command { get { return NodeCommand.PerfTest; } }
		#endregion
	}
}