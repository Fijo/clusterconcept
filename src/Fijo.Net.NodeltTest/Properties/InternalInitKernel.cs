﻿using Fijo.Net.Nodelt.Properties;
using FijoCore.Infrastructure.DependencyInjection.InitKernel.Init;

namespace Fijo.Net.NodeltTest.Properties {
	public class InternalInitKernel : ExtendedInitKernel {
		public override void PreInit() {
			LoadModules(new NetNodeltClientInjectionModule<NodeCommand>(), new NetNodeltServerInjectionModule<NodeCommand>());
		}
	}
}