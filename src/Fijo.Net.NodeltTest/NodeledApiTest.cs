﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Sockets;
using System.Threading;
using ClusterConcept.Shared.Model.Node;
using Fijo.Infrastructure.DesignPattern.Repository;
using Fijo.Net.Nodelt.Client.Com;
using Fijo.Net.Nodelt.Dto;
using Fijo.Net.Nodelt.Handler;
using Fijo.Net.Nodelt.Provider;
using Fijo.Net.NodeltTest.Properties;
using Fijo.Net.NodeltTest.TestClasses;
using Fijo.Net.Services.Server;
using FijoCore.Infrastructure.DependencyInjection.InitKernel;
using FijoCore.Infrastructure.LightContrib.Module.Serialization.Stream;
using NUnit.Framework;

namespace Fijo.Net.NodeltTest {
	[TestFixture(Description= "Nodelt API main initgration test")]
	public class NodeledApiTest {
		[SetUp]
		public void SetUp() {
			new InternalInitKernel().Init();
			Kernel.Inject.Bind<IStreamSerializer<NodeCommand>>().To<NodeCommandSerializer>().InSingletonScope();
			Kernel.Inject.Bind<IRepository<InitNodeConfiguration>>().To<InitNodeConfigurationProvider>().InSingletonScope();
			Kernel.Inject.Bind<IRepository<NodeServerConfig>>().To<NodeServerConfigRepository>().InSingletonScope();
			Kernel.Inject.Bind<IRepository<SockPoolConfiguration>>().To<SockPoolConfigurationRepository>().InSingletonScope();
		}

		[Test]
		public void ServerTest() {
			Kernel.Inject.Bind<IBaseHandler<NodeCommand>>().To<TestHandler>().InSingletonScope();
			
			var server = Kernel.Resolve<ISocketServer>();
			StartServer(server, () => {
				var client = Kernel.Resolve<INodeClientCom<NodeCommand>>();
				var provider = Kernel.Resolve<IHandlerProvider<NodeCommand>>();
				var handler = (IHandler<string, string, NodeCommand>) provider.Get(NodeCommand.Test);
				string actual;
				client.Send(handler, "Hallo Welt", out actual);
				Assert.AreEqual("Sie sagten Hallo Welt!", actual);
			});
		}

		[Test]
		public void PerfTest() {
			Kernel.Inject.Bind<IBaseHandler<NodeCommand>>().To<PerfTestHandler>().InSingletonScope();

			var server = Kernel.Resolve<ISocketServer>();
			StartServer(server, () => {
				var client = Kernel.Resolve<INodeClientCom<NodeCommand>>();
				var provider = Kernel.Resolve<IHandlerProvider<NodeCommand>>();
				var handler = (IHandler<IEnumerable<int>, IEnumerable<int>, NodeCommand>) provider.Get(NodeCommand.PerfTest);
				//2500000
				var length = 1500;
				var byteCount = length * 4;
				var totalByteCount = byteCount * 2;
				var clientThread = new Thread(() => {
					var range = Enumerable.Range(0, length);
					IEnumerable<int> enumerable;
					client.Send(handler, range, out enumerable);
				});
				var from = DateTime.Now.Ticks;
				clientThread.Start();
				while (clientThread.IsAlive) Thread.Sleep(10);
				var to = DateTime.Now.Ticks;
				var timeSpan = new TimeSpan((to - from) / totalByteCount * 1000000000000);
				Debug.WriteLine("{0} seconds per terabyte", timeSpan.ToString());
			});
		}

		private void StartServer(ISocketServer server, Action action) {
			InternalStartServer(server);
			try {
				action();
			}
			finally {
				InternalStopServer(server);
			}
		}

		private void InternalStopServer(ISocketServer server) {
			server.Stop();
		}

		private void InternalStartServer(ISocketServer server) {
			while (true) {
				try {
					server.Start();
					break;
				}
				catch (SocketException) {
					Thread.Sleep(100);
				}
			}
		}
	}
}
