using Fijo.Net.Nodelt.ImplBase;
using JetBrains.Annotations;

namespace Fijo.Net.NodeltTest {
	[UsedImplicitly]
	public class NodeCommandSerializer : DefaultNodeCommandSerializer<NodeCommand> {
		#region Overrides of DefaultNodeCommandSerializer<NodeCommand>
		protected override NodeCommand Convert(int value) {
			return (NodeCommand) value;
		}

		protected override int Convert(NodeCommand value) {
			return (int) value;
		}
		#endregion
	}
}