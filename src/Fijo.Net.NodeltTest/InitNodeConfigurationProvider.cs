using System.Linq;
using System.Net;
using ClusterConcept.Shared.Model.Node;
using Fijo.Infrastructure.DesignPattern.Repository;
using FijoCore.Infrastructure.DependencyInjection.InitKernel;
using FijoCore.Infrastructure.LightContrib.Extentions.Generic.IntoCollection;
using FijoCore.Infrastructure.LightContrib.Module.Converter.Simple.Interface;

namespace Fijo.Net.NodeltTest {
	public class InitNodeConfigurationProvider : SingletonRepositoryBase<InitNodeConfiguration> {
		protected override InitNodeConfiguration Create() {
			var endPointParser = Kernel.Resolve<IConverter<string, EndPoint>>();
			var ownAddress = endPointParser.Convert("127.0.0.1:2342");
			return new InitNodeConfiguration
			{
				NodeAddresses = ownAddress.IntoList(),
				OwnNodeAddress = ownAddress
			};
		}
	}
}