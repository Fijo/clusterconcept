using System;
using JetBrains.Annotations;

namespace ClusterConcept.EntityFramework.Model.Attributes {
	
	[AttributeUsage(AttributeTargets.All)] // TODO correct this
	public class UIdAttribute : Attribute {
		[NotNull] public string UId { get; protected set; }

		public UIdAttribute([NotNull] string uid) {
			UId = uid;
		}
	}
}