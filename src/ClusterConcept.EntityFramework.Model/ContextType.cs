using System;
using ClusterConcept.EntityFramework.Model.DataContext;

namespace ClusterConcept.EntityFramework.Model {
	public class ContextType : BasicEntityType	{
		public Func<IDataContext> Create;
		public IDataContext Context;
	}
}