using System;
using Fijo.Infrastructure.Documentation.Attributes.Info;

namespace ClusterConcept.EntityFramework.Model.Exceptions
{
	[Serializable, Desc("No Id had been set yet.")]
	public class NoIdSetException : InvalidOperationException	{
		public NoIdSetException() {

		}
		public NoIdSetException(string message) : base(message) {

		}
		public NoIdSetException(string message, Exception innerException) : base(message, innerException) {

		}
	}
}