//BS:Frei Wild - Das Land der Vollidioten (Cover)

using ClusterConcept.Shared.Model.ComEntity;
using Fijo.Infrastructure.Documentation.Attributes.DesignPattern;
using FijoCore.Infrastructure.LightContrib.Module.Transaction;

namespace ClusterConcept.EntityFramework.Model.Internal {
	[Dto]
	public class DataBundle {
		public PBasicEntity Data;
		public Transaction Chnageset;
		public ulong ObjectRevision;
	}
}