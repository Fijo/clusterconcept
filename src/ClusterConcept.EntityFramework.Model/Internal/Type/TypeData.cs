using System.Collections.Generic;
using System.Diagnostics;
using ClusterConcept.EntityFramework.Model.DataContext;
using ClusterConcept.EntityFramework.Model.Entity;
using ClusterConcept.Shared.Model.ComEntity.Type;
using ClusterConcept.Shared.Model.Enums;
using Fijo.Infrastructure.Documentation.Attributes.DesignPattern;

namespace ClusterConcept.EntityFramework.Model.Internal.Type {
	[Dto]
	public class TypeData {
		public IDataContext Context;
		public IDictionary<long, IEntity> Entities;
		public IDictionary<long, object> Primitives;
	}
}