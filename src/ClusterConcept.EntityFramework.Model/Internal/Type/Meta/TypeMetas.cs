using System.Collections.Generic;
using Fijo.Infrastructure.Documentation.Attributes.DesignPattern;

namespace ClusterConcept.EntityFramework.Model.Internal.Type.Meta {
	[Dto]
	public class TypeMetas {
		public IList<ContextTypeMeta> Contextes;
		public IList<EntityTypeMeta> Entities;
		public IList<PrimitiveTypeMeta> Primitives;
	}
}