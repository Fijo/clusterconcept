using System;
using ClusterConcept.EntityFramework.Model.DataContext;
using Fijo.Infrastructure.Documentation.Attributes.DesignPattern;

namespace ClusterConcept.EntityFramework.Model.Internal.Type.Meta {
	[Dto]
	public class ContextTypeMeta : TypeMeta {
		public Func<IDataContext> Create;
		public IDataContext Context;
	}
}