using System.Collections.Generic;
using ClusterConcept.Shared.Model.ComEntity.Type;
using ClusterConcept.Shared.Model.Enums;
using Fijo.Infrastructure.Documentation.Attributes.DesignPattern;

namespace ClusterConcept.EntityFramework.Model.Internal.Type.Meta {
	[Dto]
	public class TypeMeta {
		public System.Type Type;
		public PType PKey;
		public DataType DataType;
		public IList<PropertyMeta> Entities;
		public IList<PropertyMeta> Primitives;
		public IList<PropertyMeta> Contextes;
		public TypeData Data;
	}
}