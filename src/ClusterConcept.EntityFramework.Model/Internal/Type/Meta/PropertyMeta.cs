using ClusterConcept.Shared.Model.ComEntity.Type;
using Fijo.Infrastructure.DesignPattern.Accessor.Delegates;
using Fijo.Infrastructure.Documentation.Attributes.DesignPattern;

namespace ClusterConcept.EntityFramework.Model.Internal.Type.Meta {
	[Dto]
	public class PropertyMeta {
		public Setter<object, object> Setter;
		public Getter<object, object> Getter;
		public PType PKey;
		public TypeMeta TypeMeta;
	}
}