using System;
using ClusterConcept.EntityFramework.Model.Entity;
using Fijo.Infrastructure.Documentation.Attributes.DesignPattern;

namespace ClusterConcept.EntityFramework.Model.Internal.Type.Meta {
	[Dto]
	public class EntityTypeMeta : TypeMeta {
		public long CurrentId;
		public Func<IEntity> Create;
	}
}