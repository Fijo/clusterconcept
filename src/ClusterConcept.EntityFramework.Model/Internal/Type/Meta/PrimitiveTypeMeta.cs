using Fijo.Infrastructure.Documentation.Attributes.DesignPattern;

namespace ClusterConcept.EntityFramework.Model.Internal.Type.Meta {
	[Dto]
	public class PrimitiveTypeMeta : TypeMeta {}
}