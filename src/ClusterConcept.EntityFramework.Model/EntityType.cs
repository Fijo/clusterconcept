//BS:Sum 41 - Crazy Amanda Bunkface

using System;
using ClusterConcept.EntityFramework.Model.Entity;

namespace ClusterConcept.EntityFramework.Model
{
	public class EntityType : BasicEntityType {
		public long CurrentId;
		public Func<IEntity> Create;
	}
}