using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using FijoCore.Infrastructure.LightContrib.Default.Service.CheckName;
using FijoCore.Infrastructure.LightContrib.Extentions.Generic;
using FijoCore.Infrastructure.LightContrib.Module.Encoding;
using FijoCore.Infrastructure.LightContrib.Module.Stream;
using JetBrains.Annotations;
using ThreadState = System.Threading.ThreadState;

namespace Fijo.Net.CmdServer {
	[UsedImplicitly]
	public class SocketCmdServer : ISocketCmdServer {
		private readonly ISocketCmdHandler _cmdSocketHandler;
		private TcpListener _serverSocket;
		private Thread _cmdSocketConnectionAccepterThread;
		private readonly ISet<Thread> _listenerThreads = new HashSet<Thread>();
		private readonly IStreamService _streamService;
		private Encoding _encoding;
		private bool _isDisposed;

		public SocketCmdServer(ISocketCmdHandler cmdSocketHandler, IStreamService streamService, IEncodingProvider encodingProvider) {
			_cmdSocketHandler = cmdSocketHandler;
			_streamService = streamService;
			_encoding = encodingProvider.GetDefault();
		}

		public void Init(IPAddress ipAddress, int port) {
			CheckDisposed();
			if (_serverSocket != null) throw new InvalidOperationException(string.Format("The {0} is already inited. You can only call Init once.", CN.Get<SocketCmdServer>()));

			_serverSocket = new TcpListener(ipAddress, port);

			_cmdSocketConnectionAccepterThread = new Thread(Run){IsBackground = true, Name = string.Format("{0} ServerConnectionAccepter", CN.Get<SocketCmdServer>())};
		}

		protected void Run() {
			_serverSocket.Start();
			try {
				while (true) {
					var acceptSocket = _serverSocket.AcceptTcpClient();
					var thread = new Thread(() => ListenClient(acceptSocket)) {IsBackground = true, Name = string.Format("{0} ServerListener", CN.Get<SocketCmdServer>())};
					_listenerThreads.Add(thread);
					thread.Start();
				}
			}
			catch (ThreadAbortException) {
				Parallel.ForEach(_listenerThreads, x => x.Abort());
				_serverSocket.Stop();
			}
		}

		protected void ListenClient(TcpClient client) {
			try {
				using (var stream = client.GetStream()) {
					var clientContext = _cmdSocketHandler.GetClientContext();
					WriteToStream(stream, _cmdSocketHandler.Salutate(clientContext));

					while (clientContext.StayConnected) {
						// ToDo check if that works
						while (client.Available == 0) Thread.Sleep(100);
						var input = ReadToString(stream, client.Available);
						WriteToStream(stream, _cmdSocketHandler.Execute(input, clientContext));
					}

					WriteToStream(stream, _cmdSocketHandler.Shutdown(clientContext));
				}
				_listenerThreads.Remove(Thread.CurrentThread);
			}
			catch (ThreadAbortException) {
				client.Close();
			}
		}

		private void WriteToStream(Stream stream, string value) {
			if (string.IsNullOrEmpty(value)) return;
			_streamService.WriteToStream(stream, value);
		}

		private string ReadToString(Stream stream, int count) {
			if (count == 0) return string.Empty;
			var bytes = new byte[count];
			stream.Read(bytes, 0, count);
			return _encoding.GetString(bytes);
		}

		public void StartListen() {
			CheckDisposed();
			if (!IsInited()) throw new InvalidOperationException(string.Format("The {0} is has not been inited yet. Please call Init first.", CN.Get<SocketCmdServer>()));

			if (!_cmdSocketConnectionAccepterThread.ThreadState.HasFlag(ThreadState.Unstarted))
				throw new InvalidOperationException(string.Format("{0} has already been started. You can only start it once.", CN.Get<SocketCmdServer>()));

			_cmdSocketConnectionAccepterThread.Start();
		}

		#region Implementation of IDisposable
		public void Dispose() {
			CheckDisposed();
			_isDisposed = true;

			if (!IsInited()) return;
			Debug.Assert(new[]{ThreadState.Running, ThreadState.Aborted, ThreadState.AbortRequested, ThreadState.Unstarted}.Any(x => x.HasFlag(_cmdSocketConnectionAccepterThread.ThreadState)), "unexpected thread state");

			if (_cmdSocketConnectionAccepterThread.ThreadState.HasFlag(ThreadState.Running))
				_cmdSocketConnectionAccepterThread.Abort();
		}
		#endregion

		private bool IsInited() {
			return _serverSocket != null;
		}
		
		private void CheckDisposed() {
			if (_isDisposed) throw new ObjectDisposedException(CN.Get<SocketCmdServer>());
		}
	}
}