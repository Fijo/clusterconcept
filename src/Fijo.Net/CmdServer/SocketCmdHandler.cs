namespace Fijo.Net.CmdServer {
	public abstract class SocketCmdHandler<T> : ISocketCmdHandler where T : CmdClientContext {
		#region Implementation of ISocketCmdHandler
		public CmdClientContext GetClientContext() {
			return GetInternClientContext();
		}

		public string Salutate(CmdClientContext clientContext) {
			return Salutate((T) clientContext);
		}

		public string Execute(string value, CmdClientContext clientContext) {
			return Execute(value, (T) clientContext);
		}

		public string Shutdown(CmdClientContext clientContext) {
			return Shutdown((T) clientContext);
		}
		#endregion

		protected abstract T GetInternClientContext();

		protected abstract string Salutate(T clientContext);

		protected abstract string Execute(string value, T clientContext);

		protected abstract string Shutdown(T clientContext);
	}
}