using JetBrains.Annotations;

namespace Fijo.Net.CmdServer {
	[PublicAPI]
	public class CmdClientContext {
		[PublicAPI]
		public bool StayConnected = true;
	}
}