using System;
using System.Net;

namespace Fijo.Net.CmdServer {
	public interface ISocketCmdServer : IDisposable {
		void Init(IPAddress ipAddress, int port);
		void StartListen();
	}
}