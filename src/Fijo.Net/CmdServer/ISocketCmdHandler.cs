using JetBrains.Annotations;

namespace Fijo.Net.CmdServer {
	[PublicAPI]
	public interface ISocketCmdHandler {
		CmdClientContext GetClientContext();

		string Salutate(CmdClientContext clientContext);

		string Execute(string value, CmdClientContext clientContext);

		string Shutdown(CmdClientContext clientContext);
	}
}