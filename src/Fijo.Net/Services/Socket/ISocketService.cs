using System.IO;
using JetBrains.Annotations;
using SNet = System.Net.Sockets;

namespace Fijo.Net.Services.Socket {
	public interface ISocketService {
		[NotNull] Stream GetStream([NotNull] SNet.Socket socket);
	}
}