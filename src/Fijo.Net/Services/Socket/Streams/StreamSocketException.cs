using System;

namespace Fijo.Net.Services.Socket.Streams {
	[Serializable]
	public class StreamSocketException : SocketException {
		public StreamSocketException() {}

		public StreamSocketException(int errorCode) : base(errorCode) {}

		public StreamSocketException(string message) : base(message) {}

		public StreamSocketException(string message, Exception innerException) : base(message, innerException) {}
	}
}