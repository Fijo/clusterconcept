using System;
using System.IO;
using FijoCore.Infrastructure.LightContrib.Default.Service.Out.Interface;
using FijoCore.Infrastructure.LightContrib.Service.ExecptionFormatter;
using JetBrains.Annotations;
using SNet = System.Net.Sockets;
using System.Diagnostics;

namespace Fijo.Net.Services.Socket.Streams {
	[PublicAPI]
	public class SocketStream : Stream {
		#region Constant & Static
		protected const byte SpecialByte = 255;
		protected static readonly byte[] EscapedSpecialByte = new[] {(byte) SpecialBytes.Escaped};
		protected static readonly byte[] EndBuffer = new[] {SpecialByte, (byte) SpecialBytes.End};
		#endregion

		private readonly IOut _out;
		private readonly IExceptionFormatter _exceptionFormatter;
		protected readonly SNet.NetworkStream InnerStream;
		protected readonly object WriteLock = new object();
		protected readonly object ReadLock = new object();

		protected byte ReadCurrent;
		protected bool IsReadEnd;
		protected byte[] ReadBuffer = new byte[1];

		protected long WritePosition;
		protected bool IsWriteEnd;
		
		#region Subclasses
		protected enum SpecialBytes : byte {
			End = 8,
			Escaped = 255
		}
		#endregion

		public SocketStream(SNet.NetworkStream innerStream, IOut @out, IExceptionFormatter exceptionFormatter) {
			InnerStream = innerStream;
			_out = @out;
			_exceptionFormatter = exceptionFormatter;
		}

		#region Overrides of Stream
		public override void Flush() {
			InnerStream.Flush();
			LogFlush();
		}

		#region Seek
		protected virtual void SendEnd() {
			lock (WriteLock) {
				#region PreCondition
				Debug.Assert(!IsWriteEnd, "illegal call of SendEnd - SocketStream writing already ended.");
				Debug.Assert(EndBuffer.Length == 2);
				#endregion
				InnerStream.Write(EndBuffer, 0, 2);
			}
		}

		public override long Seek(long offset, SeekOrigin origin) {
			lock(WriteLock) {
				if (offset != 0 || origin != SeekOrigin.End) return InnerStream.Seek(offset, origin);
				CheckWriteEnd();
				SendEnd();
				Flush();
				LogSendEnd();
				IsWriteEnd = true;
				return WritePosition;
			}
		}
		#endregion

		#region WriteEnd
		private void CheckWriteEnd() {
			if (GetIsWriteEnd()) HandleWriteEnd();
		}

		protected virtual bool GetIsWriteEnd() {
			return IsWriteEnd;
		}

		[TerminatesProgram]
		protected virtual void HandleWriteEnd() {
			throw new EndOfStreamException();
		}
		#endregion

		#region ReadEnd
		protected virtual bool GetIsReadEnd() {
			return IsReadEnd;
		}
		#endregion

		public override void SetLength(long value) {
			InnerStream.SetLength(value);
		}
		
		#region Read
		protected bool ReadNext() {
			lock(ReadLock) {
				if (GetIsReadEnd()) return false;
				byte current;
				if (!TryGetCurrentRead(out current)) return false;
				if (current == SpecialByte) {
					if (!TryGetCurrentRead(out current)) return false;
					if (current != (byte) SpecialBytes.Escaped) {
						CheckSpecialByte(current);
						return ReadNext();
					}
				}
				ReadCurrent = current;
				return true;
			}
		}

		protected virtual void CheckSpecialByte(byte current) {
			lock(ReadLock) {
				if (current == (byte) SpecialBytes.End)
					IsReadEnd = true;
			}
		}

		private bool TryGetCurrentRead(out byte result) {
			lock(ReadLock) {
				try {
					int read;
					read = InnerStream.ReadByte();
					if(read == -1) return _out.False(out result);
					return _out.True(out result, (byte) read);
				}
				catch(IOException e) {
					Trace.WriteLine(string.Format("Reading from remote socket faild. {0}", _exceptionFormatter.Format(e)));
					//may do not alway handle that in this way
					IsReadEnd = true;
					return _out.False(out result);
				}
			}
		}

		public override int Read(byte[] buffer, int offset, int count) {
			#region PreCondtion
			if (buffer == null) throw new ArgumentNullException("buffer");
			if(offset + count > buffer.Length) throw new ArgumentOutOfRangeException("offset", offset, "offset + count have to be smaller or equal than buffer.Length");
			if(offset < 0) throw new ArgumentOutOfRangeException("offset", offset, "must be at least 0");
			if(count < 0) throw new ArgumentOutOfRangeException("count", count, "must be at least 0");
			#endregion
			if(count == 0) return PassReadResult(0);
			lock(ReadLock) {
				var startOffset = offset;
				var end = offset + count;
				while (ReadNext()) {
					buffer[offset++] = ReadCurrent;
					if (offset == end) return PassReadResult(count);
				}
				return PassReadResult(offset - startOffset);
			}
		}

		private int PassReadResult(int count) {
			LogRead(count);
			return count;
		}
		#endregion

		#region Write
		public override void Write(byte[] buffer, int offset, int count) {
			#region PreCondtion
			if (buffer == null) throw new ArgumentNullException("buffer");
			if(offset + count > buffer.Length) throw new ArgumentOutOfRangeException("offset", offset, "offset + count have to be smaller or equal than buffer.Length");
			if(offset < 0) throw new ArgumentOutOfRangeException("offset", offset, "must be at least 0");
			if(count < 0) throw new ArgumentOutOfRangeException("count", count, "must be at least 0");
			#endregion
			CheckWriteEnd();
			lock(WriteLock) {
				WritePosition += count;
				var startOffset = offset;
				for (var i = offset; i < count; i++) {
					var current = buffer[i];
					if (current != SpecialByte) continue;
					var newOffset = i + 1;
					InnerWrite(buffer, offset, newOffset - offset);
					offset = newOffset;
					InnerWrite(EscapedSpecialByte, 0, 1);
				}
				if (offset != count) InnerWrite(buffer, offset, count - offset - startOffset);
				LogWrite(count);
			}
		}

		private void InnerWrite(byte[] buffer, int offset, int size) {
			try {
				InnerStream.Write(buffer, offset, size);
			}
			catch(IOException e) {
				throw new StreamSocketException(e.Message, e);
			}
		}
		#endregion

		public override bool CanRead { get { return InnerStream.CanRead; } }
		public override bool CanSeek { get { return InnerStream.CanSeek; } }
		public override bool CanWrite { get { return InnerStream.CanWrite; } }
		public override long Length { get { return InnerStream.Length; } }
		public override long Position { get { return WritePosition; } set { throw new NotSupportedException(); } }
		#endregion

		#region Logging
		[Conditional("TraceSocketStream")]
		private void LogSendEnd() {
			#if TraceSocketStream
			Trace.WriteLine("send end");
			#endif
		}

		[Conditional("TraceSocketStream")]
		private void LogFlush() {
			#if TraceSocketStream
			Trace.WriteLine("flushed");
			#endif
		}

		[Conditional("TraceSocketStream")]
		private void LogRead(int count) {
			#if TraceSocketStream
			Trace.WriteLine(count + " bytes read");
			#endif
		}

		[Conditional("TraceSocketStream")]
		private void LogWrite(int count) {
			#if TraceSocketStream
			Trace.WriteLine(count + " bytes send");
			#endif
		}
		#endregion
	}

	[Serializable]
	public class SocketException : SNet.SocketException {
		private readonly string _message;
		public override string Message { get { return (string.IsNullOrEmpty(_message) ? string.Empty : _message + Environment.NewLine) + base.Message; } }
		public new Exception InnerException { get; private set; }

		public SocketException() {}

		public SocketException(int errorCode) : base(errorCode) {}

		public SocketException(string message) {
			_message = message;
		}

		public SocketException(string message, Exception innerException) {
			_message = message;
			InnerException = innerException;
		}
	}
}