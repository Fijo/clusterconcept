using System.IO;
using Fijo.Net.Services.Socket.Streams;
using FijoCore.Infrastructure.LightContrib.Default.Service.Out.Interface;
using FijoCore.Infrastructure.LightContrib.Service.ExecptionFormatter;
using JetBrains.Annotations;
using SNet = System.Net.Sockets;

namespace Fijo.Net.Services.Socket {
	[UsedImplicitly]
	public class SocketService : ISocketService {
		private readonly IOut _out;
		private readonly IExceptionFormatter _exceptionFormatter;

		public SocketService(IOut @out, IExceptionFormatter exceptionFormatter) {
			_out = @out;
			_exceptionFormatter = exceptionFormatter;
		}

		#region Implementation of ISocketService
		public Stream GetStream(SNet.Socket socket) {
			return new SocketStream(new SNet.NetworkStream(socket), _out, _exceptionFormatter);
		}
		#endregion
	}
}