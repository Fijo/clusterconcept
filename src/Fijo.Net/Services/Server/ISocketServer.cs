using System;
using System.Net;
using System.Net.Sockets;

namespace Fijo.Net.Services.Server {
	public interface ISocketServer : IDisposable {
		void Init(EndPoint address, AddressFamily addressFamily = AddressFamily.InterNetwork, SocketType socketType = SocketType.Stream, ProtocolType protocolType = ProtocolType.Tcp);
		void Start();
		void Stop();
	}
}