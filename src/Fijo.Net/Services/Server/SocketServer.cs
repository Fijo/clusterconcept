using System;
using System.Diagnostics;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using FijoCore.Infrastructure.DependencyInjection.InitKernel;
using FijoCore.Infrastructure.LightContrib.Service.ExecptionFormatter;

namespace Fijo.Net.Services.Server {
	public abstract class SocketServer : ISocketServer {
		private readonly IExceptionFormatter _exceptionFormatter = Kernel.Resolve<IExceptionFormatter>();
		private EndPoint _address;
		private System.Net.Sockets.Socket _listener;
		private readonly ManualResetEvent _allDone = new ManualResetEvent(false);
		private Thread _serverThread;
		private bool _listen = true;

		public virtual void Init(EndPoint address, AddressFamily addressFamily = AddressFamily.InterNetwork, SocketType socketType = SocketType.Stream, ProtocolType protocolType = ProtocolType.Tcp) {
			_address = address;
			_listener = new System.Net.Sockets.Socket(addressFamily, socketType, protocolType);
			_serverThread = new Thread(ThreadStart) {IsBackground = true, Name = string.Format("{0} ServerListener", typeof(SocketServer).Name)};
		}

		public virtual void Start() {
			EnsureInited();
			_serverThread.Start();
		}

		protected void ThreadStart() {
			_listener.Bind(_address);
			_listener.Listen(100);
			while (_listen) {
				_allDone.Reset();
				_listener.BeginAccept(AcceptCallback, _listener);
				_allDone.WaitOne();
			}
		}

		protected virtual void AcceptCallback(IAsyncResult ar) {
			_allDone.Set();
			
			var currentListener = (System.Net.Sockets.Socket) ar.AsyncState;
			var currentHandler = currentListener.EndAccept(ar);
			try {
				Handle(currentHandler);
			}
			catch(Exception e) {
				Trace.WriteLine(string.Format("{0} faild to handle incomeing client IO. {1}", typeof (SocketServer).Name, _exceptionFormatter.Format(e)));
			}
			currentHandler.Shutdown(SocketShutdown.Both);
			currentHandler.Close();
		}

		private void EnsureInited() {
			if (_serverThread == null) throw new InvalidOperationException("You have to call init before other functions");
		}

		public virtual void Stop() {
			EnsureInited();
			_listen = false;
			_serverThread.Interrupt();
			Shutdown();
			_listener.Close();
		}

		protected virtual void Shutdown() {}

		protected abstract void Handle(System.Net.Sockets.Socket handler);

		#region Implementation of IDisposable
		public void Dispose() {
			Stop();
			_listener.Dispose();
			_allDone.Dispose();
		}
		#endregion
	}
}