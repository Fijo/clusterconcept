using Fijo.Net.CmdServer;
using Fijo.Net.Services.Socket;
using FijoCore.Infrastructure.DependencyInjection.Extentions.IKernel;
using FijoCore.Infrastructure.DependencyInjection.InitKernel.Base;
using FijoCore.Infrastructure.LightContrib.Properties;
using IKernel = FijoCore.Infrastructure.WrappedLibs.DependencyInjection.Interface.IKernel;

namespace Fijo.Net.Properties {
	public class NetInjectionModule : ExtendedNinjectModule {
		public override void AddModule(IKernel kernel) {
			kernel.Load(new LightContribInjectionModule());
		}

		public override void OnLoad(IKernel kernel) {
			kernel.Bind<ISocketCmdServer>().To<SocketCmdServer>().InSingletonScope();

			kernel.Bind<ISocketService>().To<SocketService>().InSingletonScope();
		}
	}
}