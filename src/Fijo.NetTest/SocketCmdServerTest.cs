﻿using System.Net;
using System.Threading;
using Fijo.Net.CmdServer;
using Fijo.NetTest.Properties;
using FijoCore.Infrastructure.DependencyInjection.InitKernel;
using NUnit.Framework;

namespace Fijo.NetTest {
	[TestFixture]
	public class SocketCmdServerTest {
		class TestSocketCmdHandler : ISocketCmdHandler {
			#region Implementation of ISocketCmdHandler
			public CmdClientContext GetClientContext() {
				return new CmdClientContext();
			}

			public string Salutate(CmdClientContext clientContext) {
				return "Hello Client!";
			}

			public string Execute(string value, CmdClientContext clientContext) {
				switch (value.Length <= 5 ? value : value.Substring(0, 5)) {
					case "say..":
						return GetMessage(value);
					case "bye..":
						clientContext.StayConnected = false;
						return string.Empty;
					default:
						return "Invalid Command";
				}
			}

			private string GetMessage(string value) {
				return value.Substring(5);
			}

			public string Shutdown(CmdClientContext clientContext) {
				return "Goodbye";
			}
			#endregion
		}

		private ISocketCmdServer _socketCmdServer;

		[SetUp]
		public void SetUp() {
			new InternalInitKernel().Init();
			Kernel.Inject.Bind<ISocketCmdHandler>().To<TestSocketCmdHandler>().InSingletonScope();
			_socketCmdServer = Kernel.Resolve<ISocketCmdServer>();
		}

		[Test, Ignore]
		public void Test() {
			using (_socketCmdServer) {
				_socketCmdServer.Init(IPAddress.Any, 40);
				_socketCmdServer.StartListen();
				Thread.Sleep(int.MaxValue);
			}
		}
	}
}