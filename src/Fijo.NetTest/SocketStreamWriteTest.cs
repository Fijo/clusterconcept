using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Threading;
using Fijo.Net.Services.Socket.Streams;
using Fijo.NetTest.Properties;
using FijoCore.Infrastructure.DependencyInjection.InitKernel;
using FijoCore.Infrastructure.LightContrib.Default.Service.Out.Interface;
using FijoCore.Infrastructure.LightContrib.Service.ExecptionFormatter;
using NUnit.Framework;

namespace Fijo.NetTest {
	[TestFixture(typeof(SocketStream))]
	public class SocketStreamWriteTest : SocketTestBase {
		private IOut _out;
		private IExceptionFormatter _exceptionFormatter;

		[SetUp]
		public void SetUp() {
			new InternalInitKernel().Init();
			_out = Kernel.Resolve<IOut>();
			_exceptionFormatter = Kernel.Resolve<IExceptionFormatter>();
			
		}

		private SocketStream GetSocketStream(Socket connection) {
			return new SocketStream(new NetworkStream(connection), _out, _exceptionFormatter);
		}

		private byte[] GetSocketEnd() {
			return new[] {byte.MaxValue, (byte) 8};
		}

		private byte[] GetBufferRange(int length) {
			return Enumerable.Range(0, length).Select(x => (byte) x).ToArray();
		}

		[TestCase(1)]
		[TestCase(8)]
		[TestCase(50)]
		[TestCase(120)]
		public void Socket_Write_WorksAsExpected(int length) {
			#region PreCondtition
			Debug.Assert(length > 0);
			Debug.Assert(length < 200);
			#endregion
			var bytes = GetBufferRange(length);
			TestBase(connection => ReciveHandling(connection, bytes),
			client => {
				var stream = GetSocketStream(client);
				stream.Write(bytes, 0, bytes.Length);
				stream.Flush();
			});
		}

		private void ReciveHandling(Socket connection, byte[] wanted) {
			var size = wanted.Length;
			var buffer = new byte[size];
			connection.Receive(buffer, size, SocketFlags.None);
			CollectionAssert.AreEqual(wanted, buffer);
		}
		
		[TestCase(1)]
		[TestCase(8)]
		[TestCase(50)]
		[TestCase(99)]
		public void Socket_Flush_FlushesBufferAsExcepted(int length) {
			#region PreCondtition
			Debug.Assert(length > 0);
			Debug.Assert(length < 100);
			#endregion
			var bytes = GetBufferRange(length);
			var partRead = new ManualResetEvent(false);
			TestBase(connection => {
				ReciveHandling(connection, bytes);
				partRead.Set();
				ReciveHandling(connection, bytes);
			},
			client => {
				var stream = GetSocketStream(client);
				stream.Write(bytes, 0, bytes.Length);
				stream.Flush();
				partRead.WaitOne();
				stream.Write(bytes, 0, bytes.Length);
			});
		}
		
		[TestCase(1)]
		[TestCase(8)]
		[TestCase(50)]
		[TestCase(99)]
		public void Socket_Seek_SendsBufferAsExcepted(int length) {
			#region PreCondtition
			Debug.Assert(length > 0);
			Debug.Assert(length < 100);
			#endregion
			var bytes = GetBufferRange(length);
			var wanted = bytes.Concat(GetSocketEnd()).ToArray();
			var allWritten = new ManualResetEvent(false);
			TestBase(connection => {
				allWritten.WaitOne();
				ReciveHandling(connection, wanted);
			},
			client => {
				var stream = GetSocketStream(client);
				stream.Write(bytes, 0, bytes.Length);
				stream.Seek(0, SeekOrigin.End);
				allWritten.Set();
			});
		}

		[Test]
		public void SocketAtEnd_Write_MustThrowEndOfStreamException() {
			SocketAtEndMustThrowEndOfStreamException((stream, bytes) => stream.Write(bytes, 0, bytes.Length));
		}

		[Test]
		public void SocketAtEnd_Seek0End_MustThrowEndOfStreamException() {
			SocketAtEndMustThrowEndOfStreamException((stream, bytes) => stream.Seek(0, SeekOrigin.End));
		}

		private void SocketAtEndMustThrowEndOfStreamException(Action<Stream, byte[]> failAction) {
			var length = 100;
			var bytes = GetBufferRange(length);
			var allWritten = new ManualResetEvent(false);
			TestBase(connection => {
				allWritten.WaitOne();
				DefaultReciveHandling(length, connection);
			},
			client => {
				var stream = GetSocketStream(client);
				stream.Write(bytes, 0, bytes.Length);
				stream.Seek(0, SeekOrigin.End);
				try {
					Assert.Throws<EndOfStreamException>(() => failAction(stream, bytes));
				}
				finally {
					allWritten.Set();
				}
			});
		}

		[TestCase(0, SeekOrigin.Begin)]
		[TestCase(1, SeekOrigin.Begin)]
		[TestCase(-1, SeekOrigin.Begin)]
		[TestCase(0, SeekOrigin.Current)]
		[TestCase(1, SeekOrigin.Current)]
		[TestCase(-1, SeekOrigin.Current)]
		[TestCase(1, SeekOrigin.End)]
		[TestCase(-1, SeekOrigin.End)]
		public void Socket_SeekNotToEnd_MustThrowNotSupportedException(long offset, SeekOrigin origin) {
			var length = 100;
			var bytes = GetBufferRange(length);
			TestBase(connection => DefaultReciveHandling(length, connection),
			         client => {
				         var stream = GetSocketStream(client);
				         stream.Write(bytes, 0, bytes.Length);
				         Assert.Throws<NotSupportedException>(() => stream.Seek(offset, origin));
			         });
		}
		
		[Test]
		public void Socket_Write_SetPositionToTheCorrectValue() {
			var length = 100;
			var bytes = GetBufferRange(length);
			TestBase(connection => DefaultReciveHandling(length, connection),
			         client => {
				         var stream = GetSocketStream(client);
				         var count = bytes.Length;
				         stream.Write(bytes, 0, count);
				         Assert.AreEqual(stream.Position, count);
				         stream.Flush();
			         });
		}
		
		[Test]
		public void Socket_SetPosition_MustThrowNotSupportedException() {
			var length = 100;
			var bytes = GetBufferRange(length);
			TestBase(connection => DefaultReciveHandling(length, connection),
			         client => {
				         var stream = GetSocketStream(client);
				         var count = bytes.Length;
				         stream.Write(bytes, 0, count);
				         Assert.Throws<NotSupportedException>(() => stream.Position = 3);
			         });
		}

		private void DefaultReciveHandling(int length, Socket connection) {
			var buffer = new byte[length];
			connection.Receive(buffer, length, SocketFlags.None);
		}
	}
}