using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Threading;
using Fijo.Net.Services.Socket.Streams;
using Fijo.NetTest.Properties;
using FijoCore.Infrastructure.DependencyInjection.InitKernel;
using FijoCore.Infrastructure.LightContrib.Default.Service.Out.Interface;
using FijoCore.Infrastructure.LightContrib.Service.ExecptionFormatter;
using NUnit.Framework;

namespace Fijo.NetTest {
	[TestFixture(typeof(SocketStream))]
	public class SimpleSocketStreamTest : SocketTestBase {
		private IOut _out;
		private IExceptionFormatter _exceptionFormatter;

		[SetUp]
		public void SetUp() {
			new InternalInitKernel().Init();
			_out = Kernel.Resolve<IOut>();
			_exceptionFormatter = Kernel.Resolve<IExceptionFormatter>();
		}

		[Test]
		public void Socket_ReadWrite_MustReturnSameByteArray() {
			TestBase(connection => {
				var length = 30;
				var bytes = Enumerable.Range(byte.MinValue, byte.MaxValue).Select(x => (byte) x);
				var stream = new SocketStream(new NetworkStream(connection), _out, _exceptionFormatter);
				foreach (var i in Enumerable.Range(0, length)) {
					Thread.Sleep(345);
					foreach (var q in Enumerable.Range(0, length)) {
						var buffer = new byte[byte.MaxValue];
						stream.Read(buffer, 0, byte.MaxValue);
						CollectionAssert.AreEqual(bytes.ToArray(), buffer);
					}
				}
				var test = new byte[byte.MaxValue];
				Assert.AreEqual(0, stream.Read(test, 0, byte.MaxValue));
				CollectionAssert.AreEqual(new byte[byte.MaxValue], test);
			},
			client => {
				var length = 30;
				var bytes = Enumerable.Range(byte.MinValue, byte.MaxValue).Select(x => (byte) x);
				var stream = new SocketStream(new NetworkStream(client), _out, _exceptionFormatter);
				foreach (var i in Enumerable.Range(0, length)) {
					foreach (var q in Enumerable.Range(0, length)) {
						var buffer = bytes.ToArray();
						stream.Write(buffer, 0, buffer.Length);
					}
				}
				stream.Seek(0, SeekOrigin.End);
			});
		}
	}
}