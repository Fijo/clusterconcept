using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Threading;

namespace Fijo.NetTest {
	public class SocketTestBase {
		private static int _testRun;

		protected void TestBase(Action<Socket> connectionCallback, Action<Socket> clientCallback) {
			var local = GetLocal();
			var exceptions = new List<Exception>();
			var serverReady = new ManualResetEvent(false);
			var serverClosed = new ManualResetEvent(false);
			var clientClosed = new ManualResetEvent(false);
			var serverThread = new Thread(WrapWithTry(exceptions, () => {
				var server = GetSocket();
				server.Bind(local);
				server.Listen(10);
				serverReady.Set();
				var connection = server.Accept();
				connectionCallback(connection);
				connection.Shutdown(SocketShutdown.Both);
				connection.Close();
				serverClosed.Set();
			})) {Name = "TestServer"};
			var clientThread = new Thread(WrapWithTry(exceptions, () => {
				serverReady.WaitOne();
				var client = GetSocket();
				client.Connect(local);
				clientCallback(client);
				serverClosed.WaitOne();
				client.Shutdown(SocketShutdown.Both);
				client.Close();
				clientClosed.Set();
			})) {Name = "TestClient"};
			serverThread.Start();
			clientThread.Start();
			clientClosed.WaitOne();
			foreach (var exception in exceptions)
				throw exception;
		}

		private ThreadStart WrapWithTry(IList<Exception> exceptions, Action action) {
			return () => {
				try {
					action();
				}
				catch (Exception e) {
					exceptions.Add(e);
				}
			};
		}

		private IPEndPoint GetLocal() {
			return new IPEndPoint(IPAddress.Parse("127.0.0.1"), GetPort());
		}

		private int GetPort() {
			return 8180 + _testRun++;
		}

		private static Socket GetSocket() {
			return new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
		}
	}
}