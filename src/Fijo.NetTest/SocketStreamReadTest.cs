using System.Diagnostics;
using System.Linq;
using System.Net.Sockets;
using Fijo.Net.Services.Socket.Streams;
using Fijo.NetTest.Properties;
using FijoCore.Infrastructure.DependencyInjection.InitKernel;
using FijoCore.Infrastructure.LightContrib.Default.Service.Out.Interface;
using FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic;
using FijoCore.Infrastructure.LightContrib.Service.ExecptionFormatter;
using NUnit.Framework;

namespace Fijo.NetTest {
	[TestFixture(typeof(SocketStream))]
	public class SocketStreamReadTest : SocketTestBase {
		private IOut _out;
		private IExceptionFormatter _exceptionFormatter;

		[SetUp]
		public void SetUp() {
			new InternalInitKernel().Init();
			_out = Kernel.Resolve<IOut>();
			_exceptionFormatter = Kernel.Resolve<IExceptionFormatter>();
		}

		private SocketStream GetSocketStream(Socket connection) {
			return new SocketStream(new NetworkStream(connection), _out, _exceptionFormatter);
		}

		private byte[] GetSocketEnd() {
			return new[] {byte.MaxValue, (byte) 8};
		}

		private byte[] GetBufferRange(int length) {
			return Enumerable.Range(0, length).Select(x => (byte) x).ToArray();
		}

		[TestCase(0)]
		[TestCase(8)]
		[TestCase(50)]
		[TestCase(120)]
		public void Socket_Read_WorksAsExpected(int length) {
			#region PreCondition
			Debug.Assert(length >= 0);
			Debug.Assert(length < 200);
			#endregion
			var bytes = GetBufferRange(length);
			TestBase(connection => {
				var stream = GetSocketStream(connection);
				var buffer = new byte[length];
				Assert.AreEqual(length, stream.Read(buffer, 0, length));
				CollectionAssert.AreEqual(bytes, buffer);
			},
			client => client.Send(bytes, SocketFlags.None));
		}

		[TestCase(0)]
		[TestCase(1)]
		[TestCase(8)]
		[TestCase(20)]
		public void Socket_Read_DontReadMoreToBufferAfterEndSignal(int bytesAfterEnd) {
			#region PreCondition
			Debug.Assert(bytesAfterEnd < 100);
			Debug.Assert(bytesAfterEnd >= 0);
			#endregion
			var length = 10;
			var bytes = GetBufferRange(length);
			var sendBuffer = bytes.Concat(GetSocketEnd(), bytes).ToArray();
			TestBase(connection => {
				var stream = GetSocketStream(connection);
				var fullLength = length + bytesAfterEnd;
				var buffer = new byte[fullLength];
				var read = stream.Read(buffer, 0, fullLength);
				Assert.AreEqual(length, read);
			},
			client => client.Send(sendBuffer, SocketFlags.None));
		}

		[TestCase(0)]
		[TestCase(1)]
		[TestCase(8)]
		[TestCase(20)]
		public void SocketAtEnd_Read_DontReadMoreToBuffer(int bytesAfterEnd) {
			#region PreCondition
			Debug.Assert(bytesAfterEnd < 100);
			Debug.Assert(bytesAfterEnd >= 0);
			#endregion
			var length = 50;
			var bytes = GetBufferRange(length);
			var sendBuffer = bytes.Concat(GetSocketEnd(), bytes).ToArray();
			TestBase(connection => {
				var stream = GetSocketStream(connection);
				var buffer = new byte[length];
				stream.Read(buffer, 0, length);
				Assert.AreEqual(0, stream.Read(buffer, 0, bytesAfterEnd));
			},
			client => client.Send(sendBuffer, SocketFlags.None));
		}
	}
}