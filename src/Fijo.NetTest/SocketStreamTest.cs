using System.IO;
using System.Linq;
using System.Net.Sockets;
using Fijo.Net.Services.Socket.Streams;
using Fijo.NetTest.Properties;
using FijoCore.Infrastructure.DependencyInjection.InitKernel;
using FijoCore.Infrastructure.LightContrib.Default.Service.Out.Interface;
using FijoCore.Infrastructure.LightContrib.Service.ExecptionFormatter;
using NUnit.Framework;

namespace Fijo.NetTest {
	[TestFixture(typeof(SocketStream))]
	public class SocketStreamTest : SocketTestBase {
		private IOut _out;
		private IExceptionFormatter _exceptionFormatter;

		[SetUp]
		public void SetUp() {
			new InternalInitKernel().Init();
			_out = Kernel.Resolve<IOut>();
			_exceptionFormatter = Kernel.Resolve<IExceptionFormatter>();
		}

		private SocketStream GetSocketStream(Socket connection) {
			return new SocketStream(new NetworkStream(connection), _out, _exceptionFormatter);
		}

		private byte[] GetBufferRange(int length) {
			return Enumerable.Range(0, length).Select(x => (byte) x).ToArray();
		}

		[TestCase(0)]
		[TestCase(8)]
		[TestCase(40)]
		[TestCase(100)]
		[TestCase(340)]
		[TestCase(930)]
		[TestCase(8000)]
		[TestCase(500340)]
		[TestCase(805300)]
		[TestCase(8002340)]
		public void Socket_ReadWrite_MustReturnSameByteArray(int length) {
			var bytes = GetBufferRange(length);
			TestBase(connection => {
				var stream = GetSocketStream(connection);
				var buffer = new byte[length];
				stream.Read(buffer, 0, length);
				CollectionAssert.AreEqual(bytes, buffer);
			},
			client => {
				var stream = GetSocketStream(client);
				stream.Write(bytes, 0, bytes.Length);
				stream.Seek(0, SeekOrigin.End);
			});
		}
	}
}