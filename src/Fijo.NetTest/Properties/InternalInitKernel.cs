using Fijo.Net.Properties;
using FijoCore.Infrastructure.DependencyInjection.InitKernel.Init;
using FijoCore.Infrastructure.LightContrib.Properties;

namespace Fijo.NetTest.Properties {
	public class InternalInitKernel : ExtendedInitKernel {
		public override void PreInit() {
			LoadModules(new LightContribInjectionModule(), new NetInjectionModule());
		}
	}
}