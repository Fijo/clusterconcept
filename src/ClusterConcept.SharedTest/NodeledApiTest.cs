﻿using System;
using System.Net.Sockets;
using System.Threading;
using ClusterConcept.Shared.Infrastructure.Server.Com;
using ClusterConcept.SharedTest.Properties;
using Fijo.Net.Nodelt.Client.Com;
using Fijo.Net.Nodelt.Handler;
using Fijo.Net.Nodelt.Provider;
using Fijo.Net.Services.Server;
using FijoCore.Infrastructure.DependencyInjection.InitKernel;
using NUnit.Framework;

namespace ClusterConcept.SharedTest {
	[TestFixture]
	public class NodeledApiTest {
		[SetUp]
		public void SetUp() {
			new InternalInitKernel().Init();
		}

		[Test]
		public void ServerTest() {
			Kernel.Inject.Bind<IBaseHandler<NodeCommand>>().To<TestHandler>().InSingletonScope();

			var server = Kernel.Resolve<ISocketServer>();
			StartServer(server, () => {
				var client = Kernel.Resolve<INodeClientCom<NodeCommand>>();
				var provider = Kernel.Resolve<IHandlerProvider<NodeCommand>>();
				var handler = (IHandler<string, string, NodeCommand>) provider.Get(NodeCommand.Test);
				string actual;
				client.Send(handler, "Hallo Welt", out actual);
				Assert.AreEqual("Sie sagten Hallo Welt!", actual);
			});
		}

		private void StartServer(ISocketServer server, Action action) {
			InternalStartServer(server);
			try {
				action();
			}
			finally {
				InternalStopServer(server);
			}
		}

		private void InternalStopServer(ISocketServer server) {
			server.Stop();
		}

		private void InternalStartServer(ISocketServer server) {
			while (true) {
				try {
					server.Start();
					break;
				}
				catch (SocketException) {
					Thread.Sleep(100);
				}
			}
		}
	}
}