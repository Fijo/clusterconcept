using ClusterConcept.Shared.Properties;
using FijoCore.Infrastructure.DependencyInjection.InitKernel.Init;

namespace ClusterConcept.SharedTest.Properties {
	public class InternalInitKernel : ExtendedInitKernel {
		public override void PreInit() {
			LoadModules(new EntityNodeSharedInjectionModule());
		}
	}
}