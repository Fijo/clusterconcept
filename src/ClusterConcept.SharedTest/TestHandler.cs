using ClusterConcept.Shared.Infrastructure.Server.Com;
using Fijo.Net.Nodelt.Handler;
using JetBrains.Annotations;

namespace ClusterConcept.SharedTest {
	[UsedImplicitly]
	public class TestHandler : Handler<string, string, NodeCommand> {
		#region Overrides of Handler<string,string>
		public override string Handle(string obj) {
			return string.Format("Sie sagten {0}!", obj);
		}

		public override NodeCommand Command { get { return NodeCommand.Test; } }
		#endregion
	}
}