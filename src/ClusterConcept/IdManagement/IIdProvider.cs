using Fijo.Infrastructure.Documentation.Attributes.Info;

namespace ClusterConcept.IdManagement
{
	public interface IIdProvider<in TKey> {
		[Desc("returns the next id for a key")]
		long Get(TKey key);
	}
}