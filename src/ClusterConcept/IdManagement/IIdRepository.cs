using System.Collections.Generic;

namespace ClusterConcept.IdManagement
{
	public interface IIdRepository<in TKey> {
		IList<long> Get(TKey key);
	}
}