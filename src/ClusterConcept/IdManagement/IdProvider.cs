using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FijoCore.Infrastructure.DependencyInjection.InitKernel;
using FijoCore.Infrastructure.LightContrib.Extentions.IList.Generic;
using JetBrains.Annotations;

namespace ClusterConcept.IdManagement
{
	[UsedImplicitly]
	public class IdProvider<TKey> : IIdProvider<TKey> {
		private readonly IIdRepository<TKey> _idRepository = Kernel.Resolve<IIdRepository<TKey>>();
		private readonly IDictionary<TKey, IdProviderDto> _clientIds = new Dictionary<TKey, IdProviderDto>();
		private readonly int _criticalCount = 100;
		
		public class IdProviderDto {
			public readonly IList<long> Content;
			public Task FillTask;
		}

		private IdProviderDto GetDto(TKey key) {
			lock(_clientIds) {
				IdProviderDto value;
				if (!_clientIds.TryGetValue(key, out value)) return (_clientIds[key] = CreateDto(key));
				return value;
			}
		}

		private IdProviderDto CreateDto(TKey key) {
			var dto = new IdProviderDto();
			dto.FillTask = new Task(() => Fetch(dto.Content, key));
			return dto;
		}

		private void Fetch(IList<long> target, TKey key) {
			target.AddRange(_idRepository.Get(key));
		}

		private void MayFillList(IdProviderDto target, TKey key) {
			lock(target) {
				var content = target.Content;
				var count = content.Count;
				if (count == 0) {
					if(!target.FillTask.IsRunning()) target.FillTask.RunSynchronously();
					else target.FillTask.Wait();
				}
				if (count <= _criticalCount) {
					if(!target.FillTask.IsRunning()) target.FillTask.Start();
				}
			}
		}

		#region Implementation of IIdProvider<TKey>
		public long Get(TKey key) {
			var list = GetDto(key);
			lock(list) {
				MayFillList(list, key);
				return RemoveAndGetLast(list.Content);
			}
		}

		private static long RemoveAndGetLast(IList<long> content) {
			var last = content.Last();
			content.RemoveAt(content.Count - 1);
			return last;
		}
		#endregion
	}
}