namespace ClusterConcept.IdManagement
{
	// ToDo make a server able to be a client to make endless clustering possible ;)
	public interface IIdPoolServer {
		void Start();
		void Stop();
	}
}