using System;

namespace Fijo.Net.Nodelt.Exceptions {
	[Serializable]
	public class InvalidStreamDataException : Exception {
		public InvalidStreamDataException() {}

		public InvalidStreamDataException(string message) : base(message) {}

		public InvalidStreamDataException(string message, Exception innerException) : base(message, innerException) {}
	}
}