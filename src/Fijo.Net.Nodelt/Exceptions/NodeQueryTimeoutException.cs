using System;
using FijoCore.Infrastructure.LightContrib.Default.Exceptions;
using JetBrains.Annotations;

namespace Fijo.Net.Nodelt.Exceptions {
	[PublicAPI, Serializable]
	public class NodeQueryTimeoutException : RetryTimeOutException {
		public NodeQueryTimeoutException() {}

		public NodeQueryTimeoutException(string message, long tryLimit = default(long), Exception innerException = null) : base(message, tryLimit, innerException) {}
	}
}