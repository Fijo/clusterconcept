using System;
using System.Collections.Generic;
using Fijo.Net.Nodelt.Handler;
using FijoCore.Infrastructure.LightContrib.Default.Exceptions;

namespace Fijo.Net.Nodelt.Exceptions {
	[Serializable]
	public class InvalidNodeCommandException<TNodeCommand> : KeyNotFoundException<IDictionary<TNodeCommand, IBaseHandler<TNodeCommand>>, TNodeCommand> where TNodeCommand : struct, IConvertible {
		public InvalidNodeCommandException(IDictionary<TNodeCommand, IBaseHandler<TNodeCommand>> source, TNodeCommand key) : base(source, key) {}

		public InvalidNodeCommandException(IDictionary<TNodeCommand, IBaseHandler<TNodeCommand>> source, TNodeCommand key, string message) : base(source, key, message) {}

		public InvalidNodeCommandException(IDictionary<TNodeCommand, IBaseHandler<TNodeCommand>> source, TNodeCommand key, string message, Exception innerException) : base(source, key, message, innerException) {}
	}
}