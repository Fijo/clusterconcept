using System;
using System.Collections.Generic;
using System.Linq;
using Fijo.Net.Nodelt.Exceptions;
using Fijo.Net.Nodelt.Handler;
using FijoCore.Infrastructure.DependencyInjection.InitKernel;
using JetBrains.Annotations;
using Ninject;

namespace Fijo.Net.Nodelt.Provider {
	[UsedImplicitly]
	public class HandlerProvider<TNodeCommand> : IHandlerProvider<TNodeCommand> where TNodeCommand : struct, IConvertible {
		private readonly IDictionary<TNodeCommand, IBaseHandler<TNodeCommand>> _content;
		
		public HandlerProvider() {
			_content = Kernel.Inject.GetAll<IBaseHandler<TNodeCommand>>().ToDictionary(x => x.Command);
		}
		
		#region Implementation of IHandlerProvider
		public IBaseHandler<TNodeCommand> Get(TNodeCommand command) {
			IBaseHandler<TNodeCommand> handler;
			if(!_content.TryGetValue(command, out handler)) throw new InvalidNodeCommandException<TNodeCommand>(_content, command);
			return handler;
		}
		#endregion
	}
}