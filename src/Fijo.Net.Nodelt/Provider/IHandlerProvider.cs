using System;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using Fijo.Net.Nodelt.Handler;

namespace Fijo.Net.Nodelt.Provider {
	[About("ICommandHandlerProvider")]
	public interface IHandlerProvider<TNodeCommand> where TNodeCommand : struct, IConvertible {
		IBaseHandler<TNodeCommand> Get(TNodeCommand command);
	}
}