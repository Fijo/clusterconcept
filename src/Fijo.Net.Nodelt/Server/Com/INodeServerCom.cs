using System.Net.Sockets;

namespace Fijo.Net.Nodelt.Server.Com {
	public interface INodeServerCom {
		void Receive(Socket handler);
	}
}