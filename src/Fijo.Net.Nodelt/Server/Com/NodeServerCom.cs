using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Sockets;
using Fijo.Net.Nodelt.Handler;
using Fijo.Net.Nodelt.Provider;
using Fijo.Net.Services.Socket;
using FijoCore.Infrastructure.DependencyInjection.InitKernel;
using FijoCore.Infrastructure.LightContrib.Module.Serialization.Stream;
using FijoCore.Infrastructure.LightContrib.Module.Stream;
using JetBrains.Annotations;

namespace Fijo.Net.Nodelt.Server.Com {
	[UsedImplicitly]
	public class NodeServerCom<TNodeCommand> : INodeServerCom where TNodeCommand : struct, IConvertible {
		private readonly IStreamSerializer<TNodeCommand> _commandSerializer = Kernel.Resolve<IStreamSerializer<TNodeCommand>>();
		private readonly IHandlerProvider<TNodeCommand> _handlerProvider = Kernel.Resolve<IHandlerProvider<TNodeCommand>>();
		private readonly ISocketService _socketService = Kernel.Resolve<ISocketService>();
		private readonly IStreamService _streamService = Kernel.Resolve<IStreamService>();
		private readonly IList<Func<Socket, StreamReader, IBaseHandler<TNodeCommand>, bool>> _handlerHandlings;

		public NodeServerCom() {
			_handlerHandlings = new List<Func<Socket, StreamReader, IBaseHandler<TNodeCommand>, bool>>
			{
				(socket, stream, handler) => TryHandle<IHandler<TNodeCommand>>(socket, stream, handler, Handle),
				(socket, stream, handler) => TryHandle<IReadonlyHandler<TNodeCommand>>(socket, stream, handler, Handle),
				(socket, stream, handler) => TryHandle<ISendonlyHandler<TNodeCommand>>(socket, stream, handler, Handle)
			};
		}
		
		#region Implementation of INodeServerCom
		public void Receive(Socket handler) {
			using(var stream = ReciveStream(handler)) {
				var streamReader = _streamService.GetReader(stream);
				TNodeCommand command;
				if(!TryDeserialize(streamReader, out command)) return;
				var handlerService = _handlerProvider.Get(command);
				HandleHandler(handler, streamReader, handlerService);
			}
		}

		private bool TryDeserialize(StreamReader streamReader, out TNodeCommand result) {
			return _commandSerializer.TryDeserialize(streamReader, out result);
		}

		protected void HandleHandler(Socket socket, StreamReader input, IBaseHandler<TNodeCommand> handler) {
			foreach (var handlerHandling in _handlerHandlings)
				if(handlerHandling(socket, input, handler)) return;
		}

		protected bool TryHandle<T>(Socket socket, StreamReader input, IBaseHandler<TNodeCommand> handler, Action<Socket, StreamReader, T> action) where T : class, IBaseHandler<TNodeCommand> {
			var currentHandler = handler as T;
			if(currentHandler == null) return false;
			action(socket, input, currentHandler);
			return true;
		}

		private void Handle(Socket socket, StreamReader input, IHandler<TNodeCommand> handler) {
			using(var output = _socketService.GetStream(socket)) {
				handler.Handle(input, _streamService.GetWriter(output));
				output.Seek(0, SeekOrigin.End);
			}
		}

		private void Handle(Socket socket, StreamReader input, IReadonlyHandler<TNodeCommand> handler) {
			using(var stream = _socketService.GetStream(socket)) {
				handler.Handle(_streamService.GetWriter(stream));
				stream.Seek(0, SeekOrigin.End);
			}
		}

		private void Handle(Socket socket, StreamReader input, ISendonlyHandler<TNodeCommand> handler) {
			handler.Handle(input);
		}

		private Stream ReciveStream(Socket handler) {
			return _socketService.GetStream(handler);
		}
		#endregion
	}
}