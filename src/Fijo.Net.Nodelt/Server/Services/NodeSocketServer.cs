using System.Net.Sockets;
using Fijo.Infrastructure.DesignPattern.Repository;
using Fijo.Net.Nodelt.Dto;
using Fijo.Net.Nodelt.Server.Com;
using Fijo.Net.Services.Server;
using JetBrains.Annotations;

namespace Fijo.Net.Nodelt.Server.Services {
	[UsedImplicitly]
	public class NodeSocketServer : SocketServer {
		private readonly INodeServerCom _nodeServerCom;

		public NodeSocketServer(IRepository<NodeServerConfig> nodeServerConfigRepo, INodeServerCom nodeServerCom) {
			_nodeServerCom = nodeServerCom;
			var config = nodeServerConfigRepo.Get();
			Init(config.NodeAddress);
		}

		#region Overrides of SocketServer
		protected override void Handle(Socket handler) {
			_nodeServerCom.Receive(handler);
		}
		#endregion
	}
}