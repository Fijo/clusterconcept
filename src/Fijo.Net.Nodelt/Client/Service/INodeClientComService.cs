using System;
using System.Collections.Generic;
using System.Net;
using Fijo.Infrastructure.Documentation.Attributes.DesignPattern;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using Fijo.Net.Nodelt.Handler;

namespace Fijo.Net.Nodelt.Client.Service {
	[Service]
	// => has an spec. INodeClientCom<T>
	// is a facade for this concrete one, but has some �helper� or extention like functionality
	public interface INodeClientComService<in TNodeCommand> where TNodeCommand : struct, IConvertible {
		#region Send (Facade for INodeClientCom<TNodeCommand>)
		bool Send<TInputObj, TOutputObj>(IHandler<TInputObj, TOutputObj, TNodeCommand> handler, TInputObj inputObj, out TOutputObj result, ICollection<EndPoint> toEndPoint = null);
		bool Send<TInputObj>(ISendonlyHandler<TInputObj, TNodeCommand> handler, TInputObj inputObj, ICollection<EndPoint> toEndPoint = null);
		bool Send<TOutputObj>(IReadonlyHandler<TOutputObj, TNodeCommand> handler, out TOutputObj result, ICollection<EndPoint> toEndPoint = null);
		#endregion

		#region ForceSend
		[Desc("returns no succsess - is always successfull (retries until it is so and can othewise just throw an exeception)")]
		TOutputObj ForceSend<TInputObj, TOutputObj>(IHandler<TInputObj, TOutputObj, TNodeCommand> handler, TInputObj inputObj, ICollection<EndPoint> toEndPoint = null);
		[Desc("returns no succsess - is always successfull (retries until it is so and can othewise just throw an exeception)")]
		void ForceSend<TInputObj>(ISendonlyHandler<TInputObj, TNodeCommand> handler, TInputObj inputObj, ICollection<EndPoint> toEndPoint = null);
		[Desc("returns no succsess - is always successfull (retries until it is so and can othewise just throw an exeception)")]
		TOutputObj  ForceSend<TOutputObj>(IReadonlyHandler<TOutputObj, TNodeCommand> handler, ICollection<EndPoint> toEndPoint = null);
		#endregion
	}
}