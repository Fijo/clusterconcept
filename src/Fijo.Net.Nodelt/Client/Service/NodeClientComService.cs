using System;
using System.Collections.Generic;
using System.Net;
using Fijo.Net.Nodelt.Client.Com;
using Fijo.Net.Nodelt.Exceptions;
using Fijo.Net.Nodelt.Handler;
using FijoCore.Infrastructure.DependencyInjection.InitKernel;
using FijoCore.Infrastructure.LightContrib.Default.Service.CheckName;
using FijoCore.Infrastructure.LightContrib.Default.Service.Try;
using FijoCore.Infrastructure.LightContrib.Module.Configuration;

namespace Fijo.Net.Nodelt.Client.Service {
	public class NodeClientComService<TNodeCommand> : INodeClientComService<TNodeCommand> where TNodeCommand : struct, IConvertible {
		private readonly INodeClientCom<TNodeCommand> _nodeClientCom;
		private readonly ITry _try = Kernel.Resolve<ITry>();
		private readonly TimeSpan _trysTimeout = Kernel.Resolve<IConfigurationService>().Get<TimeSpan>(string.Format("{0}.NodeClientComService.TrysTimeout", typeof(NodeClientComService<>).Namespace));
		private readonly string _timeoutExMessage;

		public NodeClientComService() {
			_timeoutExMessage = string.Format("The query (send using �{0}�) fails after at least retrys for {1}.", CN.Get<NodeClientCom<TNodeCommand>>(), _trysTimeout);
		}

		// I found it more correct to pass the concrete impl in using the consturctor, here.
		public NodeClientComService(INodeClientCom<TNodeCommand> nodeClientCom) {
			_nodeClientCom = nodeClientCom;
		}
		#region Implementation of INodeClientComService<in TNodeCommand>
		#region Send
		public bool Send<TInputObj, TOutputObj>(IHandler<TInputObj, TOutputObj, TNodeCommand> handler, TInputObj inputObj, out TOutputObj result, ICollection<EndPoint> toEndPoint = null) {
			return _nodeClientCom.Send(handler, inputObj, out result, toEndPoint);
		}

		public bool Send<TInputObj>(ISendonlyHandler<TInputObj, TNodeCommand> handler, TInputObj inputObj, ICollection<EndPoint> toEndPoint = null) {
			return _nodeClientCom.Send(handler, inputObj, toEndPoint);
		}

		public bool Send<TOutputObj>(IReadonlyHandler<TOutputObj, TNodeCommand> handler, out TOutputObj result, ICollection<EndPoint> toEndPoint = null) {
			return _nodeClientCom.Send(handler, out result, toEndPoint);
		}
		#endregion

		#region ForceSend
		public TOutputObj ForceSend<TInputObj, TOutputObj>(IHandler<TInputObj, TOutputObj, TNodeCommand> handler, TInputObj inputObj, ICollection<EndPoint> toEndPoint = null) {
			return _try.ExlessTryFor(CreateTimeoutException, (out TOutputObj innerResult) => _nodeClientCom.Send(handler, inputObj, out innerResult, toEndPoint), _trysTimeout);
		}

		public void ForceSend<TInputObj>(ISendonlyHandler<TInputObj, TNodeCommand> handler, TInputObj inputObj, ICollection<EndPoint> toEndPoint = null) {
			_try.ExlessTryFor(CreateTimeoutException, () => _nodeClientCom.Send(handler, inputObj, toEndPoint), _trysTimeout);
		}

		public TOutputObj ForceSend<TOutputObj>(IReadonlyHandler<TOutputObj, TNodeCommand> handler, ICollection<EndPoint> toEndPoint = null) {
			return _try.ExlessTryFor(CreateTimeoutException, (out TOutputObj innerResult) => _nodeClientCom.Send(handler, out innerResult, toEndPoint), _trysTimeout);
		}
		#endregion

		protected virtual NodeQueryTimeoutException CreateTimeoutException() {
			return new NodeQueryTimeoutException(_timeoutExMessage);
		}
		#endregion
	}
}