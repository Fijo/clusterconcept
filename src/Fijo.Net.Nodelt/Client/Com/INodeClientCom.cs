using System;
using System.Collections.Generic;
using System.Net;
using Fijo.Net.Nodelt.Handler;

namespace Fijo.Net.Nodelt.Client.Com {
	public interface INodeClientCom<in TNodeCommand> where TNodeCommand : struct, IConvertible {
		bool Send<TInputObj, TOutputObj>(IHandler<TInputObj, TOutputObj, TNodeCommand> commandService, TInputObj inputObj, out TOutputObj result, ICollection<EndPoint> toEndPoint = null);
		bool Send<TInputObj>(ISendonlyHandler<TInputObj, TNodeCommand> handler, TInputObj inputObj, ICollection<EndPoint> toEndPoint = null);
		bool Send<TOutputObj>(IReadonlyHandler<TOutputObj, TNodeCommand> handler, out TOutputObj result, ICollection<EndPoint> toEndPoint = null);
	}
}