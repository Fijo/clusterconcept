//BS:NOFX - Medi-o-core

using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Sockets;
using Fijo.Net.Nodelt.Client.Provider;
using Fijo.Net.Nodelt.Exceptions;
using Fijo.Net.Nodelt.Handler;
using Fijo.Net.Services.Socket;
using Fijo.Net.Services.Socket.Streams;
using FijoCore.Infrastructure.DependencyInjection.InitKernel;
using FijoCore.Infrastructure.LightContrib.Default.Service.Out.Interface;
using FijoCore.Infrastructure.LightContrib.Module.Serialization.Stream;
using FijoCore.Infrastructure.LightContrib.Module.Stream;
using JetBrains.Annotations;

namespace Fijo.Net.Nodelt.Client.Com {
	[UsedImplicitly]
	public class NodeClientCom<TNodeCommand> : INodeClientCom<TNodeCommand> where TNodeCommand : struct, IConvertible {
		private readonly ISockProvider _sockProvider = Kernel.Resolve<ISockProvider>();
		private readonly IStreamService _streamService = Kernel.Resolve<IStreamService>();
		private readonly IStreamSerializer<TNodeCommand> _commandSerializer = Kernel.Resolve<IStreamSerializer<TNodeCommand>>();
		private readonly ISocketService _socketService = Kernel.Resolve<ISocketService>();
		private readonly IOut _out = Kernel.Resolve<IOut>();

		public bool Send<TInputObj, TOutputObj>(IHandler<TInputObj, TOutputObj, TNodeCommand> handler, TInputObj inputObj, out TOutputObj result, ICollection<EndPoint> toEndPoint = null) {
			try {
				if (!SocketAvailible()) return _out.False(out result);
				var socket = GetSocket(toEndPoint);
				lock(socket) {
					Send(socket, handler, inputObj);
					result = Receive(socket, handler);
					ReleaseSocket(socket);
				}
				return true;
			}
			catch(StreamSocketException) {
				return _out.False(out result);
			}
		}

		public bool Send<TInputObj>(ISendonlyHandler<TInputObj, TNodeCommand> handler, TInputObj inputObj, ICollection<EndPoint> toEndPoint = null) {
			try {
				if (!SocketAvailible()) return false;
				var socket = GetSocket(toEndPoint);
				lock(socket) {
					Send(socket, handler, inputObj);
					ReleaseSocket(socket);
				}
				return true;
			}
			catch(StreamSocketException) {
				return false;
			}
		}

		public bool Send<TOutputObj>(IReadonlyHandler<TOutputObj, TNodeCommand> handler, out TOutputObj result, ICollection<EndPoint> toEndPoint = null) {
			try {
				if (!SocketAvailible()) return _out.False(out result);
				var socket = GetSocket(toEndPoint);
				lock(socket) {
					SendCommand(socket, handler);
					result = Receive(socket, handler);
					ReleaseSocket(socket);
				}
				return true;
			}
			catch (StreamSocketException) {
				return _out.False(out result);
			}
		}

		private bool SocketAvailible() {
			return _sockProvider.CanGet();
		}

		private Socket GetSocket(ICollection<EndPoint> toEndPoint) {
			return toEndPoint == null
				       ? _sockProvider.Get()
				       : _sockProvider.Get(toEndPoint);
		}

		private void ReleaseSocket(Socket socket) {
			_sockProvider.Finish(socket);
		}

		private void SendCommand(Socket socket, IBaseHandler<TNodeCommand> handler) {
			using (var stream = _socketService.GetStream(socket)) {
				_commandSerializer.Serialize(_streamService.GetWriter(stream), handler.Command);
				stream.Seek(0, SeekOrigin.End);
			}
		}

		private void InternalSendCommand(Stream stream, IBaseHandler<TNodeCommand> handler) {
			_commandSerializer.Serialize(_streamService.GetWriter(stream), handler.Command);
		}

		private void Send<TInputObj>(Socket socket, IInternalSendonlyHandler<TInputObj, TNodeCommand> handler, TInputObj inputObj) {
			using (var stream = _socketService.GetStream(socket)) {
				InternalSendCommand(stream, handler);
				handler.InputSerializer.Serialize(_streamService.GetWriter(stream), inputObj);
				stream.Seek(0, SeekOrigin.End);
			}
		}

		private TOutputObj Receive<TOutputObj>(Socket socket, IInternalReadonlyHandler<TOutputObj, TNodeCommand> handler) {
			using (var s = _socketService.GetStream(socket))
				return handler.OutputSerializer.Deserialize(_streamService.GetReader(s));	
		}
	}

	[PublicAPI]
	public static class DeserializeExtention {
		[PublicAPI]
		public static T Deserialize<T>([NotNull] this IStreamSerializer<T> serializer, [NotNull] StreamReader reader) {
			T result;
			if (!serializer.TryDeserialize(reader, out result)) throw new InvalidStreamDataException();
			return result;
		}
	}
}