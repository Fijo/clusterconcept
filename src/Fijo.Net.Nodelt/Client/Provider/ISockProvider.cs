using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;

namespace Fijo.Net.Nodelt.Client.Provider {
	public interface ISockProvider {
		bool CanGet();
		Socket Get();
		Socket Get(ICollection<EndPoint> endPoint);
		void Add(IEnumerable<EndPoint> candidates);
		void Finish(Socket socket);
	}
}