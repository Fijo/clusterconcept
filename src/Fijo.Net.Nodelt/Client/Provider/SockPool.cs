//BS:Frei Wild - Hand aufs Herz

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;
using Fijo.Infrastructure.DesignPattern.Repository;
using Fijo.Net.Nodelt.Dto;
using FijoCore.Infrastructure.DependencyInjection.InitKernel;
using FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic;
using FijoCore.Infrastructure.LightContrib.Module.Pool;
using FijoCore.Infrastructure.LightContrib.Module.Pool.Dto;
using JetBrains.Annotations;

namespace Fijo.Net.Nodelt.Client.Provider {
	public interface INodeltSocketHelper {
		bool TryConnect(Socket socket, EndPoint endPoint);
		Socket CreateSocket();
	}

	public class NodeltSocketHelper : INodeltSocketHelper {
		public bool TryConnect(Socket socket, EndPoint endPoint) {
			try {
				socket.Connect(endPoint);
				return true;
			}
			catch (SocketException e) {
				Trace.WriteLine(string.Format("TryConnect for {0} faild.", endPoint));
				return false;
			}
		}

		public Socket CreateSocket() {
			return new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
		}
	}

	[UsedImplicitly]
	public class SockPool : ISockProvider, IDisposable
	{
		private readonly InternalSocketPool _socketPool;

		private class InternalSocketPool : BlockablePool<Socket, SockPoolEntry>	{
			private readonly int _socketUsagesLimitToRecycle;
			private readonly int _allowRetryConnectionFaildAfterMilliseconds;
			private readonly int _retryDirectConnectWaitTime;
			private readonly int _directConnectMaxRetryCount;
			private readonly INodeltSocketHelper _nodeltSocketHelper;

			public InternalSocketPool(INodeltSocketHelper nodeltSocketHelper, IEnumerable<SockPoolEntry> content, int shouldActives, int maintenanceIntervall, int socketUsagesLimitToRecycle, int allowRetryConnectionFaildAfterMilliseconds, int retryDirectConnectWaitTime, int directConnectMaxRetryCount) : base(content, shouldActives, maintenanceIntervall) {
				#region PreCondition
				if(socketUsagesLimitToRecycle <= 0) throw new ArgumentOutOfRangeException("socketUsagesLimitToRecycle", socketUsagesLimitToRecycle, "have to be at least 1");
				if(allowRetryConnectionFaildAfterMilliseconds < 0) throw new ArgumentOutOfRangeException("allowRetryConnectionFaildAfterMilliseconds", allowRetryConnectionFaildAfterMilliseconds, "have to be at least 0");
				if(directConnectMaxRetryCount <= 0) throw new ArgumentOutOfRangeException("directConnectMaxRetryCount", directConnectMaxRetryCount, "must be bigger than 0");
				if(retryDirectConnectWaitTime <= 0) throw new ArgumentOutOfRangeException("retryDirectConnectWaitTime", retryDirectConnectWaitTime, "must be bigger than 0");
				#endregion
				_socketUsagesLimitToRecycle = socketUsagesLimitToRecycle;
				_retryDirectConnectWaitTime = retryDirectConnectWaitTime;
				_directConnectMaxRetryCount = directConnectMaxRetryCount;
				_allowRetryConnectionFaildAfterMilliseconds = -allowRetryConnectionFaildAfterMilliseconds;
				_nodeltSocketHelper = nodeltSocketHelper;
				Init();
			}

			#region Overrides of Pool<Socket,SockPoolEntry>
			protected override bool Activate(SockPoolEntry entry) {
				lock(entry) {
					if(HasJustFaild(entry)) return false;
					var isSuccessfull = TryConnect(entry);
					entry.LastUsage = DateTime.Now;
					entry.LastFaild = !isSuccessfull;
					return isSuccessfull;
				}
			}

			private bool TryConnect(SockPoolEntry entry) {
				return _nodeltSocketHelper.TryConnect(GetSocket(entry), entry.EndPoint);
			}

			private Socket GetSocket(SockPoolEntry entry) {
				return entry.Content ?? (entry.Content = _nodeltSocketHelper.CreateSocket());
			}

			private bool HasJustFaild(SockPoolEntry entry) {
				return entry.LastFaild && entry.LastUsage >= DateTime.Now.AddMilliseconds(_allowRetryConnectionFaildAfterMilliseconds);
			}

			protected override void Deactivate(SockPoolEntry entry) {
				lock (entry) {
//					if(entry.Content.Connected)
					entry.Content.Disconnect(true);
					entry.Usages = 0;
				}
			}

			protected override bool IsToRecycle(SockPoolEntry entry) {
				return entry.Usages > _socketUsagesLimitToRecycle;
			}
			
			#region DirectGet
			public Socket Get(ICollection<EndPoint> endPoint) {
				CheckDisposed();
				return InternalGet(endPoint).Content;
			}

			private SockPoolEntry InternalGet(ICollection<EndPoint> endPoint) {
				return InternalGet(() => InternalGet(endPoint, 0));
			}

			private SockPoolEntry InternalGet(ICollection<EndPoint> endPoint, int reTryCount) {
				#region PreCondition
				#if DEBUG
				Debug.Assert(reTryCount >= 0, "invalid reTryCount - it cannot be negative");
				var sockPoolEntries = Availible.Concat(Active, Content).Execute();
				Debug.Assert(endPoint.All(x => sockPoolEntries.Any(y => y.EndPoint.Equals(x))), "endPoint has to be a knwon remote node. please call Add with your endPoint before using Get with the same endPoint.");
				#endif
				#endregion
				var socket = Availible.Where(x => endPoint.Contains(x.EndPoint)).Shuffle().FirstOrDefault();
				if (socket != null) return socket;
				if (reTryCount != 0) HandleRetry(reTryCount);
				ActivateEndPoint(endPoint);
				return InternalGet(endPoint, reTryCount + 1);
			}

			private void HandleRetry(int reTryCount) {
				if (reTryCount >= _directConnectMaxRetryCount) throw new SocketException();
				Thread.Sleep(_retryDirectConnectWaitTime);
			}

			private void ActivateEndPoint(ICollection<EndPoint> endPoints) {
				lock (Availible) lock (Content)
					ActivateObjects(x => GetDirectObjectToActivate(x, endPoints), 1);
			}

			private IEnumerable<SockPoolEntry> GetDirectObjectToActivate(int count, ICollection<EndPoint> endPoints) {
				return Content.AsParallel()
					.Where(ActivatePredicate)
					.Where(x => endPoints.Contains(x.EndPoint))
					.Shuffle()
					.Where(InternalActivate)
					.Take(count);
			}
			#endregion

			public override void Dispose() {
				base.Dispose();
				Parallel.ForEach(Content.AsParallel()
					                 .Select(x => x.Content)
					                 .Where(x => x != null),
				                 x => x.Dispose());
			}
			#endregion
		}

		private class SockPoolEntry : PoolEntry<Socket> {
			public readonly EndPoint EndPoint;
			public DateTime LastUsage;
			public bool LastFaild;
			public int Usages;

			public SockPoolEntry(EndPoint endPoint) {
				EndPoint = endPoint;
				LastUsage = DateTime.Now;
			}
		}

		public SockPool() {
			var socketHelper = Kernel.Resolve<INodeltSocketHelper>();
			var config = Kernel.Resolve<IRepository<SockPoolConfiguration>>().Get();
			var entries = config.Content.AsParallel()
				.Select(PassSockPoolEntry);

			_socketPool = new InternalSocketPool(socketHelper, entries, config.ShouldActives, config.MaintenanceIntervall, config.SocketUsagesLimitToRecycle, config.AllowRetryConnectionFaildAfterMilliseconds, config.RetryDirectConnectWaitTime, config.DirectConnectMaxRetryCount);
		}

		private SockPoolEntry PassSockPoolEntry(EndPoint endPoint) {
			return new SockPoolEntry(endPoint);
		}
		
		public bool CanGet() {
			return _socketPool.CanGet();
		}

		public Socket Get() {
			return _socketPool.Get();
		}
		
		public Socket Get(ICollection<EndPoint> endPoints) {
			return _socketPool.Get(endPoints);
		}

		public void Add(IEnumerable<EndPoint> candidates) {
			_socketPool.Add(candidates.AsParallel().Select(PassSockPoolEntry));
		}

		public void Finish(Socket socket) {
			_socketPool.Finish(socket);
		}
		
		#region Implementation of IDisposable
		public virtual void Dispose() {
			_socketPool.Dispose();
		}
		#endregion
	}
}