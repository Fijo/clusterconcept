﻿using System.IO;
using FijoCore.Infrastructure.DependencyInjection.InitKernel;
using FijoCore.Infrastructure.LightContrib.Default.Service.Out.Interface;
using FijoCore.Infrastructure.LightContrib.Module.Serialization.Stream;

namespace Fijo.Net.Nodelt.ImplBase {
	public abstract class DefaultNodeCommandSerializer<T> : IStreamSerializer<T> {
		private readonly IStreamSerializer<int> _streamSerializer = Kernel.Resolve<IStreamSerializer<int>>();
		private readonly IOut _out = Kernel.Resolve<IOut>();
		#region Implementation of IStreamSerializer<NodeCommand>
		public void Serialize(StreamWriter streamWriter, T obj) {
			_streamSerializer.Serialize(streamWriter, Convert(obj));
		}

		public bool TryDeserialize(StreamReader streamReader, out T result) {
			int obj;
			return _streamSerializer.TryDeserialize(streamReader, out obj)
				       ? _out.True(out result, Convert(obj))
				       : _out.False(out result);
		}
		#endregion

		protected abstract T Convert(int value);
		protected abstract int Convert(T value);
	}
}