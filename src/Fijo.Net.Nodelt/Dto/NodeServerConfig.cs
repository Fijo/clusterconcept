using System.Net;
using Fijo.Infrastructure.Documentation.Attributes.DesignPattern;

namespace Fijo.Net.Nodelt.Dto {
	[Dto]
	public class NodeServerConfig {
		public EndPoint NodeAddress;
	}
}