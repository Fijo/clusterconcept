using System.Collections.Generic;
using System.Net;
using Fijo.Infrastructure.Documentation.Attributes.DesignPattern;

namespace Fijo.Net.Nodelt.Dto {
	[Dto]
	public class SockPoolConfiguration {
		public IEnumerable<EndPoint> Content;
		public int ShouldActives;
		public int MaintenanceIntervall;
		public int SocketUsagesLimitToRecycle;
		public int AllowRetryConnectionFaildAfterMilliseconds;
		public int RetryDirectConnectWaitTime;
		public int DirectConnectMaxRetryCount;
	}
}