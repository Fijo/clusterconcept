using System;
using System.IO;

namespace Fijo.Net.Nodelt.Handler {
	public interface IReadonlyHandler<out TNodeCommand> : IInternalReadonlyHandler<TNodeCommand> where TNodeCommand : struct, IConvertible {
		void Handle(StreamWriter output);
	}

	public interface IReadonlyHandler<TOutputObj, out TNodeCommand> : IReadonlyHandler<TNodeCommand>, IInternalReadonlyHandler<TOutputObj, TNodeCommand> where TNodeCommand : struct, IConvertible {
		TOutputObj Handle();
	}
}