using System;
using System.IO;
using Fijo.Net.Nodelt.Client.Com;
using FijoCore.Infrastructure.DependencyInjection.InitKernel;
using FijoCore.Infrastructure.LightContrib.Module.Serialization.Stream;

namespace Fijo.Net.Nodelt.Handler {
	public abstract class Handler<TInputObj, TOutputObj, TNodeCommand> : IHandler<TInputObj, TOutputObj, TNodeCommand> where TNodeCommand : struct, IConvertible {
		protected Handler() {
			InputSerializer = Kernel.Resolve<IStreamSerializer<TInputObj>>();
			OutputSerializer = Kernel.Resolve<IStreamSerializer<TOutputObj>>();
		}
		#region Implementation of IBaseHandler
		public abstract TOutputObj Handle(TInputObj obj);
		public abstract TNodeCommand Command { get; }
		#endregion
		#region Implementation of IInternalReadonlyHandler<TOutputObj>
		public virtual IStreamSerializer<TOutputObj> OutputSerializer { get; private set; }
		#endregion
		#region Implementation of IInternalSendonlyHandler<TInputObj>
		public virtual IStreamSerializer<TInputObj> InputSerializer { get; private set; }
		#endregion
		#region Implementation of IHandler
		public void Handle(StreamReader input, StreamWriter output) {
			OutputSerializer.Serialize(output, Handle(InputSerializer.Deserialize(input)));
		}
		#endregion
	}
}