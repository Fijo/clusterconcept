using System;
using System.IO;
using FijoCore.Infrastructure.DependencyInjection.InitKernel;
using FijoCore.Infrastructure.LightContrib.Module.Serialization.Stream;

namespace Fijo.Net.Nodelt.Handler
{
	public abstract class ReadonlyHandler<TOutputObj, TNodeCommand> : IReadonlyHandler<TOutputObj, TNodeCommand> where TNodeCommand : struct, IConvertible
	{
		protected ReadonlyHandler() {
			OutputSerializer = Kernel.Resolve<IStreamSerializer<TOutputObj>>();
		}
		#region Implementation of IBaseHandler
		public abstract TOutputObj Handle();
		public abstract TNodeCommand Command { get; }
		public void Handle(StreamWriter output) {
			OutputSerializer.Serialize(output, Handle());
		}
		#endregion
		#region Implementation of IInternalReadonlyHandler<TOutputObj>
		public virtual IStreamSerializer<TOutputObj> OutputSerializer { get; private set; }
		#endregion
	}
}