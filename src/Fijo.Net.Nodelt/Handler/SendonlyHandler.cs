using System;
using System.IO;
using Fijo.Net.Nodelt.Client.Com;
using FijoCore.Infrastructure.DependencyInjection.InitKernel;
using FijoCore.Infrastructure.LightContrib.Module.Serialization.Stream;

namespace Fijo.Net.Nodelt.Handler {
	public abstract class SendonlyHandler<TInputObj, TNodeCommand> : ISendonlyHandler<TInputObj, TNodeCommand> where TNodeCommand : struct, IConvertible {
		protected SendonlyHandler() {
			InputSerializer = Kernel.Resolve<IStreamSerializer<TInputObj>>();
		}
		#region Implementation of IBaseHandler
		public abstract void Handle(TInputObj obj);
		public abstract TNodeCommand Command { get; }

		public void Handle(StreamReader input) {
			Handle(InputSerializer.Deserialize(input));
		}
		#endregion
		#region Implementation of IInternalSendonlyHandler<TInputObj>
		public virtual IStreamSerializer<TInputObj> InputSerializer { get; private set; }
		#endregion
	}
}