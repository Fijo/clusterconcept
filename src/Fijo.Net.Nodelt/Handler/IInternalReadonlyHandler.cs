using System;
using FijoCore.Infrastructure.LightContrib.Module.Serialization.Stream;

namespace Fijo.Net.Nodelt.Handler {
	public interface IInternalReadonlyHandler<out TNodeCommand> : IBaseHandler<TNodeCommand>  where TNodeCommand : struct, IConvertible {}

	public interface IInternalReadonlyHandler<TOutputObj, out TNodeCommand> : IInternalReadonlyHandler<TNodeCommand> where TNodeCommand : struct, IConvertible {
		IStreamSerializer<TOutputObj> OutputSerializer { get; }
	}
}