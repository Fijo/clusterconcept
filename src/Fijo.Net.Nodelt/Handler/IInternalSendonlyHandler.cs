using System;
using FijoCore.Infrastructure.LightContrib.Module.Serialization.Stream;

namespace Fijo.Net.Nodelt.Handler {
	public interface IInternalSendonlyHandler<out TNodeCommand> : IBaseHandler<TNodeCommand> where TNodeCommand : struct, IConvertible {}

	public interface IInternalSendonlyHandler<TInputObj, out TNodeCommand> : IInternalSendonlyHandler<TNodeCommand> where TNodeCommand : struct, IConvertible {
		IStreamSerializer<TInputObj> InputSerializer { get; }
	}
}