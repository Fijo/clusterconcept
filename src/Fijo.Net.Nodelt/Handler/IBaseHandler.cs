using System;

namespace Fijo.Net.Nodelt.Handler {
	public interface IBaseHandler<out TNodeCommand> where TNodeCommand : struct, IConvertible {
		TNodeCommand Command { get; }
	}
}