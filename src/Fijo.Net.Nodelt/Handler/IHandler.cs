using System;
using System.IO;

namespace Fijo.Net.Nodelt.Handler {
	public interface IHandler<out TNodeCommand> : IInternalReadonlyHandler<TNodeCommand>, IInternalSendonlyHandler<TNodeCommand> where TNodeCommand : struct, IConvertible {
		void Handle(StreamReader input, StreamWriter output);
	}

	public interface IHandler<TInputObj, TOutputObj, out TNodeCommand> : IHandler<TNodeCommand>, IInternalReadonlyHandler<TOutputObj, TNodeCommand>, IInternalSendonlyHandler<TInputObj, TNodeCommand> where TNodeCommand : struct, IConvertible {
		TOutputObj Handle(TInputObj obj);
	}
}