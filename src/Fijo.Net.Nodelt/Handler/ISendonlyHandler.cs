using System;
using System.IO;

namespace Fijo.Net.Nodelt.Handler {
	public interface ISendonlyHandler<out TNodeCommand> : IInternalSendonlyHandler<TNodeCommand> where TNodeCommand : struct, IConvertible {
		void Handle(StreamReader input);
	}

	public interface ISendonlyHandler<TInputObj, out TNodeCommand> : ISendonlyHandler<TNodeCommand>, IInternalSendonlyHandler<TInputObj, TNodeCommand> where TNodeCommand : struct, IConvertible {
		void Handle(TInputObj obj);
	}
}