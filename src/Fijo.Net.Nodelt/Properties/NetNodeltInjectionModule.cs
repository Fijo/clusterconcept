using System;
using Fijo.Net.Nodelt.Provider;
using Fijo.Net.Properties;
using FijoCore.Infrastructure.DependencyInjection.Extentions.IKernel;
using FijoCore.Infrastructure.DependencyInjection.InitKernel.Base;
using FijoCore.Infrastructure.WrappedLibs.DependencyInjection.Interface;

namespace Fijo.Net.Nodelt.Properties {
	public class NetNodeltSharedInjectionModule<TNodeCommand> : ExtendedNinjectModule where TNodeCommand : struct, IConvertible {
		public override void AddModule(IKernel kernel) {
			kernel.Load(new NetInjectionModule());
		}

		public override void OnLoad(IKernel kernel) {
			kernel.Bind<IHandlerProvider<TNodeCommand>>().To<HandlerProvider<TNodeCommand>>().InSingletonScope();
		}
	}
}