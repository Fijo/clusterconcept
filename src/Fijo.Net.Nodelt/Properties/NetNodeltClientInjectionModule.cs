using System;
using Fijo.Net.Nodelt.Client.Com;
using Fijo.Net.Nodelt.Client.Service;
using FijoCore.Infrastructure.DependencyInjection.Extentions.IKernel;
using FijoCore.Infrastructure.DependencyInjection.InitKernel.Base;
using IKernel = FijoCore.Infrastructure.WrappedLibs.DependencyInjection.Interface.IKernel;

namespace Fijo.Net.Nodelt.Properties {
	public class NetNodeltClientInjectionModule<TNodeCommand> : ExtendedNinjectModule where TNodeCommand : struct, IConvertible {
		public override void AddModule(IKernel kernel) {
			kernel.Load(new NetNodeltSharedInjectionModule<TNodeCommand>());
		}

		public override void OnLoad(IKernel kernel) {
			kernel.Bind<INodeClientCom<TNodeCommand>>().To<NodeClientCom<TNodeCommand>>().InSingletonScope();
			kernel.Bind<INodeClientComService<TNodeCommand>>().To<NodeClientComService<TNodeCommand>>().InSingletonScope();
		}
	}
}