using System;
using Fijo.Net.Nodelt.Client.Provider;
using Fijo.Net.Nodelt.Server.Com;
using Fijo.Net.Nodelt.Server.Services;
using Fijo.Net.Services.Server;
using FijoCore.Infrastructure.DependencyInjection.Extentions.IKernel;
using FijoCore.Infrastructure.DependencyInjection.InitKernel.Base;
using FijoCore.Infrastructure.WrappedLibs.DependencyInjection.Interface;

namespace Fijo.Net.Nodelt.Properties {
	public class NetNodeltServerInjectionModule<TNodeCommand> : ExtendedNinjectModule where TNodeCommand : struct, IConvertible {
		public override void AddModule(IKernel kernel) {
			kernel.Load(new NetNodeltSharedInjectionModule<TNodeCommand>());
		}

		public override void OnLoad(IKernel kernel) {
			kernel.Bind<INodeServerCom>().To<NodeServerCom<TNodeCommand>>().InSingletonScope();
			kernel.Bind<ISocketServer>().To<NodeSocketServer>().InSingletonScope();
			kernel.Bind<INodeltSocketHelper>().To<NodeltSocketHelper>().InSingletonScope();
			kernel.Bind<ISockProvider>().To<SockPool>().InSingletonScope();
		}
	}
}