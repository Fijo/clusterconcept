﻿using System;
using ClusterConcept.Shared.Infrastructure.Service.Exec;
using ClusterConcept.SharedTest.Properties;
using Fijo.Net.Services.Server;
using FijoCore.Infrastructure.DependencyInjection.InitKernel;

namespace ClusterConcept.Shared.TestNode {
	class Program {
		static void Main(string[] args) {
			new InternalInitKernel().Init();
			Kernel.Resolve<ISocketServer>().Start();
			Kernel.Resolve<IExecService>().HelloCluster(force: true);
			Console.ReadLine();
		}
	}
}
