using ClusterConceptTest.Properties;
using FijoCore.Infrastructure.DependencyInjection.InitKernel.Init;

namespace ClusterConcept.SharedTest.Properties {
	public class InternalInitKernel : ExtendedInitKernel {
		public override void PreInit() {
			LoadModules(new EntityServerInjectionModule());
		}
	}
}