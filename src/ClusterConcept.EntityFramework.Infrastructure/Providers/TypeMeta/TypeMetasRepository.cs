//BS:NOFX - Don't call me white

using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using ClusterConcept.EntityFramework.Infrastructure.Service.TypeMeta;
using ClusterConcept.EntityFramework.Model.Attributes;
using ClusterConcept.EntityFramework.Model.Internal.Type.Meta;
using ClusterConcept.Shared.Model.Enums;
using Fijo.Infrastructure.DesignPattern.Accessor.Delegates;
using Fijo.Infrastructure.DesignPattern.Repository;
using Fijo.Infrastructure.Documentation.Enums;
using FijoCore.Infrastructure.DependencyInjection.InitKernel;
using FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic;
using FijoCore.Infrastructure.LightContrib.Extentions.Type;
using FijoCore.Infrastructure.LightContrib.Module.Assembly;
using FijoCore.Infrastructure.LightContrib.Module.Generics.Lookup;

namespace ClusterConcept.EntityFramework.Infrastructure.Providers.TypeMeta
{
	public class TypeMetasRepository : RepositoryBase<TypeMetas>
	{
		private readonly AssembliesProvider _assembliesProvider = Kernel.Resolve<AssembliesProvider>();
		private readonly IGenericLookup<ITypeMetaServiceBase, DataType> _typeMetaServiceLookup = Kernel.Resolve<IGenericLookup<ITypeMetaServiceBase, DataType>>();
		private readonly ICollection<Type> _types;
		private TypeMetas _metas;
		private IList<Model.Internal.Type.Meta.TypeMeta> _allTypes;

		public TypeMetasRepository() {
			_types = GetTypes();
			Init();
		}

		protected void Init() {
			_metas = new TypeMetas
			{
				Contextes = CreateMetas<ContextTypeMeta>(_types, DataType.DataContext),
				Entities = CreateMetas<EntityTypeMeta>(_types, DataType.Entity),
				Primitives = CreateMetas<PrimitiveTypeMeta>(_types, DataType.Primitive)
			};
			_allTypes = _metas.Contextes.Concat<Model.Internal.Type.Meta.TypeMeta>(_metas.Entities, _metas.Primitives).ToList();
			Parallel.ForEach(_allTypes, AdjustProperties);
		}

		private void AdjustProperties<T>(T typeMeta) where T : Model.Internal.Type.Meta.TypeMeta {
			var properties = GetProperties(typeMeta.Type).Where(HasUIdAttribute).Execute();
			AdjustProperties<ContextTypeMeta>(typeMeta, properties, DataType.DataContext, (me, value) => me.Contextes = value);
			AdjustProperties<EntityTypeMeta>(typeMeta, properties, DataType.Entity, (me, value) => me.Entities = value);
			AdjustProperties<PrimitiveTypeMeta>(typeMeta, properties, DataType.Primitive, (me, value) => me.Primitives = value);
		}

		private void AdjustProperties<T>(Model.Internal.Type.Meta.TypeMeta typeMeta, IEnumerable<PropertyInfo> infos, DataType dataType, Setter<Model.Internal.Type.Meta.TypeMeta, IList<PropertyMeta>> setPropertyMeta) where T : Model.Internal.Type.Meta.TypeMeta, new() {
			var service = GetService<T>(dataType);
			var properties = infos.Where(x => service.IsType(x.PropertyType))
				.Select(x => {
					var propertyMeta = service.Map(x);
					var propertyType = x.PropertyType;
					propertyMeta.TypeMeta = _allTypes.Single(y => y.Type == propertyType);
					return propertyMeta;
				})
				.ToList();
			setPropertyMeta(typeMeta, properties);
		}

		// ToDo improve performance - do not do that every time
		private ITypeMetaServiceBase<T> GetService<T>(DataType dataType) where T : Model.Internal.Type.Meta.TypeMeta, new() {
			return _typeMetaServiceLookup.Get<ITypeMetaServiceBase<T>>(dataType);
		}

		private ICollection<Type> GetTypes() {
			return _assembliesProvider.GetAssemblies()
				.SelectMany(x => x.GetTypes())
				.Where(x => !x.IsIgnoredFor(IgnoreFor.EntityFramework))
				.Execute();
		}

		private IList<T> CreateMetas<T>(IEnumerable<Type> types, DataType dataType) where T : Model.Internal.Type.Meta.TypeMeta, new() {
			var service = GetService<T>(dataType);
			return types.AsParallel().Where(service.IsType).Select(service.Map).ToList();
		}

		#region PropertyInfo Resolving
		private bool HasUIdAttribute(PropertyInfo propertyInfo) {
			return GetUIdAttribute(propertyInfo) != null;
		}

		private UIdAttribute GetUIdAttribute(PropertyInfo propertyInfo) {
			return (UIdAttribute) propertyInfo.GetCustomAttributes(typeof (UIdAttribute), true).SingleOrDefault();
		}

		private IEnumerable<PropertyInfo> GetProperties(System.Type type)	{
			return type.GetProperties(BindingFlags.Instance | BindingFlags.Public | BindingFlags.SetField | BindingFlags.GetField);
		}
		#endregion

		#region Overrides of RepositoryBase<TypeMetas>
		public override TypeMetas Get() {
			return _metas;
		}
		#endregion
	}
}