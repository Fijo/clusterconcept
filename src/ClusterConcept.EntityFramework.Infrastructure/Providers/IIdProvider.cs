using ClusterConcept.EntityFramework.Model.Internal.Type.Meta;

namespace ClusterConcept.EntityFramework.Infrastructure.Providers {
	public interface IIdProvider {
		long Get(EntityTypeMeta key);
	}
}