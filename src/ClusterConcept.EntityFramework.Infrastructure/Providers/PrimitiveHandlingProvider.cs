using System.Collections.Generic;
using System.Linq;
using ClusterConcept.EntityFramework.Infrastructure.Interfaces;
using Fijo.Infrastructure.DesignPattern.Repository.Collection;
using FijoCore.Infrastructure.DependencyInjection.InitKernel;
using Ninject;
using SType = System.Type;

namespace ClusterConcept.EntityFramework.Infrastructure.Providers {
	public class PrimitiveHandlingProvider : CollectionRepositoryBase<KeyValuePair<SType, IPrimitiveHandling>> {
		#region Overrides of CollectionRepositoryBase<KeyValuePair<SType,IPrimitiveHandling>>
		protected override IEnumerable<KeyValuePair<SType, IPrimitiveHandling>> GetAll() {
			return Kernel.Inject.GetAll<IPrimitiveHandling>()
				.ToDictionary(x => x.Type, x => x);
		}
		#endregion
	}
}