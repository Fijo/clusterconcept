using ClusterConcept.EntityFramework.Model.Internal.Type.Meta;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using JetBrains.Annotations;

namespace ClusterConcept.EntityFramework.Infrastructure.Providers {
	[UsedImplicitly]
	[Desc("simple non clustered id provider")]
	public class SimpleIdProvider : IIdProvider {
		#region Implementation of IIdProvider
		public long Get(EntityTypeMeta key) {
			return key.CurrentId++;
		}
		#endregion
	}
}