//BS:Frei Wild - 08 Die Gedanken Sie Sind Frei

using ClusterConcept.EntityFramework.Infrastructure.Providers;
using ClusterConcept.EntityFramework.Model.Entity;
using ClusterConcept.EntityFramework.Model.Internal.Type.Meta;
using JetBrains.Annotations;

namespace ClusterConcept.EntityFramework.Infrastructure.Factory {
	[UsedImplicitly]
	public class EntityFactory : IEntityFactory {
		private readonly IIdProvider _idProvider;

		public EntityFactory(IIdProvider idProvider) {
			_idProvider = idProvider;
		}

		public IEntity Create(EntityTypeMeta key) {
			return Create(key, _idProvider.Get(key));
		}

		public IEntity Create(EntityTypeMeta key, long id) {
			var entity = key.Create();
			entity.Id = id;
			return entity;
		}
	}
}