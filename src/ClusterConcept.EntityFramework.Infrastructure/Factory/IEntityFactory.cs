using ClusterConcept.EntityFramework.Model.Entity;
using ClusterConcept.EntityFramework.Model.Internal.Type.Meta;

namespace ClusterConcept.EntityFramework.Infrastructure.Factory
{
	public interface IEntityFactory	{
		IEntity Create(EntityTypeMeta key);
		IEntity Create(EntityTypeMeta key, long id);
	}
}