using ClusterConcept.EntityFramework.Infrastructure.Service.TypeMeta;
using ClusterConcept.Shared.Model.Enums;
using FijoCore.Infrastructure.LightContrib.Module.Generics.Lookup;

namespace ClusterConcept.EntityFramework.Infrastructure.GenericLookups {
	public class TypeMetaServiceBaseLookup : InjectLookup<ITypeMetaServiceBase, DataType> {
		#region Overrides of GenericLookup<ITypeMetaServiceBase,DataType>
		protected override DataType GetKey(ITypeMetaServiceBase entity) {
			return entity.DataType;
		}
		#endregion
	}
}