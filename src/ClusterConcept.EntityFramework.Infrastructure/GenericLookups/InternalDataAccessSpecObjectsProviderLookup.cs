using ClusterConcept.EntityFramework.Infrastructure.Service.Data.Access.Spec;
using ClusterConcept.Shared.Model.Enums;
using FijoCore.Infrastructure.LightContrib.Module.Generics.Lookup;

namespace ClusterConcept.EntityFramework.Infrastructure.GenericLookups {
	public class InternalDataAccessSpecObjectsProviderLookup : InjectLookup<IInternalDataAccessSpecObjectsProvider, DataType> {
		#region Overrides of GenericLookup<IInternalDataAccessSpecObjectsProvider,DataType>
		protected override DataType GetKey(IInternalDataAccessSpecObjectsProvider entity) {
			return entity.DataType;
		}
		#endregion
	}
}