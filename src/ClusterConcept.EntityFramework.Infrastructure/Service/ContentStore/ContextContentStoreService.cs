//BS:Green Day - 16 21 Guns

using System.Collections.Generic;
using ClusterConcept.EntityFramework.Infrastructure.Service.ObjectStore;
using ClusterConcept.EntityFramework.Model.Base;
using ClusterConcept.EntityFramework.Model.DataContext;
using ClusterConcept.EntityFramework.Model.Internal;
using ClusterConcept.EntityFramework.Model.Internal.Type.Meta;
using ClusterConcept.Shared.Model.ComEntity.Type;
using ClusterConcept.Shared.Model.Enums;
using Fijo.Infrastructure.DesignPattern.Lazy.Interface;
using FijoCore.Infrastructure.LightContrib.Module.Lazy;
using JetBrains.Annotations;
using DTypeMeta = ClusterConcept.EntityFramework.Model.Internal.Type.Meta.TypeMeta;

namespace ClusterConcept.EntityFramework.Infrastructure.Service.ContentStore {
	[UsedImplicitly]
	public class ContextContentStoreService : IContentStoreService {
		private readonly ILazy<IObjectStoreService<IDataContext>> _objectStoreService = new LazyInject<IObjectStoreService<IDataContext>>();
		private readonly ILazy<IDataContextService> _dataContextService = new LazyInject<IDataContextService>();

		#region Implementation of IContentStoreService<IDataContext>
		public DataType DataType { get { return DataType.DataContext; } }

		public void Save(IBasicEntity context, PBasicEntityType pContextKey, DataBundle content, DTypeMeta typeMeta, IList<StoreSet> toStoreSets) {
			var objectStoreService = _objectStoreService.Get();
			foreach (var propertyMeta in typeMeta.Contextes)
				objectStoreService.Save((IDataContext) propertyMeta.Getter(context), propertyMeta.TypeMeta, toStoreSets);
		}

		public void Load(IBasicEntity context, DataBundle content, DTypeMeta typeMeta) {
			var dataContextService = _dataContextService.Get();
			foreach (var propertyMeta in typeMeta.Contextes)
				propertyMeta.Setter(context, dataContextService.Get((ContextTypeMeta) propertyMeta.TypeMeta));
		}
		#endregion
	}
}