//BS:Frei Wild - Harte Zeiten

using System.Collections.Generic;
using System.Diagnostics;
using ClusterConcept.EntityFramework.Infrastructure.Service.Creation;
using ClusterConcept.EntityFramework.Infrastructure.Service.Data.Bundle;
using ClusterConcept.EntityFramework.Infrastructure.Service.Event;
using ClusterConcept.EntityFramework.Infrastructure.Service.ObjectStore;
using ClusterConcept.EntityFramework.Model.Base;
using ClusterConcept.EntityFramework.Model.Entity;
using ClusterConcept.EntityFramework.Model.Internal;
using ClusterConcept.EntityFramework.Model.Internal.Type.Meta;
using ClusterConcept.Shared.Model.ComEntity.Type;
using ClusterConcept.Shared.Model.Enums;
using ClusterConcept.Shared.Model.Events;
using Fijo.Infrastructure.DesignPattern.Events;
using Fijo.Infrastructure.DesignPattern.Lazy.Interface;
using FijoCore.Infrastructure.DependencyInjection.InitKernel;
using FijoCore.Infrastructure.LightContrib.Extentions.ICollection.Generic;
using FijoCore.Infrastructure.LightContrib.Extentions.IDictionary.Generic;
using FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic;
using FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic.KeyValuePair;
using FijoCore.Infrastructure.LightContrib.Factories.Interface;
using FijoCore.Infrastructure.LightContrib.Module.Lazy;
using JetBrains.Annotations;
using DTypeMeta = ClusterConcept.EntityFramework.Model.Internal.Type.Meta.TypeMeta;

namespace ClusterConcept.EntityFramework.Infrastructure.Service.ContentStore {
	[UsedImplicitly]
	public class EntityContentStoreService : IContentStoreService {
		private readonly IPairFactory _pairFactory = Kernel.Resolve<IPairFactory>();
		private readonly ILazy<IObjectStoreService<IEntity>> _objectStoreService = new LazyInject<IObjectStoreService<IEntity>>();
		private readonly ILazy<IEntityService> _entityService = new LazyInject<IEntityService>();
		private readonly ICreationService<IEntity> _creationService = Kernel.Resolve<ICreationService<IEntity>>();
		private readonly IChangeEventService _changeEventService = Kernel.Resolve<IChangeEventService>();
		private readonly IDataBundleService _dataBundleService = Kernel.Resolve<IDataBundleService>();
		private readonly IStoreSetService _storeSetService = Kernel.Resolve<IStoreSetService>();

		#region Implementation of ISaveService<IEntity>
		public DataType DataType { get { return DataType.Entity; } }

		public void Save(IBasicEntity context, PBasicEntityType pContextKey, DataBundle content, DTypeMeta typeMeta, IList<StoreSet> toStoreSets) {
			var subEntityIds = content.Data.SubEntityIds;
			ICollection<KeyValuePair<PType, long>> changedSubEntities;
			lock (subEntityIds) changedSubEntities = GetChangedSubEntities(context, subEntityIds, typeMeta, toStoreSets).Execute();
			if(changedSubEntities.Any())
				_storeSetService.StoreChangeset(toStoreSets,
												pContextKey,
												CreateStoreEntityEvent(changedSubEntities));
		}

		[NotNull, Pure]
		private IEvent CreateStoreEntityEvent([NotNull] IEnumerable<KeyValuePair<PType, long>> changedSubEntities) {
			return new ChangeEntityEvent
			{
				Changeset = changedSubEntities.ToDict()
			};
		}

		private IEnumerable<KeyValuePair<PType, long>> GetChangedSubEntities(IBasicEntity context, IDictionary<PType, long> subEntityIds, DTypeMeta typeMeta, IList<StoreSet> toStoreSets) {
			var objectStoreService = _objectStoreService.Get();
			foreach (var propertyMeta in typeMeta.Entities) {
				var key = propertyMeta.PKey;
				var entity = GetValue(context, propertyMeta);
				if(entity == null) {
					HandleNullSubEntities(key);
					continue;
				}
				objectStoreService.Save(entity, propertyMeta.TypeMeta, toStoreSets);
				var id = entity.Id;
				long value;
				if (subEntityIds.TryGetValue(key, out value) && value == id) continue;
				subEntityIds[key] = id;
				yield return GetChangePair(key, id);
			}
		}

		[Pure]
		private IEntity GetValue(IBasicEntity context, PropertyMeta propertyMeta) {
			return (IEntity) propertyMeta.Getter(context);
		}

		private void HandleNullSubEntities(PType pType) {
			Trace.WriteLine(string.Format("skip save of subentity, that is null at property {0}", pType.UId));
			
			// ToDO may create null entities
		}

		private KeyValuePair<PType, long> GetChangePair(PType pType, long id) {
			Trace.WriteLine(string.Format("push change entity at property {0} is linked to {1}", pType, id));
			return _pairFactory.KeyValuePair(pType, id);
		}

		public void Load(IBasicEntity context, DataBundle content, DTypeMeta typeMeta) {
			var isNewFetched = IsFullFetch(content);
			var isNewCreated = isNewFetched && IsNewCreated(content);
			var entities = typeMeta.Entities;
			if(isNewCreated) CreateEntities(context, entities);
			else LoadEntities(context, content, entities, isNewFetched);
		}

		private void LoadEntities(IBasicEntity context, DataBundle content, IList<PropertyMeta> propertyMetas, bool isNewFetched) {
			var subEntities = _changeEventService.GetEntityChanges(content);
			if (subEntities == null) return;
			lock (propertyMetas)
				foreach (var propertyMeta in propertyMetas)
					LoadEntry(context, subEntities, propertyMeta, isNewFetched);
		}

		private void CreateEntities(IBasicEntity context, IList<PropertyMeta> propertyMetas) {
			lock (propertyMetas)
				foreach (var propertyMeta in propertyMetas)
					CreateEntry(context, propertyMeta);
		}

		private void CreateEntry(IBasicEntity context, PropertyMeta propertyMeta) {
			propertyMeta.Setter(context, _creationService.Create(propertyMeta));
		}

		private bool IsFullFetch([NotNull] DataBundle content) {
			return _dataBundleService.GetBeginRevision(content) == 0;
		}

		private bool IsNewCreated([NotNull] DataBundle content) {
			return _dataBundleService.GetEndRevision(content) == 0;
		}

		private bool IsNew([NotNull] DataBundle content) {
			return _dataBundleService.GetBeginRevision(content) == 0;
		}

		private void LoadEntry([NotNull] IBasicEntity context, [NotNull] IDictionary<PType, long> subEntityIds, [NotNull] PropertyMeta propertyMeta, bool isNewFetched) {
			var pKey = propertyMeta.PKey;
			var entityService = _entityService.Get();
			subEntityIds.TryGet(pKey, value => propertyMeta.Setter(context, entityService.InternalGet(value, (EntityTypeMeta) propertyMeta.TypeMeta)), () => {
				if(isNewFetched && _creationService.AutoCreate(propertyMeta))
					propertyMeta.Setter(context, _creationService.Create(propertyMeta));
			});
		}
		#endregion
	}
}