using System.Collections.Generic;
using ClusterConcept.EntityFramework.Infrastructure.Service.ObjectStore;
using ClusterConcept.EntityFramework.Model.Base;
using ClusterConcept.EntityFramework.Model.Internal;
using ClusterConcept.Shared.Model.ComEntity.Type;
using ClusterConcept.Shared.Model.Enums;
using Fijo.Infrastructure.DesignPattern.Events;
using FijoCore.Infrastructure.LightContrib.Module.Transaction;
using JetBrains.Annotations;
using DTypeMeta = ClusterConcept.EntityFramework.Model.Internal.Type.Meta.TypeMeta;

namespace ClusterConcept.EntityFramework.Infrastructure.Service.ContentStore {
	public interface IContentStoreService {
		DataType DataType { get; }

		void Save([NotNull] IBasicEntity context, [NotNull] PBasicEntityType pContextKey, [NotNull] DataBundle content, [NotNull] DTypeMeta typeMeta, [NotNull] IList<StoreSet> toStoreSets);

		void Load([NotNull] IBasicEntity context, [NotNull] DataBundle content, [NotNull] DTypeMeta typeMeta);
	}
}