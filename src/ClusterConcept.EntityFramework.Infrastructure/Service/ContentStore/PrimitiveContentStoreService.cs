//BS:Frei Wild - Freundschaft

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using ClusterConcept.EntityFramework.Infrastructure.Interfaces;
using ClusterConcept.EntityFramework.Infrastructure.Service.Event;
using ClusterConcept.EntityFramework.Infrastructure.Service.ObjectStore;
using ClusterConcept.EntityFramework.Model.Base;
using ClusterConcept.EntityFramework.Model.Entity;
using ClusterConcept.EntityFramework.Model.Internal;
using ClusterConcept.EntityFramework.Model.Internal.Type.Meta;
using ClusterConcept.Shared.Model.ComEntity.Type;
using ClusterConcept.Shared.Model.Enums;
using ClusterConcept.Shared.Model.Events;
using Fijo.Infrastructure.DesignPattern.Events;
using Fijo.Infrastructure.DesignPattern.Repository.Collection;
using FijoCore.Infrastructure.DependencyInjection.InitKernel;
using FijoCore.Infrastructure.LightContrib.Extentions.IDictionary.Generic;
using FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic;
using FijoCore.Infrastructure.LightContrib.Factories.Interface;
using FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic.KeyValuePair;
using FijoCore.Infrastructure.LightContrib.Module.Transaction;
using JetBrains.Annotations;
using DTypeMeta = ClusterConcept.EntityFramework.Model.Internal.Type.Meta.TypeMeta;

namespace ClusterConcept.EntityFramework.Infrastructure.Service.ContentStore {
	[UsedImplicitly]
	public class PrimitiveContentStoreService : IContentStoreService {
		private readonly IPairFactory _pairFactory = Kernel.Resolve<IPairFactory>();
		// ToDo use another key inststead of Type
		private readonly IDictionary<Type, IPrimitiveHandling> _primitiveHandlings;
		private readonly IChangeEventService _changeEventService = Kernel.Resolve<IChangeEventService>();
		private readonly IStoreSetService _storeSetService = Kernel.Resolve<IStoreSetService>();

		public PrimitiveContentStoreService() {
			_primitiveHandlings = Kernel.Resolve<ICollectionRepository<KeyValuePair<Type, IPrimitiveHandling>>>().Get()
				.ToDict();
		}

		#region Implementation of IContentStoreService<object>
		public DataType DataType { get { return DataType.Primitive; } }

		public void Save(IBasicEntity context, PBasicEntityType pContextKey, DataBundle content, DTypeMeta typeMeta, IList<StoreSet> toStoreSets) {
			var subPrimitives = content.Data.SubPrimitives;

			ICollection<KeyValuePair<PType, IList<byte>>> changedSubPrimitives;
			lock (subPrimitives) changedSubPrimitives = GetChangedPrimitives(context, subPrimitives, typeMeta).Execute();
			if (changedSubPrimitives.Any())
				_storeSetService.StoreChangeset(toStoreSets,
												pContextKey,
												CreateStorePrimitiveEvent(subPrimitives));
		}

		[NotNull, Pure]
		private IEvent CreateStorePrimitiveEvent([NotNull] IEnumerable<KeyValuePair<PType, IList<byte>>> changedSubEntities) {
			return new ChangePrimitiveEvent
			{
				Changeset = changedSubEntities.ToDict()
			};
		}

		// ToDo may do not always serialize all candidates (find a way to do this)
		private IEnumerable<KeyValuePair<PType, IList<byte>>> GetChangedPrimitives(IBasicEntity context, IDictionary<PType, IList<byte>> subPrimitives, [NotNull] DTypeMeta typeMeta) {
			foreach (var propertyInfo in typeMeta.Primitives) {
				var key = propertyInfo.PKey;
				var bytes = Serialize(propertyInfo.Getter(context), propertyInfo.TypeMeta.Type).ToList();
				IList<byte> value;
				if (subPrimitives.TryGetValue(key, out value) && value.SequenceEqual(bytes)) continue;
				subPrimitives[key] = bytes;
				yield return _pairFactory.KeyValuePair<PType, IList<byte>>(key, bytes);
			}
		}

		public void Load(IBasicEntity context, DataBundle content, DTypeMeta typeMeta) {
			var subPrimitives = _changeEventService.GetPrimitiveChanges(content);
			if (subPrimitives == null) return;
			lock(subPrimitives)
				foreach (var propertyInfo in typeMeta.Primitives)
					LoadEntry(context, subPrimitives, propertyInfo);
		}

		private void LoadEntry(IBasicEntity context, IDictionary<PType, IList<byte>> subPrimitives, PropertyMeta propertyInfo) {
			subPrimitives.TryGet(propertyInfo.PKey, value => propertyInfo.Setter(context, Deserialize(value, propertyInfo.TypeMeta.Type)));
		}

		[NotNull]
		private IEnumerable<byte> Serialize(object value, [NotNull] Type type) {
			return GetPrimitiveHandling(type).Serialize(value);
		}

		private object Deserialize([NotNull] IEnumerable<byte> bytes, [NotNull] Type type) {
			return GetPrimitiveHandling(type).Deserialize(bytes);
		}

		[NotNull, Pure]
		private IPrimitiveHandling GetPrimitiveHandling([NotNull] Type type) {
			#region PreCondition
			Debug.Assert(_primitiveHandlings.ContainsKey(type));
			#endregion
			return _primitiveHandlings[type];
		}
		#endregion
	}
}