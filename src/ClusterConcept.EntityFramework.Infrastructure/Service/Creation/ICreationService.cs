using ClusterConcept.EntityFramework.Model.Internal.Type.Meta;

namespace ClusterConcept.EntityFramework.Infrastructure.Service.Creation {
	public interface ICreationService<out T> {
		bool AutoCreate(PropertyMeta propertyMeta);
		T Create(PropertyMeta propertyMeta);
	}
}