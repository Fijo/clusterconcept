//BS:Green Day - 14 Reject

using ClusterConcept.EntityFramework.Model.Entity;
using ClusterConcept.EntityFramework.Model.Internal.Type.Meta;
using Fijo.Infrastructure.DesignPattern.Lazy.Interface;
using FijoCore.Infrastructure.LightContrib.Module.Lazy;
using JetBrains.Annotations;

namespace ClusterConcept.EntityFramework.Infrastructure.Service.Creation {
	[UsedImplicitly]
	public class EntityCreationService : ICreationService<IEntity> {
		private readonly ILazy<IEntityService> _entityService = new LazyInject<IEntityService>();

		#region Implementation of ICreationService<out IEntity>
		public bool AutoCreate(PropertyMeta propertyMeta) {
			return true;
		}

		public IEntity Create(PropertyMeta propertyMeta) {
			return _entityService.Get().InternalCreate((EntityTypeMeta) propertyMeta.TypeMeta);
		}
		#endregion
	}
}