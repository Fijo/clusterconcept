//BS:Frei Wild - Eines Tages.

using System.Collections.Generic;
using System.Linq;
using ClusterConcept.EntityFramework.Infrastructure.Service.Data.Bundle;
using ClusterConcept.EntityFramework.Model.Internal;
using ClusterConcept.Shared.Model.ComEntity.Type;
using ClusterConcept.Shared.Model.Events;
using Fijo.Infrastructure.DesignPattern.Events;
using FijoCore.Infrastructure.DependencyInjection.InitKernel;
using FijoCore.Infrastructure.LightContrib.Module.Transaction;
using JetBrains.Annotations;

namespace ClusterConcept.EntityFramework.Infrastructure.Service.Event {
	[UsedImplicitly]
	public class ChangeEventService : IChangeEventService {
		private readonly ITransactionService _transactionService = Kernel.Resolve<ITransactionService>();
		private readonly IDataBundleService _dataBundleService = Kernel.Resolve<IDataBundleService>();

		public IDictionary<PType, long> GetEntityChanges(DataBundle dataBundle) {
			var events = GetEventsToPatch(dataBundle)
				.OfType<ChangeEntityEvent>();
			var changeEntityEvent = events.SingleOrDefault();
			if(changeEntityEvent == null) return new Dictionary<PType, long>();
			return changeEntityEvent.Changeset;
		}

		public IDictionary<PType, IList<byte>> GetPrimitiveChanges(DataBundle dataBundle) {
			var events = GetEventsToPatch(dataBundle)
				.OfType<ChangePrimitiveEvent>();
			var primitiveEvent = events.SingleOrDefault();
			if(primitiveEvent == null) return new Dictionary<PType, IList<byte>>();
			return primitiveEvent.Changeset;
		}

		[NotNull]
		private IEnumerable<IEvent> GetEventsToPatch([NotNull] DataBundle dataBundle) {
			var objectRevision = dataBundle.ObjectRevision;
			return _transactionService.GetEvents(dataBundle.Chnageset).Where(x => x.Revision > objectRevision);
		}

		public void SetPatched(DataBundle dataBundle) {
			// I�m able to free some memory here because I can delete some old events
			dataBundle.ObjectRevision = _dataBundleService.GetEndRevision(dataBundle);
		}
	}
}