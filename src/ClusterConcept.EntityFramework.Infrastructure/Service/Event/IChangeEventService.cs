using System.Collections.Generic;
using ClusterConcept.EntityFramework.Model.Internal;
using ClusterConcept.Shared.Model.ComEntity.Type;
using JetBrains.Annotations;

namespace ClusterConcept.EntityFramework.Infrastructure.Service.Event {
	public interface IChangeEventService {
		[Pure, CanBeNull] IDictionary<PType, long> GetEntityChanges([NotNull] DataBundle dataBundle);
		[Pure, CanBeNull] IDictionary<PType, IList<byte>> GetPrimitiveChanges([NotNull] DataBundle dataBundle);
		void SetPatched([NotNull] DataBundle dataBundle);
	}
}