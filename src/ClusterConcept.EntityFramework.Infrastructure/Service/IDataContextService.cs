using ClusterConcept.EntityFramework.Model.DataContext;
using ClusterConcept.EntityFramework.Model.Internal.Type.Meta;

namespace ClusterConcept.EntityFramework.Infrastructure.Service {
	public interface IDataContextService {
		void Save(IDataContext context);
		IDataContext Get(ContextTypeMeta typeMeta);
	}
}