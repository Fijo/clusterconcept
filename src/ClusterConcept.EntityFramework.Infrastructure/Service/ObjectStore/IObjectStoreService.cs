using System.Collections.Generic;
using ClusterConcept.EntityFramework.Model.Base;
using FijoCore.Infrastructure.LightContrib.Module.Transaction;
using DTypeMeta = ClusterConcept.EntityFramework.Model.Internal.Type.Meta.TypeMeta;

namespace ClusterConcept.EntityFramework.Infrastructure.Service.ObjectStore {
	public interface IObjectStoreService<in TBasicEntity> where TBasicEntity : IBasicEntity {
		void Save(TBasicEntity basicEntity, DTypeMeta typeMeta, IList<StoreSet> toStoreSets);
		void Load(TBasicEntity basicEntity, DTypeMeta typeMeta);
	}
}