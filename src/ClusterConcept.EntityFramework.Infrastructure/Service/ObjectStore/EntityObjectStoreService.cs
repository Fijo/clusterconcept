using System.Collections.Generic;
using ClusterConcept.EntityFramework.Model.Entity;
using ClusterConcept.Shared.Model.ComEntity.Type;
using ClusterConcept.Shared.Model.Enums;
using JetBrains.Annotations;

namespace ClusterConcept.EntityFramework.Infrastructure.Service.ObjectStore {
	[UsedImplicitly]
	public class EntityObjectStoreService : ObjectStoreService<IEntity> {
		#region Overrides of ObjectStoreService<PContextType,ContextType,IDataContext>
		protected override DataType DataType { get { return DataType.Entity; } }

		protected override IEnumerable<DataType> GetHandleContentFilter() {
			return new List<DataType>
			{
				DataType.Entity,
				DataType.Primitive
			};
		}

		protected override PBasicEntityType GetBasicKey(IEntity entity, Model.Internal.Type.Meta.TypeMeta typeMeta) {
			return new PBasicEntityType {DataType = DataType, Id = entity.Id, Type = typeMeta.PKey};
		}
		#endregion
	}
}