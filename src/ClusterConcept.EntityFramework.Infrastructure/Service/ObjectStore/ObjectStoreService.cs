//BS:Frei Wild - Der aufrechte Weg

using System.Collections.Generic;
using System.Linq;
using ClusterConcept.EntityFramework.Infrastructure.Service.ContentStore;
using ClusterConcept.EntityFramework.Infrastructure.Service.Event;
using ClusterConcept.EntityFramework.Model.Base;
using ClusterConcept.EntityFramework.Model.Internal;
using ClusterConcept.Shared.Model.ComEntity.Type;
using ClusterConcept.Shared.Model.Enums;
using FijoCore.Infrastructure.DependencyInjection.InitKernel;
using FijoCore.Infrastructure.LightContrib.Extentions.Generic;
using FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic;
using Ninject;
using DTypeMeta = ClusterConcept.EntityFramework.Model.Internal.Type.Meta.TypeMeta;

namespace ClusterConcept.EntityFramework.Infrastructure.Service.ObjectStore {
	public abstract class ObjectStoreService<TBasicEntity> : IObjectStoreService<TBasicEntity> where TBasicEntity : class, IBasicEntity {
		private readonly ICollection<IContentStoreService> _contentStoreServices;
		private readonly IChangeEventService _changeEventService = Kernel.Resolve<IChangeEventService>();
		private readonly IObjectStateService _objectStateService = Kernel.Resolve<IObjectStateService>();

		protected ObjectStoreService() {
			var handleContentFilter = GetHandleContentFilter().Execute();
			_contentStoreServices = Kernel.Inject.GetAll<IContentStoreService>()
				.Where(x => x.DataType.IsIn(handleContentFilter))
				.Execute();
		}

		protected abstract DataType DataType { get; }

		protected abstract IEnumerable<DataType> GetHandleContentFilter();

		public virtual void Save(TBasicEntity basicEntity, DTypeMeta typeMeta, IList<StoreSet> toStoreSets) {
			lock(basicEntity) {
				var basicKey = GetBasicKey(basicEntity, typeMeta);
				var content = LoadData(basicKey);
				foreach (var contentStoreService in _contentStoreServices) contentStoreService.Save(basicEntity, basicKey, content, typeMeta, toStoreSets);
			}
		}

		public virtual void Load(TBasicEntity basicEntity, DTypeMeta typeMeta) {
			lock(basicEntity) {
				var content = LoadData(GetBasicKey(basicEntity, typeMeta));
				foreach (var contentSaveService in _contentStoreServices)
					contentSaveService.Load(basicEntity, content, typeMeta);
				_changeEventService.SetPatched(content);
			}
		}

		protected virtual DataBundle LoadData(PBasicEntityType key) {
			return _objectStateService.Get(key).DataBundle;
		}

		protected abstract PBasicEntityType GetBasicKey(TBasicEntity basicEntity, DTypeMeta typeMeta);
	}
}