using System.Collections.Generic;
using ClusterConcept.EntityFramework.Model.DataContext;
using ClusterConcept.Shared.Model.ComEntity.Type;
using ClusterConcept.Shared.Model.Enums;
using JetBrains.Annotations;

namespace ClusterConcept.EntityFramework.Infrastructure.Service.ObjectStore {
	[UsedImplicitly]
	public class ContextObjectStoreService : ObjectStoreService<IDataContext> {
		#region Overrides of ObjectStoreService<PContextType,ContextType,IDataContext>
		protected override DataType DataType { get { return DataType.DataContext; } }

		protected override IEnumerable<DataType> GetHandleContentFilter() {
			return new List<DataType>	{
				DataType.DataContext,
				DataType.Entity
			};
		}

		protected override PBasicEntityType GetBasicKey(IDataContext entity, Model.Internal.Type.Meta.TypeMeta typeMeta) {
			return new PBasicEntityType {DataType = DataType, Type = typeMeta.PKey};
		}
		#endregion
	}
}