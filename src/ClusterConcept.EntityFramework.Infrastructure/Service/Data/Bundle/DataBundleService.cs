using System.Linq;
using ClusterConcept.EntityFramework.Model.Internal;
using FijoCore.Infrastructure.LightContrib.Extentions.ICollection.Generic;
using JetBrains.Annotations;

namespace ClusterConcept.EntityFramework.Infrastructure.Service.Data.Bundle {
	[UsedImplicitly]
	public class DataBundleService : IDataBundleService {
		public ulong GetBeginRevision(DataBundle dataBundle) {
			// ToDo may speedup this
			var events = dataBundle.Chnageset.Events;
			if(events.None()) return 0;
			return events.Min(x => x.Revision);
		}

		public ulong GetEndRevision(DataBundle dataBundle) {
			// ToDo may speedup this
			var events = dataBundle.Chnageset.Events;
			if(events.None()) return 0;
			return events.Max(x => x.Revision);
		}
	}
}