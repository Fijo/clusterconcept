namespace ClusterConcept.EntityFramework.Infrastructure.Service.Data.Bundle {
	public interface IDataBundleService {
		ulong GetBeginRevision(Model.Internal.DataBundle dataBundle);
		ulong GetEndRevision(Model.Internal.DataBundle dataBundle);
	}
}