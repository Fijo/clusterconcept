using System.Collections.Generic;
using ClusterConcept.EntityFramework.Model.Internal;
using ClusterConcept.Shared.Model.ComEntity;
using ClusterConcept.Shared.Model.ComEntity.Type;
using FijoCore.Infrastructure.LightContrib.Module.Transaction;
using JetBrains.Annotations;

namespace ClusterConcept.EntityFramework.Infrastructure.Service.Data.Bundle {
	[UsedImplicitly]
	public class DataBundleFactory : IDataBundleFactory {
		public DataBundle Create(Transaction chnageset) {
			return new DataBundle	{
				Data = new PBasicEntity	{
					SubEntityIds = new Dictionary<PType, long>(),
					SubPrimitives = new Dictionary<PType, IList<byte>>(),
				},
				Chnageset = chnageset
			};
		}
	}
}