using ClusterConcept.EntityFramework.Model.Internal;
using FijoCore.Infrastructure.LightContrib.Module.Transaction;

namespace ClusterConcept.EntityFramework.Infrastructure.Service.Data.Bundle {
	public interface IDataBundleFactory {
		DataBundle Create(Transaction chnageset);
	}
}