using System.Collections.Generic;
using System.Linq;
using ClusterConcept.Shared.Infrastructure.Server.Com;
using ClusterConcept.Shared.Model;
using ClusterConcept.Shared.Model.ComEntity.Type;
using Fijo.Infrastructure.DesignPattern.Events;
using Fijo.Net.Nodelt.Client.Service;
using Fijo.Net.Nodelt.Handler;
using Fijo.Net.Nodelt.Provider;
using FijoCore.Infrastructure.DependencyInjection.InitKernel;

namespace ClusterConcept.EntityFramework.Infrastructure.Service.ObjectStore {
	public class ClusterBasedDataAccessService : IDataAccessService {
		private readonly INodeClientComService<NodeCommand> _nodeClientComService = Kernel.Resolve<NodeClientComService<NodeCommand>>();
		private readonly IHandlerProvider<NodeCommand> _handlerProvider = Kernel.Resolve<IHandlerProvider<NodeCommand>>();
		private readonly IHandler<GetQuery, IList<IEvent>, NodeCommand> _getHandler;
		private readonly ISendonlyHandler<PostQuery, NodeCommand> _postHandler;

		public ClusterBasedDataAccessService() {
			_getHandler = (IHandler<GetQuery, IList<IEvent>, NodeCommand>) _handlerProvider.Get(NodeCommand.Get);
			_postHandler = (ISendonlyHandler<PostQuery, NodeCommand>) _handlerProvider.Get(NodeCommand.Post);
		}
		#region Implementation of IDataAccessService
		public IEnumerable<IEvent> Fetch(PBasicEntityType key, ulong fromRevision, ulong toRevision) {
			return InternalFetch(GetGetQuery(key, fromRevision, toRevision));
		}

		private GetQuery GetGetQuery(PBasicEntityType key, ulong fromRevision, ulong toRevision) {
			return new GetQuery {Key = key, FromRevision = fromRevision, ToRevision = toRevision};
		}

		private IEnumerable<IEvent> InternalFetch(GetQuery querry) {
			return _nodeClientComService.ForceSend(_getHandler, querry);
		}

		public void Store(PBasicEntityType key, IEnumerable<IEvent> events) {
			_nodeClientComService.ForceSend(_postHandler, GetPostQuery(key, events));
		}

		private PostQuery GetPostQuery(PBasicEntityType key, IEnumerable<IEvent> events) {
			return new PostQuery {Key = key, Events = events.ToList()};
		}
		#endregion
	}
}