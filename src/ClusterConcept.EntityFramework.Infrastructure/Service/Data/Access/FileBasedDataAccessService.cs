using System.Collections.Generic;
using System.IO;
using System.Linq;
using ClusterConcept.Shared.Model.ComEntity.Type;
using Fijo.Infrastructure.DesignPattern.Events;
using FijoCore.Infrastructure.DependencyInjection.InitKernel;
using FijoCore.Infrastructure.LightContrib.Enums;
using FijoCore.Infrastructure.LightContrib.Module.Configuration;
using FijoCore.Infrastructure.LightContrib.Module.Path;
using FijoCore.Infrastructure.LightContrib.Module.Serialization;
using JetBrains.Annotations;

namespace ClusterConcept.EntityFramework.Infrastructure.Service.ObjectStore {
	[UsedImplicitly]
	public class FileBasedDataAccessService : IDataAccessService {
		private readonly ISerializationFacade _serializationFacade = Kernel.Resolve<ISerializationFacade>();
		private readonly string _dataPath = Kernel.Resolve<IConfigurationService>().Get<string>(typeof (FileBasedDataAccessService), "DataPath");
		private readonly IPathService _pathService = Kernel.Resolve<IPathService>();

		public FileBasedDataAccessService() {
			if(!Directory.Exists(_dataPath)) Directory.CreateDirectory(_dataPath);
		}

		#region Implementation of IDataAccessService
		public IEnumerable<IEvent> Fetch(PBasicEntityType key, ulong fromRevision, ulong toRevision) {
			var path = GetPath(key);
			// It is possible, that a event do not exist yet - so there are no revisions to return .D
			if(!File.Exists(path)) return Enumerable.Empty<IEvent>();
			using(var stream = File.OpenRead(path))
				return _serializationFacade.Deserialize<IEvent[]>(SerializationFormat.Binary, stream);
		}

		public void Store(PBasicEntityType key, IEnumerable<IEvent> events) {
			using(var stream = File.OpenWrite(GetPath(key)))
				_serializationFacade.Serialize(SerializationFormat.Binary, events.ToArray(), stream);
		}

		private string GetPath(PBasicEntityType key) {
			return Path.Combine(_dataPath, GetName(key));
		}

		private string GetName(PBasicEntityType key) {
			return _pathService.Normalize(string.Join("|", new[]
			{
				key.DataType.ToString(),
				key.Type.UId.Replace("|", @"\|"),
				key.Id.ToString("x")
			}));
		}
		#endregion
	}
}