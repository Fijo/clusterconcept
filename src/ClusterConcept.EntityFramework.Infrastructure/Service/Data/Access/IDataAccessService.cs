using System.Collections.Generic;
using ClusterConcept.Shared.Model.ComEntity.Type;
using Fijo.Infrastructure.DesignPattern.Events;

namespace ClusterConcept.EntityFramework.Infrastructure.Service.ObjectStore {
	public interface IDataAccessService {
		IEnumerable<IEvent> Fetch(PBasicEntityType key, ulong fromRevision, ulong toRevision);
		void Store(PBasicEntityType key, IEnumerable<IEvent> events);
	}
}