using ClusterConcept.EntityFramework.Model.Internal;
using ClusterConcept.Shared.Model.ComEntity.Type;
using ClusterConcept.Shared.Model.Enums;

namespace ClusterConcept.EntityFramework.Infrastructure.Service.Data.Access {
	public interface IInternalDataAccessService {
		DataBundle Get(DataType dataType, PType contextType);
	}
}