using ClusterConcept.EntityFramework.Infrastructure.Service.Data.Access.Spec;
using ClusterConcept.EntityFramework.Model.Internal;
using ClusterConcept.Shared.Model.ComEntity.Type;
using ClusterConcept.Shared.Model.Enums;
using FijoCore.Infrastructure.DependencyInjection.InitKernel;
using FijoCore.Infrastructure.LightContrib.Module.Generics.Lookup;
using JetBrains.Annotations;

namespace ClusterConcept.EntityFramework.Infrastructure.Service.Data.Access {
	[UsedImplicitly]
	public class InternalDataAccessObjectsProvider : IInternalDataAccessObjectsProvider {
		private readonly IGenericLookup<IInternalDataAccessSpecObjectsProvider, DataType> _providerLookup = Kernel.Resolve<IGenericLookup<IInternalDataAccessSpecObjectsProvider, DataType>>();

		#region Implementation of IInternalDataAccessObjectsProvider
		public DataBundle Get(DataType dataType, PType pType) {
			return _providerLookup.Get(dataType).Get(pType);
		}

		public void Set(DataType dataType, PType pType, DataBundle dataBundle) {
			_providerLookup.Get(dataType).Set(pType, dataBundle);
		}
		#endregion
	}
}