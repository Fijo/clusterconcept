using ClusterConcept.EntityFramework.Model.Internal;
using ClusterConcept.Shared.Model.ComEntity.Type;
using ClusterConcept.Shared.Model.Enums;
using JetBrains.Annotations;

namespace ClusterConcept.EntityFramework.Infrastructure.Service.Data.Access {
	public interface IInternalDataAccessObjectsProvider {
		[CanBeNull]
		DataBundle Get(DataType dataType, [NotNull] PType pType);
		void Set(DataType dataType, [NotNull] PType pType, [NotNull] DataBundle dataBundle);
	}
}