using System.Collections.Generic;
using ClusterConcept.Shared.Model.ComEntity.Type;
using ClusterConcept.Shared.Model.Enums;
using FijoCore.Infrastructure.LightContrib.Extentions.IDictionary.Generic;

namespace ClusterConcept.EntityFramework.Infrastructure.Service.Data.Access.Spec {
	public abstract class InternalDataAccessSpecObjectsProvider : IInternalDataAccessSpecObjectsProvider {
		private readonly IDictionary<PType, Model.Internal.DataBundle> _content = new Dictionary<PType, Model.Internal.DataBundle>();
		
		#region Implementation of IInternalDataAccessObjectsProvider
		public abstract DataType DataType { get; }

		public Model.Internal.DataBundle Get(PType contextType) {
			return _content.TryGet(contextType);
		}

		public void Set(PType contextType, Model.Internal.DataBundle dataBundle) {
			_content[contextType] = dataBundle;
		}
		#endregion
	}
}