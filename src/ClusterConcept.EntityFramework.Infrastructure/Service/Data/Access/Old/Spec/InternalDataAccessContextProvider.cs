using ClusterConcept.Shared.Model.Enums;
using JetBrains.Annotations;

namespace ClusterConcept.EntityFramework.Infrastructure.Service.Data.Access.Spec {
	[UsedImplicitly]
	public class InternalDataAccessContextProvider : InternalDataAccessSpecObjectsProvider {
		#region Overrides of InternalDataAccessObjectsProvider
		public override DataType DataType { get { return DataType.DataContext; } }
		#endregion
	}
}