using ClusterConcept.Shared.Model.ComEntity.Type;
using ClusterConcept.Shared.Model.Enums;
using JetBrains.Annotations;

namespace ClusterConcept.EntityFramework.Infrastructure.Service.Data.Access.Spec {
	public interface IInternalDataAccessSpecObjectsProvider {
		DataType DataType { get; }
		[CanBeNull]
		Model.Internal.DataBundle Get([NotNull] PType contextType);
		void Set([NotNull] PType contextType, [NotNull] Model.Internal.DataBundle dataBundle);
	}
}