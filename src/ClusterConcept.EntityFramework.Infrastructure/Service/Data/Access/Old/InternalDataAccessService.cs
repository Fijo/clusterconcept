//BS:Frei Wild - Freiheit

using System.Collections.Generic;
using ClusterConcept.EntityFramework.Model.Internal;
using ClusterConcept.Shared.Model.ComEntity;
using ClusterConcept.Shared.Model.ComEntity.Type;
using ClusterConcept.Shared.Model.Enums;
using FijoCore.Infrastructure.DependencyInjection.InitKernel;
using FijoCore.Infrastructure.LightContrib.Module.Transaction;
using JetBrains.Annotations;

namespace ClusterConcept.EntityFramework.Infrastructure.Service.Data.Access {
	[UsedImplicitly]
	public class InternalDataAccessService : IInternalDataAccessService {
		private readonly ITransactionFactory _transactionFactory = Kernel.Resolve<ITransactionFactory>();
		private readonly IInternalDataAccessObjectsProvider _internalDataAccessObjectsProvider = Kernel.Resolve<IInternalDataAccessObjectsProvider>();
		private readonly ITransactionService _transactionService = Kernel.Resolve<ITransactionService>();

		#region Implementation of IInternalDataAccessService<in TPersistType>
		public DataBundle Get(DataType dataType, PType contextType) {
			var dataBundle = _internalDataAccessObjectsProvider.Get(dataType, contextType);
			if(dataBundle != null) return dataBundle;
			dataBundle = Create();
			_internalDataAccessObjectsProvider.Set(dataType, contextType, dataBundle);
			return dataBundle;
		}

		private DataBundle Create() {
			var chnageset = _transactionFactory.CreateTransaction();
			var dataBundle = new DataBundle	{
				Data = new PBasicEntity	{
					SubEntityIds = new Dictionary<PType, long>(),
					SubPrimitives = new Dictionary<PType, IList<byte>>(),
				},
				Chnageset = chnageset
			};

			return dataBundle;
		}
		#endregion
	}
}