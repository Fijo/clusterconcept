using ClusterConcept.Shared.Model.ComEntity.Type;

namespace ClusterConcept.EntityFramework.Infrastructure.Service.ContentStore {
	public interface IPBasicEntityTypeFactory {
		PBasicEntityType CreateForEntity(PType pType, long id);
		PBasicEntityType CreateForContext(PType pType);
	}
}