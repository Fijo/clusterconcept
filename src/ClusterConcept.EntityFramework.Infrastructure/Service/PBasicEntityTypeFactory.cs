using ClusterConcept.Shared.Model.ComEntity.Type;
using ClusterConcept.Shared.Model.Enums;
using JetBrains.Annotations;

namespace ClusterConcept.EntityFramework.Infrastructure.Service.ContentStore {
	[UsedImplicitly]
	public class PBasicEntityTypeFactory : IPBasicEntityTypeFactory {
		public PBasicEntityType CreateForEntity(PType pType, long id) {
			return new PBasicEntityType
			{
				Type = pType,
				DataType = DataType.Entity,
				Id = id
			};
		}

		public PBasicEntityType CreateForContext(PType pType) {
			return new PBasicEntityType
			{
				Type = pType,
				DataType = DataType.DataContext,
			};
		}
	}
}