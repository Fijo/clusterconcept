using System;
using ClusterConcept.EntityFramework.Model.Entity;
using ClusterConcept.EntityFramework.Model.Internal.Type.Meta;
using ClusterConcept.Shared.Model.ComEntity.Type;

namespace ClusterConcept.EntityFramework.Infrastructure.Service {
	public interface IEntityService	{
		void Save(IEntity entity);
		IEntity Get(Type type, long id);
		IEntity InternalGet(long id, PType pType);
		IEntity Create(Type type);
		IEntity InternalCreate(PType pType);
		IEntity InternalCreate(EntityTypeMeta entityTypeMeta);
		IEntity InternalGet(long id, EntityTypeMeta entityTypeMeta);
		void InternalSave(IEntity entity, Model.Internal.Type.Meta.TypeMeta typeMeta);
	}
}