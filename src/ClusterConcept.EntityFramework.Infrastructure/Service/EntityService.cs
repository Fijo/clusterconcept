//BS:Green Day - 10 When I Come Around

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using ClusterConcept.EntityFramework.Infrastructure.Factory;
using ClusterConcept.EntityFramework.Infrastructure.Service.ContentStore;
using ClusterConcept.EntityFramework.Infrastructure.Service.ObjectStore;
using ClusterConcept.EntityFramework.Model.Entity;
using ClusterConcept.EntityFramework.Model.Internal.Type.Meta;
using ClusterConcept.Shared.Model.ComEntity.Type;
using Fijo.Infrastructure.DesignPattern.Repository;
using FijoCore.Infrastructure.DependencyInjection.InitKernel;
using FijoCore.Infrastructure.LightContrib.Extentions.IDictionary.Generic;
using FijoCore.Infrastructure.LightContrib.Extentions.Type;
using JetBrains.Annotations;

namespace ClusterConcept.EntityFramework.Infrastructure.Service {
	[UsedImplicitly]
	public class EntityService : IEntityService {
		private readonly IObjectStoreService<IEntity> _objectStoreService = Kernel.Resolve<IObjectStoreService<IEntity>>();
		private readonly TypeMetas _typeMetas = Kernel.Resolve<IRepository<TypeMetas>>().Get();
		private readonly IStoreSetService _storeSetService = Kernel.Resolve<IStoreSetService>();
		private readonly IEntityFactory _entityFactory = Kernel.Resolve<IEntityFactory>();

		#region Implementation of IEntityService
		public void Save(IEntity entity) {
			InternalSave(entity, GetMeta(entity.GetType()));
		}

		public void InternalSave(IEntity entity, Model.Internal.Type.Meta.TypeMeta typeMeta) {
			var toStoreSets = new List<StoreSet>();
			_objectStoreService.Save(entity, typeMeta, toStoreSets);
			_storeSetService.Save(toStoreSets);
		}

		public IEntity Get(Type type, long id) {
			#region PreCondition
			Debug.Assert(type.ImplementsInteface<IEntity>());
			#endregion
			return InternalGet(id, GetMeta(type));
		}
		
		public virtual IEntity InternalGet(long id, PType pType) {
			return InternalGet(id, GetMeta(pType));
		}

		public virtual IEntity InternalGet(long id, EntityTypeMeta entityTypeMeta) {
			return entityTypeMeta.Data.Entities.GetOrCreate(id, () => InternalGet(entityTypeMeta, id));
		}

		protected virtual IEntity InternalGet(EntityTypeMeta entityTypeMeta, long id) {
			var entity = _entityFactory.Create(entityTypeMeta, id);
			Load(entityTypeMeta, entity);
			return entity;
		}

		private void Load(EntityTypeMeta entityTypeMeta, IEntity entity) {
			_objectStoreService.Load(entity, entityTypeMeta);
		}

		public IEntity Create(Type type) {
			return InternalCreate(GetMeta(type));
		}

		public IEntity InternalCreate(PType pType) {
			return InternalCreate(GetMeta(pType));
		}

		public IEntity InternalCreate(EntityTypeMeta entityTypeMeta) {
			var entity = _entityFactory.Create(entityTypeMeta);
			Load(entityTypeMeta, entity);
			Debug.Assert(!entityTypeMeta.Data.Entities.ContainsKey(entity.Id),
			             string.Format("Created a entity with a key that already exists or that has not been delete full yet (a entity with the id {0} does already exist in the entity cache)", entity.Id));
			entityTypeMeta.Data.Entities.Add(entity.Id, entity);
			return entity;
		}

		private EntityTypeMeta GetMeta(PType pType) {
			return _typeMetas.Entities.Single(x => Equals(x.PKey, pType));
		}

		private EntityTypeMeta GetMeta(Type type) {
			return _typeMetas.Entities.Single(x => x.Type == type);
		}
		#endregion
	}
}