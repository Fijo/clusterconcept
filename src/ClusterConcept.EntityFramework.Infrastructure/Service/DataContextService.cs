//BS:Die �rzte - Deine Schuld (Cover)

using System;
using System.Collections.Generic;
using System.Linq;
using ClusterConcept.EntityFramework.Infrastructure.Service.ContentStore;
using ClusterConcept.EntityFramework.Infrastructure.Service.ObjectStore;
using ClusterConcept.EntityFramework.Model.DataContext;
using ClusterConcept.EntityFramework.Model.Internal.Type.Meta;
using Fijo.Infrastructure.DesignPattern.Repository;
using FijoCore.Infrastructure.DependencyInjection.InitKernel;
using JetBrains.Annotations;

namespace ClusterConcept.EntityFramework.Infrastructure.Service	{
	[UsedImplicitly]
	public class DataContextService : IDataContextService {
		private readonly IObjectStoreService<IDataContext> _objectStoreService = Kernel.Resolve<IObjectStoreService<IDataContext>>();
		private readonly IStoreSetService _storeSetService = Kernel.Resolve<IStoreSetService>();
		private readonly TypeMetas _typeMetas = Kernel.Resolve<IRepository<TypeMetas>>().Get();

		public virtual void Save(IDataContext context) {
			var toStoreSets = new List<StoreSet>();
			_objectStoreService.Save(context, GetMeta(context.GetType()), toStoreSets);
			_storeSetService.Save(toStoreSets);
		}

		public IDataContext Get(ContextTypeMeta typeMeta) {
			var context = (typeMeta.Context ?? (typeMeta.Context = InternalCreate(typeMeta)));
			InternalLoad(typeMeta, context);
			return context;
		}
		protected virtual IDataContext InternalCreate(ContextTypeMeta typeMeta) {
			return typeMeta.Create();
		}

		private void InternalLoad(ContextTypeMeta typeMeta, IDataContext context) {
			_objectStoreService.Load(context, typeMeta);
		}

		private ContextTypeMeta GetMeta(Type type) {
			return _typeMetas.Contextes.Single(x => x.Type == type);
		}
	}
}