using System;
using ClusterConcept.Shared.Model.ComEntity.Type;

namespace ClusterConcept.EntityFramework.Infrastructure.Service.KeyResolver {
	public interface IKeyResolver<in TKey> {
		PType Get(TKey key);
	}
}