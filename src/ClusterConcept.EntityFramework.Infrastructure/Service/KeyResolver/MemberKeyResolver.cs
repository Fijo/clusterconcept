using System.Reflection;
using ClusterConcept.Shared.Model.ComEntity.Type;
using JetBrains.Annotations;

namespace ClusterConcept.EntityFramework.Infrastructure.Service.KeyResolver {
	[UsedImplicitly]
	public class MemberKeyResolver : IKeyResolver<MemberInfo> {
		#region Implementation of IKeyResolver<in MemberInfo>
		public PType Get(MemberInfo key) {			
			return new PType(key.Name);
		}
		#endregion
	}
}