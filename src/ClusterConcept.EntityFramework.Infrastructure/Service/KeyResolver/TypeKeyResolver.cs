using System;
using ClusterConcept.Shared.Model.ComEntity.Type;
using JetBrains.Annotations;

namespace ClusterConcept.EntityFramework.Infrastructure.Service.KeyResolver {
	[UsedImplicitly]
	public class TypeKeyResolver : IKeyResolver<Type> {
		#region Implementation of IKeyResolver<in Type>
		public PType Get(Type key) {			
			return new PType(key.Name);
		}
		#endregion
	}
}