using ClusterConcept.Shared.Model.ComEntity.Type;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using JetBrains.Annotations;

namespace ClusterConcept.EntityFramework.Infrastructure.Service.ObjectStore {
	public interface IObjectStateService {
		[Note("Call this methode after you have created an object (call it internal in the EntityFramework)")]
		[Note("With create I mean if an object of the is created localy (not a creation in the the cluster or any persistant store or something like this)")]
		void Create([NotNull] PBasicEntityType key);

		[Note("Call this methode after you have deleted an object (call it internal in the EntityFramework)")]
		[Note("With delete I mean if an object of the is deleted localy (not a delete action in the the cluster or any persistant store or something like this)")]
		void Remove([NotNull] PBasicEntityType key);

		ObjectState Get([NotNull] PBasicEntityType key);
	}
}