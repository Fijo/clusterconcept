using ClusterConcept.EntityFramework.Infrastructure.Service.Data.Bundle;
using ClusterConcept.EntityFramework.Model.Internal;
using ClusterConcept.Shared.Model.ComEntity.Type;
using FijoCore.Infrastructure.DependencyInjection.InitKernel;
using FijoCore.Infrastructure.LightContrib.Extentions.Generic;
using FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic;
using FijoCore.Infrastructure.LightContrib.Module.Transaction;
using JetBrains.Annotations;

namespace ClusterConcept.EntityFramework.Infrastructure.Service.ObjectStore {
	[UsedImplicitly]
	public class ObjectStateService : IObjectStateService {
		private readonly IObjectsStateStock _objectsStateStock = Kernel.Resolve<IObjectsStateStock>();
		private readonly IDataAccessService _dataAccessService = Kernel.Resolve<IDataAccessService>();
		private readonly ITransactionService _transactionService = Kernel.Resolve<ITransactionService>();
		private readonly IDataBundleFactory _dataBundleFactory = Kernel.Resolve<IDataBundleFactory>();
		private readonly ITransactionFactory _transactionFactory = Kernel.Resolve<ITransactionFactory>();

		#region Implementation of IObjectStateService
		public void Create(PBasicEntityType key) {
			_objectsStateStock.Add(key, new ObjectState());
		}

		public void Remove(PBasicEntityType key) {
			// ToDo delete entity from types meta
			_objectsStateStock.Remove(key);
			// ToDo may delete using garbage collection
		}

		public ObjectState Get(PBasicEntityType key) {
			var objectState = _objectsStateStock.Get(key) ?? InternalCreate(key);
			lock(objectState) {
				MayFetch(objectState, key);
				return objectState;
			}
		}

		private void MayFetch(ObjectState state, PBasicEntityType key) {
			// do this only if the objectstate is not autofetched (if you have impl autofetching)
			var dataBundle = state.DataBundle ?? (state.DataBundle = _dataBundleFactory.Create(_transactionFactory.CreateTransaction()));
			var events = _dataAccessService.Fetch(key, dataBundle.ObjectRevision, ulong.MaxValue);
			var transaction = dataBundle.Chnageset;
			events.ForEach(x => _transactionService.AddEvent(transaction, x));
		}

		private ObjectState InternalCreate(PBasicEntityType key) {
			var objectState = new ObjectState();
			_objectsStateStock.Add(key, objectState);
			return objectState;
		}
		#endregion
	}
}