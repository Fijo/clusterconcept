using System.Collections.Generic;
using ClusterConcept.Shared.Model.ComEntity.Type;
using FijoCore.Infrastructure.LightContrib.Extentions.IDictionary.Generic;

namespace ClusterConcept.EntityFramework.Infrastructure.Service.ObjectStore {
	public class ObjectsStateStock : IObjectsStateStock {
		protected IDictionary<PBasicEntityType, ObjectState> ObjectStates = new Dictionary<PBasicEntityType, ObjectState>();

		public void Add(PBasicEntityType basicEntityType, ObjectState state) {
			ObjectStates.Add(basicEntityType, state);
		}
		
		public void Remove(PBasicEntityType basicEntityType) {
			ObjectStates.Remove(basicEntityType);
		}
		
		public ObjectState Get(PBasicEntityType basicEntityType) {
			return ObjectStates.TryGet(basicEntityType);
		}
		
		public IDictionary<PBasicEntityType, ObjectState> GetAll(PBasicEntityType basicEntityType) {
			return ObjectStates;
		}
	}
}