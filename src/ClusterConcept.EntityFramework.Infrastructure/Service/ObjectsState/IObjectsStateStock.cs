using System.Collections.Generic;
using ClusterConcept.Shared.Model.ComEntity.Type;

namespace ClusterConcept.EntityFramework.Infrastructure.Service.ObjectStore {
	public interface IObjectsStateStock {
		void Add(PBasicEntityType basicEntityType, ObjectState state);
		void Remove(PBasicEntityType basicEntityType);
		ObjectState Get(PBasicEntityType basicEntityType);
		IDictionary<PBasicEntityType, ObjectState> GetAll(PBasicEntityType basicEntityType);
	}
}