using System.Diagnostics;
using ClusterConcept.Shared.Model.ComEntity.Type;
using Fijo.Infrastructure.Documentation.Attributes.DesignPattern;
using FijoCore.Infrastructure.LightContrib.Default.Service.EqualityHelpers;
using FijoCore.Infrastructure.LightContrib.Module.Transaction;

namespace ClusterConcept.EntityFramework.Infrastructure.Service.ObjectStore {
	[Dto]
	public class StoreSet {
		public Transaction Transaction;
		public PBasicEntityType Key;

		#region Equality
		protected bool Equals(StoreSet other) {
			#region PreCondition
			Debug.Assert(Key != null);
			Debug.Assert(other.Key != null);
			#endregion
			return Key.Equals(other.Key);
		}

		public override bool Equals(object obj) {
			return Equality.Equals<StoreSet>(obj, Equals);
		}

		public override int GetHashCode() {
			#region PreCondition
			Debug.Assert(Key != null);
			#endregion
			return Key.GetHashCode();
		}
		#endregion
	}
}