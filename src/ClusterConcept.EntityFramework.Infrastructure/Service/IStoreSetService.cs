using System.Collections.Generic;
using ClusterConcept.EntityFramework.Infrastructure.Service.ObjectStore;
using ClusterConcept.Shared.Model.ComEntity.Type;
using Fijo.Infrastructure.DesignPattern.Events;

namespace ClusterConcept.EntityFramework.Infrastructure.Service.ContentStore {
	public interface IStoreSetService {
		void StoreChangeset(IList<StoreSet> toStoreSets, PBasicEntityType key, IEvent @event);
		void Save(IList<StoreSet> toStoreSets);
	}
}