using System;
using ClusterConcept.EntityFramework.Model.DataContext;
using ClusterConcept.EntityFramework.Model.Internal.Type;
using ClusterConcept.EntityFramework.Model.Internal.Type.Meta;
using ClusterConcept.Shared.Model.Enums;
using FijoCore.Infrastructure.LightContrib.Extentions.Type;
using JetBrains.Annotations;

namespace ClusterConcept.EntityFramework.Infrastructure.Service.TypeMeta {
	[UsedImplicitly]
	public class ContextTypeMetaService : TypeMetaBaseEntityService<ContextTypeMeta> {
		#region Overrides of TypeMetaServiceBase
		public override DataType DataType { get { return DataType.DataContext; } }
		public override bool IsType(Type type) {
			return IsBasicEntity<IDataContext>(type);
		}

		protected override TypeData MapData(Type type) {
			return new TypeData();
		}

		public override ContextTypeMeta Map(Type type) {
			var contextTypeMeta = base.Map(type);
			contextTypeMeta.Create = () => type.New<IDataContext>();
			return contextTypeMeta;
		}
		#endregion
	}
}