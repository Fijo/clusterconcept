using System;
using System.Collections.Generic;
using System.Linq;
using ClusterConcept.EntityFramework.Infrastructure.Interfaces;
using ClusterConcept.EntityFramework.Model.Entity;
using ClusterConcept.EntityFramework.Model.Internal.Type;
using ClusterConcept.EntityFramework.Model.Internal.Type.Meta;
using ClusterConcept.Shared.Model.Enums;
using Fijo.Infrastructure.DesignPattern.Repository.Collection;
using FijoCore.Infrastructure.DependencyInjection.InitKernel;
using FijoCore.Infrastructure.LightContrib.Extentions.Generic;
using FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic;
using JetBrains.Annotations;

namespace ClusterConcept.EntityFramework.Infrastructure.Service.TypeMeta {
	[UsedImplicitly]
	public class PrimitiveTypeMetaService : TypeMetaBaseEntityService<PrimitiveTypeMeta> {
		private readonly ICollectionRepository<KeyValuePair<Type, IPrimitiveHandling>> _collectionRepository = Kernel.Resolve<ICollectionRepository<KeyValuePair<Type, IPrimitiveHandling>>>();
		private readonly ICollection<Type> _primitives;

		public PrimitiveTypeMetaService() {
			_primitives = GetPrimitives();
		}

		private ICollection<Type> GetPrimitives() {
			return _collectionRepository.Get()
				.Select(x => x.Key).Execute();
		}

		#region Overrides of TypeMetaServiceBase
		public override DataType DataType { get { return DataType.Primitive; } }
		public override bool IsType(Type type) {
			return type.IsIn(_primitives);
		}

		protected override TypeData MapData(Type type) {
			return new TypeData
			{
				Entities = new Dictionary<long, IEntity>()
			};
		}
		#endregion
	}
}