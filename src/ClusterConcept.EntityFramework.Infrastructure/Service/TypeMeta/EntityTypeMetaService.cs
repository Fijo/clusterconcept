using System;
using System.Collections.Generic;
using ClusterConcept.EntityFramework.Model.Entity;
using ClusterConcept.EntityFramework.Model.Internal.Type;
using ClusterConcept.EntityFramework.Model.Internal.Type.Meta;
using ClusterConcept.Shared.Model.Enums;
using FijoCore.Infrastructure.LightContrib.Extentions.Type;
using JetBrains.Annotations;

namespace ClusterConcept.EntityFramework.Infrastructure.Service.TypeMeta {
	[UsedImplicitly]
	public class EntityTypeMetaService : TypeMetaBaseEntityService<EntityTypeMeta> {
		#region Overrides of TypeMetaServiceBase
		public override DataType DataType { get { return DataType.Entity; } }
		public override bool IsType(Type type) {
			return IsBasicEntity<IEntity>(type);
		}

		protected override TypeData MapData(Type type) {
			return new TypeData
			{
				Entities = new Dictionary<long, IEntity>()
			};
		}

		public override EntityTypeMeta Map(Type type) {
			var entityTypeMeta = base.Map(type);
			entityTypeMeta.CurrentId = long.MinValue;
			entityTypeMeta.Create = () => type.New<IEntity>();
			return entityTypeMeta;
		}
		#endregion
	}
}