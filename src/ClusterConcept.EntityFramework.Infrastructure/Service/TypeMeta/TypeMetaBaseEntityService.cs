using System;
using System.Diagnostics;
using ClusterConcept.EntityFramework.Model.Base;
using FijoCore.Infrastructure.LightContrib.Extentions.Type;

namespace ClusterConcept.EntityFramework.Infrastructure.Service.TypeMeta {
	public abstract class TypeMetaBaseEntityService<T> : TypeMetaServiceBase<T> where T : Model.Internal.Type.Meta.TypeMeta, new() {
		protected bool IsBasicEntity<T>(Type type) where T : IBasicEntity {
			var implements = type.ImplementsInteface<T>();
			if (implements) Debug.Assert(type.HasEmptyConstructor());
			return implements;
		}
	}
}