using System;
using System.Reflection;
using ClusterConcept.EntityFramework.Model.Internal.Type.Meta;
using ClusterConcept.Shared.Model.Enums;

namespace ClusterConcept.EntityFramework.Infrastructure.Service.TypeMeta {
	public interface ITypeMetaServiceBase<out T> : ITypeMetaServiceBase where T : Model.Internal.Type.Meta.TypeMeta, new() {
		new T Map(Type type);
	}

	public interface ITypeMetaServiceBase {
		DataType DataType { get; }
		bool IsType(Type type);
		Model.Internal.Type.Meta.TypeMeta Map(Type type);
		PropertyMeta Map(PropertyInfo info);
	}
}