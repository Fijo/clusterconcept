using System;
using System.Reflection;
using ClusterConcept.EntityFramework.Infrastructure.Service.KeyResolver;
using ClusterConcept.EntityFramework.Model.Internal.Type;
using ClusterConcept.EntityFramework.Model.Internal.Type.Meta;
using ClusterConcept.Shared.Model.Enums;
using Fijo.Infrastructure.Documentation.Attributes.DesignPattern;
using FijoCore.Infrastructure.DependencyInjection.InitKernel;

namespace ClusterConcept.EntityFramework.Infrastructure.Service.TypeMeta {
	[Service]
	public abstract class TypeMetaServiceBase<T> : ITypeMetaServiceBase<T> where T : Model.Internal.Type.Meta.TypeMeta, new() {
		private readonly IKeyResolver<Type> _typeKeyResolver = Kernel.Resolve<IKeyResolver<Type>>();
		private readonly IKeyResolver<MemberInfo> _memberKeyResolver = Kernel.Resolve<IKeyResolver<MemberInfo>>();

		public abstract DataType DataType { get; }

		public abstract bool IsType(Type type);

		Model.Internal.Type.Meta.TypeMeta ITypeMetaServiceBase.Map(Type type) {
			return Map(type);
		}

		public virtual T Map(Type type) {
			return new T
			{
				Type = type,
				PKey = _typeKeyResolver.Get(type),
				DataType = DataType,
				Data = MapData(type)
			};
		}

		protected abstract TypeData MapData(Type type);

		#region Map PropertyInfo
		public virtual PropertyMeta Map(PropertyInfo info) {
			return new PropertyMeta
			{
				Setter = (me, value) => info.SetValue(me, value, null),
				Getter = me => info.GetValue(me, null),
				PKey = _memberKeyResolver.Get(info)
			};
		}
		#endregion
	}
}