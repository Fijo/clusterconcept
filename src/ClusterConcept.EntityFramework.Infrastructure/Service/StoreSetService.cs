using System;
using System.Collections.Generic;
using ClusterConcept.EntityFramework.Infrastructure.Service.ObjectStore;
using ClusterConcept.Shared.Model.ComEntity.Type;
using Fijo.Infrastructure.DesignPattern.Events;
using FijoCore.Infrastructure.DependencyInjection.InitKernel;
using FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic;
using FijoCore.Infrastructure.LightContrib.Module.Transaction;
using JetBrains.Annotations;

namespace ClusterConcept.EntityFramework.Infrastructure.Service.ContentStore {
	[UsedImplicitly]
	public class StoreSetService : IStoreSetService {
		private readonly ITransactionFactory _transactionFactory = Kernel.Resolve<ITransactionFactory>();
		private readonly ITransactionService _transactionService = Kernel.Resolve<ITransactionService>();
		private readonly IDataAccessService _dataAccessService = Kernel.Resolve<IDataAccessService>();

		#region Implementation of IStoreSetService
		public void StoreChangeset(IList<StoreSet> toStoreSets, PBasicEntityType key, IEvent @event) {
			var transaction = _transactionFactory.CreateTransaction();
			_transactionService.AddEvent(transaction, @event);
			toStoreSets.Add(new StoreSet {Transaction = transaction, Key = key});
		}

		public void Save(IList<StoreSet> toStoreSets) {
			foreach (var storeSet in toStoreSets.DistinctMerge(MergeStoreSet))
				InternalStore(storeSet);
		}

		private void InternalStore(StoreSet storeSet) {
			var events = _transactionService.GetEvents(storeSet.Transaction).Execute();
			foreach (var @event in events)
				@event.Revision = (ulong) DateTime.Now.Ticks;
			_dataAccessService.Store(storeSet.Key, events);
		}

		private StoreSet MergeStoreSet(StoreSet a, StoreSet b) {
			_transactionService.AddEvents(a.Transaction, _transactionService.GetEvents(b.Transaction));
			return a;
		}
		#endregion
	}
}