using System;
using ClusterConcept.Shared.Infrastructure.Server.Com;
using ClusterConcept.Shared.Model;
using Fijo.Net.Nodelt.Handler;
using JetBrains.Annotations;

namespace ClusterConcept.EntityFramework.Infrastructure.Handlers {
	[UsedImplicitly]
	public class FakePostHandler : SendonlyHandler<PostQuery, NodeCommand> {
		#region Overrides of SendonlyHandler<PostQuery,NodeCommand>
		public override void Handle(PostQuery obj) {
			throw new NotSupportedException();
		}

		public override NodeCommand Command { get { return NodeCommand.Post; } }
		#endregion
	}
}