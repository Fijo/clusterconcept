using System;
using System.Collections.Generic;
using ClusterConcept.Shared.Infrastructure.Server.Com;
using ClusterConcept.Shared.Model;
using Fijo.Infrastructure.DesignPattern.Events;
using Fijo.Net.Nodelt.Handler;
using JetBrains.Annotations;

namespace ClusterConcept.EntityFramework.Infrastructure.Handlers {
	[UsedImplicitly]
	public class FakeGetHandler : Handler<GetQuery, IList<IEvent>, NodeCommand> {
		#region Overrides of Handler<GetQuery,IList<IEvent>,NodeCommand>
		public override IList<IEvent> Handle(GetQuery obj) {
			throw new NotSupportedException();
		}

		public override NodeCommand Command { get { return NodeCommand.Get; } }
		#endregion
	}
}