using System;
using System.ComponentModel;

namespace ClusterConcept.EntityFramework.Exceptions
{
	[Description("No Id had been set yet.")]
	public class NoIdSetException : InvalidOperationException	{
		public NoIdSetException() {

		}
		public NoIdSetException(string message) : base(message) {

		}
		public NoIdSetException(string message, Exception innerException) : base(message, innerException) {

		}
	}
}