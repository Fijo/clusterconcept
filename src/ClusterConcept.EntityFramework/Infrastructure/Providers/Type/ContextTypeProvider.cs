using System.Collections.Generic;
using ClusterConcept.EntityFramework.Model;
using ClusterConcept.EntityFramework.Model.DataContext;
using ClusterConcept.EntityFramework.Model.Internal.TypeMeta;
using ClusterConcept.Shared.Model.Type;
using FijoCore.Infrastructure.LightContrib.Extentions.Type;
using JetBrains.Annotations;

namespace ClusterConcept.EntityFramework.Infrastructure.Providers.Type {
	[UsedImplicitly]
	public class ContextTypeProvider : EntityBaseTypeProvider<PContextType, ContextType, IDataContext> {
		#region Overrides of EntityBaseTypeProvider<PContextType,ContextType,IDataContext>
		protected override IList<TypeMeta> GetTypeMeta(TypeMetas typeMetas) {
			return typeMetas.Contextes;
		}

		protected override void InitBasicEntityType(ContextType contextType, TypeMeta typeMeta, System.Type type) {
			contextType.Create = type.New<IDataContext>;
		}
		#endregion
	}
}