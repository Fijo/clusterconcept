using System.Collections.Generic;
using System.Diagnostics;
using System.Diagnostics.Contracts;
using System.Linq;
using ClusterConcept.EntityFramework.Infrastructure.Service.KeyResolver;
using ClusterConcept.EntityFramework.Model;
using ClusterConcept.EntityFramework.Model.Base;
using ClusterConcept.EntityFramework.Model.Entity;
using ClusterConcept.EntityFramework.Model.Internal.TypeMeta;
using ClusterConcept.Shared.Model.Type;
using Fijo.Infrastructure.DesignPattern.Repository;
using FijoCore.Infrastructure.DependencyInjection.InitKernel;
using FijoCore.Infrastructure.LightContrib.Extentions.Type;

namespace ClusterConcept.EntityFramework.Infrastructure.Providers.Type {
	public abstract class EntityBaseTypeProvider<TPersitType, TBasicEntityType, TBasicEntity> : IEntityBaseTypeProvider<TPersitType, TBasicEntityType> where TPersitType : PBaseType
	                                                                                                                                                   where TBasicEntityType : BasicEntityType, new()
	                                                                                                                                                   where TBasicEntity : IBasicEntity {
		private readonly IDictionary<TPersitType, TBasicEntityType> _content = new Dictionary<TPersitType, TBasicEntityType>();

		public EntityBaseTypeProvider() {
			var keyResolver = Kernel.Resolve<IKeyResolver<TBasicEntity, TPersitType>>();
			var repository = Kernel.Resolve<IRepository<TypeMetas>>();
			_content = GetTypeMeta(repository.Get())
				.ToDictionary(x => keyResolver.Get(x.Type), GetBasicEntityType);
		}

		protected abstract IList<TypeMeta> GetTypeMeta(TypeMetas typeMetas);

		[Pure]
		protected TBasicEntityType GetBasicEntityType(TypeMeta typeMeta) {
			#region PreCondition
			Debug.Assert(typeMeta.Type.ImplementsInteface<TBasicEntity>());
			Debug.Assert(typeMeta.Type.HasEmptyConstructor());
			#endregion
			var type = typeMeta.Type;
			var basicEntityType = new TBasicEntityType
			{
				TypeMeta = typeMeta,
				Entities = new Dictionary<long, IEntity>(),
				Primitives = new Dictionary<long, object>()
			};
			InitBasicEntityType(basicEntityType, typeMeta, type);
			return basicEntityType;
		}

		protected abstract void InitBasicEntityType(TBasicEntityType basicEntityType, TypeMeta typeMeta, System.Type type);

		public TBasicEntityType Get(TPersitType key) {
			return _content[key];
		}
	                                                                                                                                                   }
}