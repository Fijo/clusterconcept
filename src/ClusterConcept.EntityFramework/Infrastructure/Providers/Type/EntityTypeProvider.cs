using System.Collections.Generic;
using ClusterConcept.EntityFramework.Model;
using ClusterConcept.EntityFramework.Model.Entity;
using ClusterConcept.EntityFramework.Model.Internal.TypeMeta;
using ClusterConcept.Shared.Model.Type;
using FijoCore.Infrastructure.LightContrib.Extentions.Type;
using JetBrains.Annotations;

namespace ClusterConcept.EntityFramework.Infrastructure.Providers.Type {
	[UsedImplicitly]
	public class EntityTypeProvider : EntityBaseTypeProvider<PEntityType, EntityType, IEntity> {
		#region Overrides of EntityBaseTypeProvider<PEntityType,EntityType,IEntity>
		protected override IList<TypeMeta> GetTypeMeta(TypeMetas typeMetas) {
			return typeMetas.Entities;
		}

		protected override void InitBasicEntityType(EntityType entityType, TypeMeta typeMeta, System.Type type) {
			entityType.CurrentId = long.MinValue;
			entityType.Create = type.New<IEntity>;
		}
		#endregion
	}
}