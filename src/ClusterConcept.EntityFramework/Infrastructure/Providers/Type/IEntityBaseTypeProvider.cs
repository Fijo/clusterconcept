namespace ClusterConcept.EntityFramework.Infrastructure.Providers.Type {
	public interface IEntityBaseTypeProvider<in TPersitType, out TBasicEntityType> {
		TBasicEntityType Get(TPersitType key);
	}
}