using System.ComponentModel;
using ClusterConcept.EntityFramework.Model;
using JetBrains.Annotations;

namespace ClusterConcept.EntityFramework.Infrastructure.Providers
{
	[UsedImplicitly]
	[Description("simple non clustered id provider")]
	public class SimpleIdProvider : IIdProvider
	{
		public long Get(EntityType entityType)	{
			return entityType.CurrentId++;
		}
	}
}