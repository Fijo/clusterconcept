using ClusterConcept.EntityFramework.Model;

namespace ClusterConcept.EntityFramework.Infrastructure.Providers
{
	public interface IIdProvider	{
		long Get(EntityType key);
	}
}