using System.Collections.Generic;
using System.Linq;
using ClusterConcept.EntityFramework.Infrastructure.Interfaces;
using Fijo.Infrastructure.DesignPattern.Repository.Collection;
using FijoCore.Infrastructure.DependencyInjection.InitKernel;
using Ninject;

namespace ClusterConcept.EntityFramework.Infrastructure.Providers {
	public class PrimitiveHandlingProvider : CollectionRepositoryBase<KeyValuePair<System.Type, IPrimitiveHandling>> {
		#region Overrides of CollectionRepositoryBase<KeyValuePair<Type,IPrimitiveHandling>>
		protected override IEnumerable<KeyValuePair<System.Type, IPrimitiveHandling>> GetAll() {
			return Kernel.Inject.GetAll<IPrimitiveHandling>()
				.ToDictionary(x => x.Type, x => x);
		}
		#endregion
	}
}