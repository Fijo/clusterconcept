using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using ClusterConcept.EntityFramework.Attributes;
using ClusterConcept.EntityFramework.Infrastructure.Interfaces;
using ClusterConcept.EntityFramework.Model.Base;
using ClusterConcept.EntityFramework.Model.DataContext;
using ClusterConcept.EntityFramework.Model.Entity;
using ClusterConcept.EntityFramework.Model.Internal.TypeMeta;
using Fijo.Infrastructure.DesignPattern.Repository;
using Fijo.Infrastructure.DesignPattern.Repository.Collection;
using Fijo.Infrastructure.Documentation.Enums;
using FijoCore.Infrastructure.DependencyInjection.InitKernel;
using FijoCore.Infrastructure.LightContrib.Extentions.Generic;
using FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic;
using FijoCore.Infrastructure.LightContrib.Extentions.Type;
using FijoCore.Infrastructure.LightContrib.Module.Assembly;

namespace ClusterConcept.EntityFramework.Infrastructure.Providers
{
	public class TypeMetasRepository : RepositoryBase<TypeMetas>
	{
		private readonly TypeMetas _metas;

		public TypeMetasRepository() {
			var types = GetTypes();
			var primitives = GetPrimitives();
			_metas = new TypeMetas
			         	{
			         		Contextes = CreateMetas(types, IsContext, primitives),
			         		Entities = CreateMetas(types, IsEntity, primitives),
			         		Primitives = CreateMetas(types, x => IsPrimitive(x, primitives), primitives)
			         	};
		}

		private static ICollection<System.Type> GetTypes() {
			var assembliesProvider = Kernel.Resolve<AssembliesProvider>();
			return assembliesProvider.GetAssemblies()
				.SelectMany(x => x.GetTypes())
				.Where(x => !x.IsIgnoredFor(IgnoreFor.EntityFramework))
				.Execute();
		}

		private static ICollection<System.Type> GetPrimitives() {
			return Kernel.Resolve<ICollectionRepository<KeyValuePair<System.Type, IPrimitiveHandling>>>().Get()
				.Select(x => x.Key).Execute();
		}

		private IList<TypeMeta> CreateMetas(ICollection<System.Type> types, Func<System.Type, bool> predicate, IEnumerable<System.Type> primitives) {
			return types.Where(predicate).Select(x => CreateMeta(x, primitives)).ToList();
		}

		private TypeMeta CreateMeta(System.Type type, IEnumerable<System.Type> primitives) {
			var properties = GetProperties(type).Where(HasUIdAttribute).Execute();
			Debug.Assert(properties.None(x => x.Name == "Id" && x.PropertyType == typeof (long)), "The Id property is no wanted here");
			Debug.Assert(properties.IsUnique(GetKey));
			return new TypeMeta
			{
				Type = type,
				Entities = FilterProperies(properties, IsEntity).ToList(),
				Primitives = FilterProperies(properties, x => IsPrimitive(x, primitives)).ToList(),
				Contextes = FilterProperies(properties, IsContext).ToList()
			};
		}

		private bool HasUIdAttribute(PropertyInfo propertyInfo) {
			return GetUIdAttribute(propertyInfo) != null;
		}

		private string GetKey(PropertyInfo propertyInfo) {
			var attr = GetUIdAttribute(propertyInfo);
			Debug.Assert(attr != null, "The property must have a UIdAttribute when you call this function. Ensure that before you call it.");
			return attr.UId;
		}

		private UIdAttribute GetUIdAttribute(PropertyInfo propertyInfo) {
			return (UIdAttribute) propertyInfo.GetCustomAttributes(typeof (UIdAttribute), true).SingleOrDefault();
		}

		private IEnumerable<PropertyInfo> FilterProperies(IEnumerable<PropertyInfo> properties, Func<System.Type, bool> predicate) {
			return properties.Where(x => predicate(x.PropertyType));
		}

		private IEnumerable<PropertyInfo> GetProperties(System.Type type)	{
			return type.GetProperties(BindingFlags.Instance | BindingFlags.Public | BindingFlags.SetField | BindingFlags.GetField);
		}

		private bool IsBasicEntity<T>(System.Type type) where T : IBasicEntity
		{
			var implements = type.ImplementsInteface<T>();
			if (implements) Debug.Assert(type.HasEmptyConstructor());
			return implements;
		}

		private bool IsContext(System.Type type)
		{
			return IsBasicEntity<IDataContext>(type);
		}

		private bool IsEntity(System.Type type)
		{
			return IsBasicEntity<IEntity>(type);
		}

		private bool IsPrimitive(System.Type type, IEnumerable<System.Type> primitives)
		{
			return type.IsIn(primitives);
		}

		public override TypeMetas Get()
		{
			return _metas;
		}
	}
}