//BS:Die �rzte - Deine Schuld (Cover)

using System;
using System.Diagnostics;
using ClusterConcept.EntityFramework.Infrastructure.Providers.Type;
using ClusterConcept.EntityFramework.Infrastructure.Service.KeyResolver;
using ClusterConcept.EntityFramework.Infrastructure.Service.ObjectStore;
using ClusterConcept.EntityFramework.Infrastructure.Service.Transaction;
using ClusterConcept.EntityFramework.Model;
using ClusterConcept.EntityFramework.Model.DataContext;
using ClusterConcept.Shared.Model.Type;
using FijoCore.Infrastructure.DependencyInjection.InitKernel;
using FijoCore.Infrastructure.LightContrib.Extentions.Type;
using JetBrains.Annotations;

namespace ClusterConcept.EntityFramework.Infrastructure.Service	{
	[UsedImplicitly]
	public class DataContextService : IDataContextService {
		private readonly IKeyResolver<IDataContext, PContextType> _keyResolver = Kernel.Resolve<IKeyResolver<IDataContext, PContextType>>();
		private readonly IObjectStoreService<IDataContext> _objectStoreService = Kernel.Resolve<IObjectStoreService<IDataContext>>();
		private readonly ITransactionFactory _transactionFactory = Kernel.Resolve<ITransactionFactory>();
		private readonly IEntityBaseTypeProvider<PContextType, ContextType> _entityBaseTypeProvider = Kernel.Resolve<IEntityBaseTypeProvider<PContextType, ContextType>>();

		public virtual void Save(IDataContext context) {
			var transaction = _transactionFactory.CreateTransaction();
			_objectStoreService.Save(context, transaction);
			// TODO handle transaction
		}

		public virtual IDataContext Get(Type type)	{
			#region PreCondition
			Debug.Assert(type.ImplementsInteface<IDataContext>());
			#endregion
			var key = _keyResolver.Get(type);
			var contextType = _entityBaseTypeProvider.Get(key);
			return contextType.Context ?? (contextType.Context = InternalGet(key, contextType));
		}
		protected virtual IDataContext InternalGet(PContextType contextType, ContextType content) {
			var context = content.Create();
			_objectStoreService.Load(context, contextType);
			return context;
		}
	}
}