using System;
using ClusterConcept.EntityFramework.Model.DataContext;

namespace ClusterConcept.EntityFramework.Infrastructure.Service
{
	public interface IDataContextService	{
		void Save(IDataContext context);
		IDataContext Get(Type type);
	}
}