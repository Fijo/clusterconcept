//BS:Green Day - 10 When I Come Around

using System;
using System.Diagnostics;
using ClusterConcept.EntityFramework.Infrastructure.Factory;
using ClusterConcept.EntityFramework.Infrastructure.Providers.Type;
using ClusterConcept.EntityFramework.Infrastructure.Service.KeyResolver;
using ClusterConcept.EntityFramework.Infrastructure.Service.ObjectStore;
using ClusterConcept.EntityFramework.Infrastructure.Service.Transaction;
using ClusterConcept.EntityFramework.Model;
using ClusterConcept.EntityFramework.Model.Entity;
using ClusterConcept.Shared.Model.Type;
using FijoCore.Infrastructure.DependencyInjection.InitKernel;
using FijoCore.Infrastructure.LightContrib.Extentions.IDictionary.Generic;
using FijoCore.Infrastructure.LightContrib.Extentions.Type;
using JetBrains.Annotations;

namespace ClusterConcept.EntityFramework.Infrastructure.Service {
	[UsedImplicitly]
	public class EntityService : IEntityService {
		private readonly IKeyResolver<IEntity, PEntityType> _keyResolver = Kernel.Resolve<IKeyResolver<IEntity, PEntityType>>();		
		private readonly IObjectStoreService<IEntity> _objectStoreService = Kernel.Resolve<IObjectStoreService<IEntity>>();
		private readonly ITransactionFactory _transactionFactory = Kernel.Resolve<ITransactionFactory>();
		private readonly IEntityBaseTypeProvider<PEntityType, EntityType> _entityBaseTypeProvider = Kernel.Resolve<IEntityBaseTypeProvider<PEntityType, EntityType>>();
		private readonly IEntityFactory _entityFactory = Kernel.Resolve<IEntityFactory>();

		#region Implementation of IEntityService
		public void Save(IEntity entity) {
			var transaction = _transactionFactory.CreateTransaction();
			_objectStoreService.Save(entity, transaction);
			// TODO handle transaction
		}

		public IEntity Get(Type type, long id) {
			#region PreCondition
			Debug.Assert(type.ImplementsInteface<IEntity>());
			#endregion
			var pEntityType = _keyResolver.Get(type);
			return InternalGet(id, pEntityType);
		}
		
		public virtual IEntity InternalGet(long id, PEntityType pEntityType) {
			var entityType = _entityBaseTypeProvider.Get(pEntityType);
			return InternalGet(id, pEntityType, entityType);
		}

		protected virtual IEntity InternalGet(long id, PEntityType pEntityType, EntityType entityType) {
			return entityType.Entities.GetOrCreate(id, () => InternalGet(pEntityType, entityType, id));
		}

		protected virtual IEntity InternalGet(PEntityType pEntityType, EntityType entityType, long id) {
			var entity = _entityFactory.Create(entityType, id);
			Load(pEntityType, entity);
			return entity;
		}

		private void Load(PEntityType pEntityType, IEntity entity) {
			_objectStoreService.Load(entity, pEntityType);
		}

		public IEntity Create(Type type) {
			var pEntityType = _keyResolver.Get(type);
			return InternalCreate(pEntityType);
		}

		public IEntity InternalCreate(PEntityType pEntityType) {
			var entityType = _entityBaseTypeProvider.Get(pEntityType);
			var entity = _entityFactory.Create(entityType);
			Load(pEntityType, entity);
			Debug.Assert(!entityType.Entities.ContainsKey(entity.Id),
			             string.Format("Created a entity with a key that already exists or that has not been delete full yet (a entity with the id {0} does already exist in the entity cache)", entity.Id));
			entityType.Entities.Add(entity.Id, entity);
			return entity;
		}
		#endregion
	}
}