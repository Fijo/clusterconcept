//BS:Frei Wild - Der aufrechte Weg

using System.Collections.Generic;
using System.Linq;
using ClusterConcept.EntityFramework.Enums;
using ClusterConcept.EntityFramework.Infrastructure.Providers.Type;
using ClusterConcept.EntityFramework.Infrastructure.Service.ContentStore;
using ClusterConcept.EntityFramework.Infrastructure.Service.DataAccess;
using ClusterConcept.EntityFramework.Infrastructure.Service.KeyResolver;
using ClusterConcept.EntityFramework.Infrastructure.Service.Transaction;
using ClusterConcept.EntityFramework.Model;
using ClusterConcept.EntityFramework.Model.Base;
using ClusterConcept.Shared.Model.Type;
using FijoCore.Infrastructure.DependencyInjection.InitKernel;
using FijoCore.Infrastructure.LightContrib.Extentions.Generic;
using FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic;
using Ninject;

namespace ClusterConcept.EntityFramework.Infrastructure.Service.ObjectStore {
	public abstract class ObjectStoreService<TPersistType, TBasicEntityType, TBasicEntity> : IObjectStoreService<TBasicEntity> where TPersistType : PBaseType where TBasicEntityType : BasicEntityType, new() where TBasicEntity : IBasicEntity {
		private readonly IInternalDataAccessService<TPersistType> _internalDataAccessService = Kernel.Resolve<IInternalDataAccessService<TPersistType>>();
		private readonly IEntityBaseTypeProvider<TPersistType, TBasicEntityType> _contextTypeProvider = Kernel.Resolve<IEntityBaseTypeProvider<TPersistType, TBasicEntityType>>();
		private readonly ICollection<IContentStoreService> _contentStoreServices;
		private readonly IKeyResolver<TBasicEntity, TPersistType> _keyResolver = Kernel.Resolve<IKeyResolver<TBasicEntity, TPersistType>>();
		private readonly ITransactionService _transactionService = Kernel.Resolve<ITransactionService>();

		protected ObjectStoreService() {
			var handleContentFilter = GetHandleContentFilter().Execute();
			_contentStoreServices = Kernel.Inject.GetAll<IContentStoreService>()
				.Where(x => x.DataType.IsIn(handleContentFilter))
				.Execute();
		}

		protected abstract IEnumerable<DataType> GetHandleContentFilter();

		public virtual void Save(TBasicEntity basicEntity, Transaction.Transaction toTransaction) {
			var type = basicEntity.GetType();
			var key = _keyResolver.Get(type);
			InternalSave(basicEntity, key, toTransaction);
		}

		protected virtual void InternalSave(IBasicEntity context, TPersistType key, Transaction.Transaction toTransaction)	{
			var content = _internalDataAccessService.Get(key);
			var typeMeta = _contextTypeProvider.Get(key);
			foreach (var contentSaveService in _contentStoreServices.SelectMany(x => x.Save(context, content, typeMeta.TypeMeta, key, toTransaction)))
				_transactionService.AddEvent(toTransaction, contentSaveService);
		}

		public virtual void Load(TBasicEntity basicEntity, PBaseType contextType) {
			var type = basicEntity.GetType();
			var key = _keyResolver.Get(type);
			InternalLoad(basicEntity, key);
		}

		private void InternalLoad(IBasicEntity context, TPersistType key)
		{
			var content = _internalDataAccessService.Get(key);
			var typeMeta = _contextTypeProvider.Get(key);
			foreach (var contentSaveService in _contentStoreServices)
				contentSaveService.Load(context, content, typeMeta.TypeMeta, key);
		}
	}
}