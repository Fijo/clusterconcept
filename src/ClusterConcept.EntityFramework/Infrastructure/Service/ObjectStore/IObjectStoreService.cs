using ClusterConcept.EntityFramework.Model.Base;
using ClusterConcept.Shared.Model.Type;

namespace ClusterConcept.EntityFramework.Infrastructure.Service.ObjectStore {
	public interface IObjectStoreService<in TBasicEntity> where TBasicEntity : IBasicEntity {
		void Save(TBasicEntity basicEntity, Transaction.Transaction toTransaction);
		void Load(TBasicEntity basicEntity, PBaseType contextType);
	}
}