using System.Collections.Generic;
using ClusterConcept.EntityFramework.Enums;
using ClusterConcept.EntityFramework.Model;
using ClusterConcept.EntityFramework.Model.Entity;
using ClusterConcept.Shared.Model.Type;
using JetBrains.Annotations;

namespace ClusterConcept.EntityFramework.Infrastructure.Service.ObjectStore {
	[UsedImplicitly]
	public class EntityObjectStoreService : ObjectStoreService<PEntityType, EntityType, IEntity> {
		#region Overrides of ObjectStoreService<PContextType,ContextType,IDataContext>
		protected override IEnumerable<DataType> GetHandleContentFilter() {
			return new List<DataType>
			{
				DataType.Entity,
				DataType.Primitive
			};
		}
		#endregion
	}
}