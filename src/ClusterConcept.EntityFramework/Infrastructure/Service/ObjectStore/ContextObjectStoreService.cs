using System.Collections.Generic;
using ClusterConcept.EntityFramework.Enums;
using ClusterConcept.EntityFramework.Model;
using ClusterConcept.EntityFramework.Model.DataContext;
using ClusterConcept.Shared.Model.Type;
using JetBrains.Annotations;

namespace ClusterConcept.EntityFramework.Infrastructure.Service.ObjectStore {
	[UsedImplicitly]
	public class ContextObjectStoreService : ObjectStoreService<PContextType, ContextType, IDataContext> {
		#region Overrides of ObjectStoreService<PContextType,ContextType,IDataContext>
		protected override IEnumerable<DataType> GetHandleContentFilter() {
			return new List<DataType>	{
				DataType.DataContext,
				DataType.Entity
			};
		}
		#endregion
	}
}