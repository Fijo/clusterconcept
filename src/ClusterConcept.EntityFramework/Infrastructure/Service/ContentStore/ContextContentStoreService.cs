//BS:Green Day - 16 21 Guns

using System.Collections.Generic;
using ClusterConcept.EntityFramework.Enums;
using ClusterConcept.EntityFramework.Infrastructure.Service.ObjectStore;
using ClusterConcept.EntityFramework.Model.Base;
using ClusterConcept.EntityFramework.Model.DataContext;
using ClusterConcept.EntityFramework.Model.Internal;
using ClusterConcept.EntityFramework.Model.Internal.Events;
using ClusterConcept.EntityFramework.Model.Internal.TypeMeta;
using ClusterConcept.Shared.Model.Type;
using Fijo.Infrastructure.DesignPattern.Lazy.Interface;
using FijoCore.Infrastructure.LightContrib.Module.Lazy;
using JetBrains.Annotations;

namespace ClusterConcept.EntityFramework.Infrastructure.Service.ContentStore {
	[UsedImplicitly]
	public class ContextContentStoreService : IContentStoreService	{
		private readonly ILazy<IObjectStoreService<IDataContext>> _objectStoreService = new LazyInject<IObjectStoreService<IDataContext>>();
		private readonly ILazy<IDataContextService> _dataContextService = new LazyInject<IDataContextService>();

		#region Implementation of IContentStoreService<IDataContext>
		public DataType DataType { get { return DataType.DataContext; } }

		public IEnumerable<IEvent> Save<TPersistType>(IBasicEntity context, DataBundle content, TypeMeta typeMeta, TPersistType persistType, Transaction.Transaction toTransaction) where TPersistType : PBaseType {
			var objectStoreService = _objectStoreService.Get();
			foreach (var cont in typeMeta.Contextes)
				objectStoreService.Save((IDataContext) cont.GetValue(context, null), toTransaction);
			yield break;
		}

		public void Load<TPersistType>(IBasicEntity context, DataBundle content, TypeMeta typeMeta, TPersistType persistType) where TPersistType : PBaseType {
			var dataContextService = _dataContextService.Get();
			foreach (var cont in typeMeta.Contextes)
				cont.SetValue(context, dataContextService.Get(cont.PropertyType), null);
		}
		#endregion
	}
}