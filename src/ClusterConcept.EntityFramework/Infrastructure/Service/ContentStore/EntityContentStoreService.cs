//BS:Frei Wild - Harte Zeiten

using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using ClusterConcept.EntityFramework.Enums;
using ClusterConcept.EntityFramework.Infrastructure.Service.Creation;
using ClusterConcept.EntityFramework.Infrastructure.Service.Event;
using ClusterConcept.EntityFramework.Infrastructure.Service.KeyResolver;
using ClusterConcept.EntityFramework.Infrastructure.Service.ObjectStore;
using ClusterConcept.EntityFramework.Model.Base;
using ClusterConcept.EntityFramework.Model.Entity;
using ClusterConcept.EntityFramework.Model.Internal;
using ClusterConcept.EntityFramework.Model.Internal.Events;
using ClusterConcept.EntityFramework.Model.Internal.TypeMeta;
using ClusterConcept.Shared.Model.Type;
using Fijo.Infrastructure.DesignPattern.Lazy.Interface;
using FijoCore.Infrastructure.DependencyInjection.InitKernel;
using FijoCore.Infrastructure.LightContrib.Extentions.IDictionary.Generic;
using FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic;
using FijoCore.Infrastructure.LightContrib.Factories.Interface;
using FijoCore.Infrastructure.LightContrib.Module.Lazy;
using JetBrains.Annotations;

namespace ClusterConcept.EntityFramework.Infrastructure.Service.ContentStore {
	public interface IDataBundleService {
		ulong GetBeginRevision(DataBundle dataBundle);
	}

	public class DataBundleService : IDataBundleService {
		public ulong GetBeginRevision(DataBundle dataBundle) {
			// ToDo may speedup this
			return dataBundle.Chnageset.Events.Min(x => x.Revision);
		}
	}

	[UsedImplicitly]
	public class EntityContentStoreService : IContentStoreService {
		private readonly IKeyResolver<IEntity, PPropertyType> _propertyKeyResolver = Kernel.Resolve<IKeyResolver<IEntity, PPropertyType>>();
		private readonly IKeyResolver<IEntity, PEntityType> _entityKeyResolver = Kernel.Resolve<IKeyResolver<IEntity, PEntityType>>();
		private readonly IPairFactory _pairFactory = Kernel.Resolve<IPairFactory>();
		private readonly ILazy<IObjectStoreService<IEntity>> _objectStoreService = new LazyInject<IObjectStoreService<IEntity>>();
		private readonly ILazy<IEntityService> _entityService = new LazyInject<IEntityService>();
		private readonly ICreationService<PEntityType, IEntity> _creationService = Kernel.Resolve<ICreationService<PEntityType, IEntity>>();
		private readonly IChangeEventService _changeEventService = Kernel.Resolve<IChangeEventService>();
		private readonly IDataBundleService _dataBundleService = Kernel.Resolve<IDataBundleService>();

		#region Implementation of ISaveService<IEntity>
		public DataType DataType { get { return DataType.Entity; } }

		public IEnumerable<IEvent> Save<TPersistType>(IBasicEntity context, DataBundle content, TypeMeta typeMeta, TPersistType persistType, Transaction.Transaction toTransaction) where TPersistType : PBaseType {
			var subEntityIds = content.Data.SubEntityIds;
			ICollection<KeyValuePair<PPropertyType, long>> changedSubEntities;
			lock (subEntityIds) changedSubEntities = GetChangedSubEntities(context, subEntityIds, typeMeta, toTransaction).Execute();
			if (changedSubEntities.Any())
				yield return CreateStoreEntityEvent(persistType, changedSubEntities);
		}

		private IEvent CreateStoreEntityEvent<TPersistType>(TPersistType persistType, IEnumerable<KeyValuePair<PPropertyType, long>> changedSubEntities) where TPersistType : PBaseType {
			return new ChangeEntityEvent<TPersistType>
			{
				Changeset = changedSubEntities.ToDictionary(x => x.Key, x => x.Value),
				PersistType = persistType
			};
		}

		private IEnumerable<KeyValuePair<PPropertyType, long>> GetChangedSubEntities(IBasicEntity context, IDictionary<PPropertyType, long> subEntityIds, TypeMeta typeMeta, Transaction.Transaction toTransaction) {
			var objectStoreService = _objectStoreService.Get();
			foreach (var propertyInfo in typeMeta.Entities) {
				var key = GetKey(propertyInfo);
				var entity = GetValue(context, propertyInfo);
				if(entity == null) {
					HandleNullSubEntities(key);
					continue;
				}
				objectStoreService.Save(entity, toTransaction);
				var id = entity.Id;
				long value;
				if (subEntityIds.TryGetValue(key, out value) && value == id) continue;
				subEntityIds[key] = id;
				yield return GetChangePair(key, id);
			}
		}

		private void HandleNullSubEntities(PPropertyType key) {
			Trace.WriteLine(string.Format("skip save of subentity, that is null at property {0}", key.UId));
			
			// ToDO may create null entities
		}

		private PPropertyType GetKey(PropertyInfo propertyInfo) {
			return _propertyKeyResolver.Get(propertyInfo.PropertyType);
		}

		private PEntityType GetTypeKey(PropertyInfo propertyInfo) {
			return _entityKeyResolver.Get(propertyInfo.PropertyType);
		}

		private IEntity GetValue(IBasicEntity context, PropertyInfo propertyInfo) {
			return ((IEntity) propertyInfo.GetValue(context, null));
		}

		private KeyValuePair<PPropertyType, long> GetChangePair(PPropertyType key, long id) {
			Trace.WriteLine(string.Format("push change entity at property {0} is linked to {1}", key, id));
			return _pairFactory.KeyValuePair(key, id);
		}

		public void Load<TPersistType>(IBasicEntity context, DataBundle content, TypeMeta typeMeta, TPersistType persistType) where TPersistType : PBaseType {
			var subEntities = _changeEventService.GetEntityChanges(content, persistType);
			var isNewFetched = IsFullFetch(content);
			var propertyInfos = typeMeta.Entities;
			lock(propertyInfos)
				foreach (var propertyInfo in propertyInfos)
					LoadEntry(context, subEntities, propertyInfo, isNewFetched);
		}

		private bool IsFullFetch(DataBundle content) {
			return _dataBundleService.GetBeginRevision(content) == 0;
		}

		private void LoadEntry(IBasicEntity context, IDictionary<PPropertyType, long> subEntityIds, PropertyInfo propertyInfo, bool isNewFetched) {
			var propertyKey = GetKey(propertyInfo);
			var key = GetTypeKey(propertyInfo);
			subEntityIds.TryGet(propertyKey, value => propertyInfo.SetValue(context, _entityService.Get().InternalGet(value, key), null), () => {
				if(isNewFetched && _creationService.AutoCreate(propertyInfo, key))
					SetValue(context, propertyInfo, _creationService.Create(propertyInfo, key));
			});
		}

		private void SetValue(IBasicEntity context, PropertyInfo propertyInfo, IEntity value) {
			propertyInfo.SetValue(context, value, null);
		}
		#endregion
	}
}