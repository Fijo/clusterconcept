using System.Collections.Generic;
using ClusterConcept.EntityFramework.Enums;
using ClusterConcept.EntityFramework.Model.Base;
using ClusterConcept.EntityFramework.Model.Internal;
using ClusterConcept.EntityFramework.Model.Internal.Events;
using ClusterConcept.EntityFramework.Model.Internal.TypeMeta;
using ClusterConcept.Shared.Model.Type;

namespace ClusterConcept.EntityFramework.Infrastructure.Service.ContentStore {
	public interface IContentStoreService {
		DataType DataType { get; }

		IEnumerable<IEvent> Save<TPersistType>(IBasicEntity context, DataBundle content, TypeMeta typeMeta, TPersistType persistType, Transaction.Transaction toTransaction)
			where TPersistType : PBaseType;

		void Load<TPersistType>(IBasicEntity context, DataBundle content, TypeMeta typeMeta, TPersistType persistType)
			where TPersistType : PBaseType;
	}
}