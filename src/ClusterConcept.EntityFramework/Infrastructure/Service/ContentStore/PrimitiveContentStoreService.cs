//BS:Frei Wild - Freundschaft

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using ClusterConcept.EntityFramework.Enums;
using ClusterConcept.EntityFramework.Infrastructure.Interfaces;
using ClusterConcept.EntityFramework.Infrastructure.Service.Event;
using ClusterConcept.EntityFramework.Infrastructure.Service.KeyResolver;
using ClusterConcept.EntityFramework.Model.Base;
using ClusterConcept.EntityFramework.Model.Entity;
using ClusterConcept.EntityFramework.Model.Internal;
using ClusterConcept.EntityFramework.Model.Internal.Events;
using ClusterConcept.EntityFramework.Model.Internal.TypeMeta;
using ClusterConcept.Shared.Model.Type;
using Fijo.Infrastructure.DesignPattern.Repository.Collection;
using FijoCore.Infrastructure.DependencyInjection.InitKernel;
using FijoCore.Infrastructure.LightContrib.Extentions.IDictionary.Generic;
using FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic;
using FijoCore.Infrastructure.LightContrib.Factories.Interface;
using FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic.KeyValuePair;
using JetBrains.Annotations;

namespace ClusterConcept.EntityFramework.Infrastructure.Service.ContentStore {
	[UsedImplicitly]
	public class PrimitiveContentStoreService : IContentStoreService {
		private readonly IKeyResolver<IEntity, PPropertyType> _propertyKeyResolver = Kernel.Resolve<IKeyResolver<IEntity, PPropertyType>>();
		private readonly IPairFactory _pairFactory = Kernel.Resolve<IPairFactory>();
		// ToDo use another key inststead of Type
		private readonly IDictionary<Type, IPrimitiveHandling> _primitiveHandlings;
		private readonly IChangeEventService _changeEventService = Kernel.Resolve<IChangeEventService>();

		public PrimitiveContentStoreService() {
			_primitiveHandlings = Kernel.Resolve<ICollectionRepository<KeyValuePair<Type, IPrimitiveHandling>>>().Get()
				.ToDictionary();
		}

		#region Implementation of IContentStoreService<object>
		public DataType DataType { get { return DataType.Primitive; } }

		public IEnumerable<IEvent> Save<TPersistType>(IBasicEntity context, DataBundle content, TypeMeta typeMeta, TPersistType persistType, Transaction.Transaction toTransaction) where TPersistType : PBaseType {
			var subPrimitives = content.Data.SubPrimitives;

			ICollection<KeyValuePair<PPropertyType, IList<byte>>> changedSubPrimitives;
			lock (subPrimitives) changedSubPrimitives = GetChangedPrimitives(context, subPrimitives, typeMeta).Execute();
			if (changedSubPrimitives.Any())
				yield return CreateStorePrimitiveEvent(persistType, changedSubPrimitives);
		}

		private IEnumerable<byte> Serialize(object value, PropertyInfo cont) {
			#region PreCondition
			Debug.Assert(_primitiveHandlings.ContainsKey(cont.PropertyType));
			#endregion
			return _primitiveHandlings[cont.PropertyType].Serialize(value);
		}

		private IEvent CreateStorePrimitiveEvent<TPersistType>(TPersistType persistType, ICollection<KeyValuePair<PPropertyType, IList<byte>>> changedSubEntities) where TPersistType : PBaseType {
			return new ChangePrimitiveEvent<TPersistType>
			{
				Changeset = changedSubEntities.ToDictionary(),
				PersistType = persistType
			};
		}

		// ToDo may do not always serialize all candidates (find a way to do this)
		private IEnumerable<KeyValuePair<PPropertyType, IList<byte>>> GetChangedPrimitives(IBasicEntity context, IDictionary<PPropertyType, IList<byte>> subPrimitives, TypeMeta typeMeta) {
			foreach (var propertyInfo in typeMeta.Primitives) {
				var type = propertyInfo.PropertyType;
				var key = _propertyKeyResolver.Get(type);
				var bytes = Serialize(propertyInfo.GetValue(context, null), propertyInfo).ToList();
				IList<byte> value;
				if (subPrimitives.TryGetValue(key, out value) && value.SequenceEqual(bytes)) continue;
				subPrimitives[key] = bytes;
				yield return _pairFactory.KeyValuePair<PPropertyType, IList<byte>>(key, bytes);
			}
		}

		public void Load<TPersistType>(IBasicEntity context, DataBundle content, TypeMeta typeMeta, TPersistType persistType) where TPersistType : PBaseType {
			var subPrimitives = _changeEventService.GetPrimitiveChanges(content, persistType);
			lock(subPrimitives)
				foreach (var propertyInfo in typeMeta.Primitives)
					LoadEntry(context, subPrimitives, propertyInfo);
		}

		private void LoadEntry(IBasicEntity context, IDictionary<PPropertyType, IList<byte>> subPrimitives, PropertyInfo propertyInfo) {
			var type = propertyInfo.PropertyType;
			var key = _propertyKeyResolver.Get(type);
			subPrimitives.TryGet(key, value => {
				var obj = Deserialize(value, propertyInfo);
				propertyInfo.SetValue(context, new[] {obj}, null);
			});
		}

		private object Deserialize(IEnumerable<byte> bytes, PropertyInfo cont)  {
			#region PreCondition
			Debug.Assert(_primitiveHandlings.ContainsKey(cont.PropertyType));
			#endregion
			return _primitiveHandlings[cont.PropertyType].Deserialize(bytes);
		}
		#endregion
	}
}