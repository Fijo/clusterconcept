using ClusterConcept.EntityFramework.Model.Internal;
using ClusterConcept.Shared.Model.Type;

namespace ClusterConcept.EntityFramework.Infrastructure.Service.DataAccess {
	public interface IInternalDataAccessService<in TPersistType> where TPersistType : PBaseType {
		DataBundle Get(TPersistType contextType);
	}
}