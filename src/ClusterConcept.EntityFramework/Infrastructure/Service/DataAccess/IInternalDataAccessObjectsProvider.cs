using ClusterConcept.EntityFramework.Model.Internal;
using ClusterConcept.Shared.Model.Type;
using JetBrains.Annotations;

namespace ClusterConcept.EntityFramework.Infrastructure.Service.DataAccess {
	public interface IInternalDataAccessObjectsProvider<in TPersistType> where TPersistType : PBaseType {
		[CanBeNull]
		DataBundle Get([NotNull] TPersistType contextType);
		void Set([NotNull] TPersistType contextType, [NotNull] DataBundle dataBundle);
	}
}