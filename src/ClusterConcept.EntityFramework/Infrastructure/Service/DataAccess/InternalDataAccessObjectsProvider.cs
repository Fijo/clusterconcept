using System.Collections.Generic;
using ClusterConcept.EntityFramework.Model.Internal;
using ClusterConcept.Shared.Model.Type;
using FijoCore.Infrastructure.LightContrib.Extentions.IDictionary.Generic;
using JetBrains.Annotations;

namespace ClusterConcept.EntityFramework.Infrastructure.Service.DataAccess {
	[UsedImplicitly]
	public class InternalDataAccessObjectsProvider<TPersistType> : IInternalDataAccessObjectsProvider<TPersistType> where TPersistType : PBaseType {
		private readonly IDictionary<TPersistType, DataBundle> _content = new Dictionary<TPersistType, DataBundle>();
		
		#region Implementation of IInternalDataAccessObjectsProvider<in TPersistType>
		public DataBundle Get(TPersistType contextType) {
			return _content.TryGet(contextType);
		}

		public void Set(TPersistType contextType, DataBundle dataBundle) {
			_content[contextType] = dataBundle;
		}
		#endregion
	}
}