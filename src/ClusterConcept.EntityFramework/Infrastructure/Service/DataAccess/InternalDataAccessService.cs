//BS:Frei Wild - Freiheit

using System.Collections.Generic;
using ClusterConcept.EntityFramework.Infrastructure.Service.Transaction;
using ClusterConcept.EntityFramework.Model;
using ClusterConcept.EntityFramework.Model.Base;
using ClusterConcept.EntityFramework.Model.Internal;
using ClusterConcept.Shared.Model;
using ClusterConcept.Shared.Model.Type;
using FijoCore.Infrastructure.DependencyInjection.InitKernel;
using JetBrains.Annotations;

namespace ClusterConcept.EntityFramework.Infrastructure.Service.DataAccess {
	[UsedImplicitly]
	public class InternalDataAccessService<TPersistType, TBasicEntityType, TBasicEntity> : IInternalDataAccessService<TPersistType> where TPersistType : PBaseType where TBasicEntityType : BasicEntityType, new() where TBasicEntity : IBasicEntity {
		private readonly ITransactionFactory _transactionFactory = Kernel.Resolve<ITransactionFactory>();
		private readonly IInternalDataAccessObjectsProvider<TPersistType> _internalDataAccessObjectsProvider = Kernel.Resolve<IInternalDataAccessObjectsProvider<TPersistType>>();

		#region Implementation of IInternalDataAccessService<in TPersistType>
		public DataBundle Get(TPersistType contextType) {
			var dataBundle = _internalDataAccessObjectsProvider.Get(contextType);
			if(dataBundle != null) return dataBundle;
			dataBundle = Create();
			_internalDataAccessObjectsProvider.Set(contextType, dataBundle);
			return dataBundle;
		}

		private DataBundle Create() {
			var dataBundle = new DataBundle	{
				Data = new PBasicEntity	{
					SubEntityIds = new Dictionary<PPropertyType, long>(),
					SubPrimitives = new Dictionary<PPropertyType, IList<byte>>(),
				},
				Chnageset = _transactionFactory.CreateTransaction()
			};

			return dataBundle;
		}
		#endregion
	}
}