using System.Reflection;
using ClusterConcept.Shared.Model.Type;

namespace ClusterConcept.EntityFramework.Infrastructure.Service.Creation {
	public interface ICreationService<TPersistType, T> where TPersistType : PBaseType {
		bool AutoCreate(PropertyInfo propertyInfo, TPersistType pType);
		T Create(PropertyInfo propertyInfo, TPersistType pType);
	}
}