//BS:Green Day - 14 Reject

using System.Reflection;
using ClusterConcept.EntityFramework.Model.Entity;
using ClusterConcept.Shared.Model.Type;
using Fijo.Infrastructure.DesignPattern.Lazy.Interface;
using FijoCore.Infrastructure.LightContrib.Module.Lazy;
using JetBrains.Annotations;

namespace ClusterConcept.EntityFramework.Infrastructure.Service.Creation {
	[UsedImplicitly]
	public class EntityCreationService : ICreationService<PEntityType, IEntity> {
		private readonly ILazy<IEntityService> _entityService = new LazyInject<IEntityService>();

		#region Implementation of ICreationService<PEntityType,IEntity>
		public bool AutoCreate(PropertyInfo propertyInfo, PEntityType pType) {
			return true;
		}

		public IEntity Create(PropertyInfo propertyInfo, PEntityType pType) {
			return _entityService.Get().InternalCreate(pType);
		}
		#endregion
	}
}