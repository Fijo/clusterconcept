using System;
using ClusterConcept.EntityFramework.Model.Entity;
using ClusterConcept.Shared.Model.Type;

namespace ClusterConcept.EntityFramework.Infrastructure.Service {
	public interface IEntityService	{
		void Save(IEntity entity);
		IEntity Get(Type type, long id);
		IEntity InternalGet(long id, PEntityType pEntityType);
		IEntity Create(Type type);
		IEntity InternalCreate(PEntityType key);
	}
}