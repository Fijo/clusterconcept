using System;
using ClusterConcept.EntityFramework.Model.Base;
using ClusterConcept.Shared.Model.Type;

namespace ClusterConcept.EntityFramework.Infrastructure.Service.KeyResolver
{
	public interface IKeyResolver<in T, out TKey> where T : IBasicEntity where TKey : PBaseType
	{
		TKey Get(Type basicEntity);
	}
}