using System;
using ClusterConcept.EntityFramework.Model.Entity;
using ClusterConcept.Shared.Model.Type;
using JetBrains.Annotations;

namespace ClusterConcept.EntityFramework.Infrastructure.Service.KeyResolver
{
	[UsedImplicitly]
	public class EntityKeyResolver : IKeyResolver<IEntity, PEntityType>
	{
		#region Implementation of IKeyResolver<in IEntity,out PEntityType>
		public PEntityType Get(Type basicEntity) {
			return new PEntityType(basicEntity.Name);
		}
		#endregion
	}
}