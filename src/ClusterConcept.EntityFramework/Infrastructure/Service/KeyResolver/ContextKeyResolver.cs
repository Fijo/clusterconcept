using System;
using ClusterConcept.EntityFramework.Model.DataContext;
using ClusterConcept.Shared.Model.Type;
using JetBrains.Annotations;

namespace ClusterConcept.EntityFramework.Infrastructure.Service.KeyResolver
{
	[UsedImplicitly]
	public class ContextKeyResolver : IKeyResolver<IDataContext, PContextType>
	{
		#region Implementation of IKeyResolver<in IDataContext,out PContextType>
		public PContextType Get(Type basicEntity) {
			return new PContextType(basicEntity.Name);
		}
		#endregion
	}
}