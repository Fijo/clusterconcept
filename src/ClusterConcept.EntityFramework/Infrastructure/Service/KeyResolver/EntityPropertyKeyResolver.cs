using System;
using ClusterConcept.EntityFramework.Model.Entity;
using ClusterConcept.Shared.Model.Type;
using JetBrains.Annotations;

namespace ClusterConcept.EntityFramework.Infrastructure.Service.KeyResolver {
	[UsedImplicitly]
	public class EntityPropertyKeyResolver : IKeyResolver<IEntity, PPropertyType>
	{
		#region Implementation of IKeyResolver<in IEntity,out PEntityType>
		public PPropertyType Get(Type basicEntity) {
			return new PPropertyType(basicEntity.Name);
		}
		#endregion
	}
}