using System.Collections.Generic;
using ClusterConcept.EntityFramework.Model.Internal.Events;

namespace ClusterConcept.EntityFramework.Infrastructure.Service.Transaction {
	public interface ITransactionService {
		void AddEvent(Transaction transaction, IEvent @event);
		IEnumerable<IEvent> GetEvents(Transaction transaction);
	}
}