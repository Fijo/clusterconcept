namespace ClusterConcept.EntityFramework.Infrastructure.Service.Transaction {
	public interface ITransactionFactory {
		Transaction CreateTransaction();
	}
}