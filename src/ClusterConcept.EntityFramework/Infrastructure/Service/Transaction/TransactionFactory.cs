using JetBrains.Annotations;

namespace ClusterConcept.EntityFramework.Infrastructure.Service.Transaction {
	[PublicAPI]
	public class TransactionFactory : ITransactionFactory {
		#region Implementation of ITransactionFactory
		public Transaction CreateTransaction() {
			return new Transaction();
		}
		#endregion
	}
}