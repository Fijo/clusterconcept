using System.Collections.Generic;
using System.Diagnostics;
using ClusterConcept.EntityFramework.Model.Internal.Events;
using JetBrains.Annotations;

namespace ClusterConcept.EntityFramework.Infrastructure.Service.Transaction {
	[PublicAPI]
	public class TransactionService : ITransactionService {
		#region Implementation of ITransactionService
		public void AddEvent(Transaction transaction, IEvent @event) {
			#region PreCondition
			Debug.Assert(!transaction.Events.Contains(@event), "Do not add the same event more than one time into an Transaction.");
			#endregion
			transaction.Events.Add(@event);
		}

		public IEnumerable<IEvent> GetEvents(Transaction transaction) {
			return transaction.Events;
		}
		#endregion
	}
}