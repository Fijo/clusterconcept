using System.Collections.Generic;
using ClusterConcept.EntityFramework.Model.Internal.Events;

namespace ClusterConcept.EntityFramework.Infrastructure.Service.Transaction {
	public class Transaction {
		public readonly IList<IEvent> Events = new List<IEvent>();
	}
}