using System.Collections.Generic;
using ClusterConcept.EntityFramework.Model.Internal;
using ClusterConcept.Shared.Model.Type;

namespace ClusterConcept.EntityFramework.Infrastructure.Service.Event {
	public interface IChangeEventService {
		IDictionary<PPropertyType, long> GetEntityChanges<TPersistType>(DataBundle dataBundle, TPersistType persistType) where TPersistType : PBaseType;
		IDictionary<PPropertyType, IList<byte>> GetPrimitiveChanges<TPersistType>(DataBundle dataBundle, TPersistType persistType) where TPersistType : PBaseType;
	}
}