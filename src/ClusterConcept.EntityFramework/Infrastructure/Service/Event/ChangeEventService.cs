//BS:Frei Wild - Eines Tages

using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using ClusterConcept.EntityFramework.Infrastructure.Service.Transaction;
using ClusterConcept.EntityFramework.Model.Internal;
using ClusterConcept.EntityFramework.Model.Internal.Events;
using ClusterConcept.Shared.Model.Type;
using FijoCore.Infrastructure.DependencyInjection.InitKernel;

namespace ClusterConcept.EntityFramework.Infrastructure.Service.Event {
	public class ChangeEventService : IChangeEventService {
		private readonly ITransactionService _transactionService = Kernel.Resolve<ITransactionService>();

		public IDictionary<PPropertyType, long> GetEntityChanges<TPersistType>(DataBundle dataBundle, TPersistType persistType) where TPersistType : PBaseType {
			var events = _transactionService.GetEvents(dataBundle.Chnageset)
				.OfType<ChangeEntityEvent<TPersistType>>();
			var changeEntityEvent = events.Single();
			Debug.Assert(Equals(persistType, changeEntityEvent.PersistType), "Equals(persistType, changeEntityEvent.PersistType)");
			return changeEntityEvent.Changeset;
		}

		public IDictionary<PPropertyType, IList<byte>> GetPrimitiveChanges<TPersistType>(DataBundle dataBundle, TPersistType persistType) where TPersistType : PBaseType {
			var events = _transactionService.GetEvents(dataBundle.Chnageset)
				.OfType<ChangePrimitiveEvent<TPersistType>>();
			var primitiveEvent = events.SingleOrDefault();
			if(primitiveEvent == null) return new Dictionary<PPropertyType, IList<byte>>();
			Debug.Assert(Equals(persistType, primitiveEvent.PersistType), "Equals(persistType, primitiveEvent.PersistType)");
			return primitiveEvent.Changeset;
		}
	}
}