using System;
using System.Collections.Generic;

namespace ClusterConcept.EntityFramework.Infrastructure.Interfaces
{
	public interface IPrimitiveHandling
	{
		string UId { get; }
		Type Type { get; }
		IEnumerable<byte> Serialize(object obj);
		object Deserialize(IEnumerable<byte> bytes);
	}
}