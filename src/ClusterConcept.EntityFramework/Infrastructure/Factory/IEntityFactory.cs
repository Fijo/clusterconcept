using ClusterConcept.EntityFramework.Model;
using ClusterConcept.EntityFramework.Model.Entity;

namespace ClusterConcept.EntityFramework.Infrastructure.Factory
{
	public interface IEntityFactory	{
		IEntity Create(EntityType key);
		IEntity Create(EntityType key, long id);
	}
}