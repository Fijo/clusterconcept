using System;
using System.Collections.Generic;
using System.Reflection;
using Fijo.Infrastructure.Documentation.Attributes.DesignPattern;

namespace ClusterConcept.EntityFramework.Model.Internal.TypeMeta {
	[Dto]
	public class TypeMeta {
		public Type Type;
		public IList<PropertyInfo> Entities;
		public IList<PropertyInfo> Primitives;
		public IList<PropertyInfo> Contextes;
	}
}