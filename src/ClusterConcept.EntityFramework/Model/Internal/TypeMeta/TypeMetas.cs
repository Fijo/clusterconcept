using System.Collections.Generic;

namespace ClusterConcept.EntityFramework.Model.Internal.TypeMeta
{
	public class TypeMetas
	{
		public IList<TypeMeta> Contextes;
		public IList<TypeMeta> Entities;
		public IList<TypeMeta> Primitives;
	}
}