namespace ClusterConcept.EntityFramework.Model.Internal.Events {
	public abstract class Event : IEvent {
		#region Implementation of IEvent
		public ulong Revision { get; set; }
		#endregion
	}
}