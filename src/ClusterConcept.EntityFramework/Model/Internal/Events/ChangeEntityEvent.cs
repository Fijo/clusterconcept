﻿using System.Collections.Generic;
using ClusterConcept.Shared.Model.Type;

namespace ClusterConcept.EntityFramework.Model.Internal.Events {
	public class ChangeEntityEvent<TPersistType> : Event
		where TPersistType : PBaseType {
		public TPersistType PersistType;
		public IDictionary<PPropertyType, long> Changeset;
		}
}