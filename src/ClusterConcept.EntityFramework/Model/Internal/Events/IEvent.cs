namespace ClusterConcept.EntityFramework.Model.Internal.Events {
	public interface IEvent {
		ulong Revision { get; set; }
	}
}