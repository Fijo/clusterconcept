using System.Collections.Generic;
using ClusterConcept.Shared.Model.Type;

namespace ClusterConcept.EntityFramework.Model.Internal.Events {
	public class ChangePrimitiveEvent<TPersistType> : Event
		where TPersistType : PBaseType {
		public TPersistType PersistType;
		public IDictionary<PPropertyType, IList<byte>> Changeset;
		}
}