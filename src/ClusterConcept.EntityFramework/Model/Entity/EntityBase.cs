using ClusterConcept.EntityFramework.Exceptions;
using Fijo.Infrastructure.Documentation.Attributes.Annotation;
using Fijo.Infrastructure.Documentation.Enums;
#if EntityNiceDebugging
using System.Diagnostics;
#endif


namespace ClusterConcept.EntityFramework.Model.Entity
{
	#if EntityNiceDebugging
	[DebuggerDisplay("{DebuggerDisplayValue()}")]
	#endif
	[Ignore(IgnoreFor.EntityFramework)]
	public abstract class EntityBase : IEntity	{
		private long _id;
		private bool _hasIdSet;

		public long Id{
			get	{
				if(!_hasIdSet) throw new NoIdSetException("no Id had been set yet - this should always be done when constructing an entity");
				return _id;
			}
			set	{
				_hasIdSet = true;
				_id = value;
			}
		}

		#if EntityNiceDebugging
		internal string DebuggerDisplayValue() {
			return string.Format("{0} #{1}", GetType().Name, _hasIdSet ? _id.ToString("X16") : " ?? ");
		}
		#endif
	}
}