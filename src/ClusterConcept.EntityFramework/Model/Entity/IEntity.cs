using System.ComponentModel;
using ClusterConcept.EntityFramework.Model.Base;

namespace ClusterConcept.EntityFramework.Model.Entity
{
	public interface IEntity : IBasicEntity	{
		long Id {get;[EditorBrowsable(EditorBrowsableState.Advanced)] set;}
	}
}