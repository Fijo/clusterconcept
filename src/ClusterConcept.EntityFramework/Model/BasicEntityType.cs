//BS:NOFX - Seeing Double at The Triple Rock Fat Wreck Chords

using System.Collections.Generic;
using ClusterConcept.EntityFramework.Model.Entity;
using ClusterConcept.EntityFramework.Model.Internal.TypeMeta;

namespace ClusterConcept.EntityFramework.Model {
	public class BasicEntityType {
		public TypeMeta TypeMeta;
		public IDictionary<long, IEntity> Entities;
		public IDictionary<long, object> Primitives;
	}
}