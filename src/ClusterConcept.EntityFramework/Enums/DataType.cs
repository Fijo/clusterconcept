namespace ClusterConcept.EntityFramework.Enums {
	public enum DataType {
		DataContext,
		Entity,
		Primitive
	}
}